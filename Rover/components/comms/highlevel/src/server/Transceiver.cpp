#include "server/Transceiver.h"

#include <unistd.h>


Transceiver::~Transceiver()
{
  is_running = false;
  sending.join();
}

void Transceiver::Start()
{
  is_running = true;
  sending = std::thread(&Transceiver::Sending, this);
  Receiving();
}

void Transceiver::Sending()
{
  // sends pending packets to its client
  // pass_message function
}

void Transceiver::Receiving()
{
  // receives packets and handles connection with the client
}

void Transceiver::tellMsg(Payload* payload)
{
  bool check = false;
  while (!check)
  {
    try
    {
      in.push(payload);
      check = true;
    } catch (std::range_error e) { usleep(500); }
  }
}

Payload* Transceiver::getMsg()
{
  bool check = false;
  while(!check)
  {
    try
    {
      return out.pop();
    } catch (std::range_error e) { usleep(500); }
  }
}
