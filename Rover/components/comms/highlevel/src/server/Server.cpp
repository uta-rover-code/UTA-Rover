#include "server/Server.h"
<<<<<<< HEAD
#include <unistd.h>


Server::Server(int port)
{
  io = new IOServer(port);
  stream = new StreamHandler(128);
}

void Server::Start()
{
  io->Start();
  is_running = true;
  sending = std::thread(&Server::Sending, this);
  Receiving();
}

void Server::Sending()
{
  while (is_running)
  {
    if (in.isEmpty())
      continue;

    if ((io->Send(in.pop())) <= 0)
      continue;
  }
}

void Server::Receiving()
{
  while (is_running)
  {
    if ((io->Recv(stream)) <= 0)
      continue;

    while(stream->hasPackets())
    {
      try
      {
        out.push(stream->next());
      } catch (std::range_error e) { usleep(500); }
    }
  }
=======
#include <iostream>

#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>

/*
Server *Server::getInstance(int port)
{
  if ()
  return new Server(port);
}
*/

void Server::Send(std::vector<byte> data)
{
  byte buffer[MAX_BYTES];
  serialize(data, buffer, MAX_BYTES);

  if (send(client_fd, buffer , MAX_BYTES, 0) != MAX_BYTES)
    throw "sever send error";
}

std::vector<byte> Server::Recv()
{
  byte buffer[MAX_BYTES];

  int valread = 0;
  if ((valread = read(client_fd, buffer, MAX_BYTES)) <= 0)
  {
    getpeername(client_fd, (struct sockaddr *)&address,
      (socklen_t *)sizeof(address));

    std::cout << "Server: Client disconnect:" << inet_ntoa(address.sin_addr) << ":" <<
        ntohs(address.sin_port) << std::endl;

    //Erase the disconnected socket
    close(client_fd);
    client_fd = 0;
  }
  else
  {
    return deserialize(buffer, MAX_BYTES);
  }
}

Server::Server(int port)
{
  //Setting up a TCP with IPv4
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    throw "Server socket creation failed";

  int on = 1;
  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
    throw "Server options failed";


  address.sin_family = AF_INET; //AF_INET is the IPv4 internet protocol
  address.sin_addr.s_addr = INADDR_ANY; //INADDR_ANY is for localhost
  address.sin_port = htons(port);
  addrlen = sizeof(address);

  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    throw "Server binding failed";

  if (listen(server_fd, 1))
    throw "Server start failed";

  is_running = true;

  if ((client_fd = accept(server_fd, (struct sockaddr *)&address,
                   (socklen_t*)&addrlen))<0)
  {
    perror("accept");
    exit(EXIT_FAILURE);
  }

  std::cout << "Server: Starting up" << std::endl;
>>>>>>> Server_branch
}
