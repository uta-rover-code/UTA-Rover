#include "server/StreamHandler.h"
#include <stdexcept>
#include <iostream>


StreamHandler::StreamHandler(int max_size)
{
  pending = Pending((max_size * 2) / pak::MIN);
  buffer.reserve(max_size * 2);
}

Payload* StreamHandler::next()
{
  if (pending.isEmpty())
    return NULL;

  return pending.pop();
}

void StreamHandler::load(byte* in, int in_size)
{
  if (pending.isFull())
    throw std::runtime_error("StreamHandler is full");

  for (int i = 0; i < in_size; i++)
  {
    if (!buffer.empty())
      buffer.push_back(in[i]);

    if (buffer.empty() && in[i] == pak::START)
      buffer.push_back(in[i]);
  }
  process();

  return;
}

void StreamHandler::process()
{
  while (buffer.size() >= buffer[1])
  {
    int total_size = buffer.capacity()/2;
    byte temp[total_size];
    int index;

    for (index = 0; index < buffer[1]; index++)
      temp[index] = buffer[index];

    try
    {
      pending.push(new Payload(temp, index, pak::MAX));
    } catch(std::invalid_argument e)
    {
      std::cout << e.what() << std::endl;
    }

    std::vector<byte> new_buffer;
    new_buffer.reserve(total_size * 2);

    for (; index < buffer.size(); index++)
    {
      if (!new_buffer.empty())
        new_buffer.push_back(buffer[index]);

      if (new_buffer.empty() && buffer[index] == pak::START)
        new_buffer.push_back(buffer[index]);
    }
    buffer.clear();
    buffer = new_buffer;
  }
}
