#include "server/Arduino.h"
#include "server/DataHandler.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <string>


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
` Constructor for Adruino class.
  Opens given serial port and sets port parameters.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Arduino::Arduino(std::string port){
  ser_dev = open(port.c_str(), O_RDWR | O_NOCTTY );

  struct termios tty;
  struct termios tty_old;
  memset (&tty, 0, sizeof(tty));

  if (tcgetattr (ser_dev, &tty) != 0)
    std::cout << "Arduino Error: get error" << std::endl;

  cfsetospeed(&tty, (speed_t)B115200);
  cfsetispeed(&tty, (speed_t)B115200);

  tty.c_cflag &= ~PARENB;
  tty.c_cflag &= ~CSTOPB;
  tty.c_cflag &= ~CSIZE;
  tty.c_cflag |= CS8;

  tty.c_cflag &= ~CRTSCTS;
  tty.c_cc[VMIN] = 0;
  tty.c_cc[VTIME] = 5;
  tty.c_cflag |= CREAD | CLOCAL;

  cfmakeraw(&tty);
  tcflush(ser_dev, TCIFLUSH);

  if (tcsetattr (ser_dev, TCSANOW, &tty) != 0)
    std::cout << "Arduino Error: set error" << std::endl;
  /*
  const char *t_device_value;
  struct termios my_serial;


  //open serial port with read and write, no controling terminal (we don't
  //want to get killed if serial sends CTRL-C), non-blocking
  ser_dev = open(port.c_str(), O_RDWR | O_NOCTTY );
  std::cout << port << std::endl;
  std::cout << ser_dev << std::endl;
  if (ser_dev < 0)
  {

    std::cout << "Failed to make Arduino" << std::endl;
  }
  sleep(3);
  bzero(&my_serial, sizeof(my_serial)); // clear struct for new port settings

  //B9600: set baud rate to 9600
  //   CS8     : 8n1 (8bit,no parity,1 stopbit)
  //   CLOCAL  : local connection, no modem contol
  //   CREAD   : enable receiving characters
  my_serial.c_cflag = B115200 | CS8 | CLOCAL | CREAD;

  tcflush(ser_dev, TCIFLUSH);
  tcsetattr(ser_dev,TCSANOW,&my_serial);

 // sleep(3);
 */
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
`         `
  Functions to send data to the arduion.
  One is designed to write a string, and the other to write a buffer (array)

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int Arduino::writePacket(std::vector<byte> buff)
{
  byte buffer[MAX_BYTES];
  serialize(buff, buffer, MAX_BYTES);

  return write(ser_dev, buffer, buff.size() + 3);
}

int Arduino::writePacket(void *buff, int size){
	return write(ser_dev,buff,size);
}

int Arduino::writePacket ( const std::string s ){
	return write(ser_dev,s.c_str(), s.size());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

  Function to read data from the arduino.
  One is designed to read a buffer (array), and the other to read a string. `         `

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int Arduino::readPacket(void *buff){

	int res = read(ser_dev,buff,64);
	return res;
	//buff = ob;
	//std::cout<<(int)buff[0]<<std::endl;
}


int Arduino::readPacket ( std::string & s )
{
  int MAXRECV = 64;
  char buf [ MAXRECV + 1 ];

  s = "";

  memset ( buf, 0, MAXRECV + 1 );
 // std::cout << "Reading\n";
  int res = read(ser_dev,buf,65);
 // std::cout << buf << std::endl;
  s = buf;
  return res;
}

int Arduino::IsOpen() {
	return ser_dev;
}
