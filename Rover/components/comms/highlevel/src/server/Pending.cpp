#include "server/Pending.h"
#include <stdexcept>


Payload* Pending::pop()
{
  if (isEmpty())
    throw std::range_error("Pop invalid");

  Payload* out = p_queue.front();
  for (auto it = memory.begin(); it != memory.end(); it++)
  {
    if (out->getSize() > 0 && *it == (*out)[0])
    {
      memory.erase(it--);
    }
  }
  p_queue.erase(p_queue.begin());

  return out;
}

void Pending::push(Payload* in)
{
  if (p_queue.size() + 1 > max_size)
    throw std::range_error("Push invalid");

  bool check = false;
  byte command;
  for (int i = 0; i < memory.size(); i++)
  {
    if (in->getSize() > 0 && memory[i] == (*in)[0])
    {
      check = true;
      command = memory[i];
      break;
    }
  }

  if(!check)
    p_queue.push_back(in);

  for(int i = 0; i < p_queue.size(); i++)
  {
    if (in->getSize() > 0 && (*p_queue[i])[0] == command)
    {
      Payload* temp = p_queue[i];
      p_queue[i] = in;
      delete temp;

      return;
    }
  }
}
