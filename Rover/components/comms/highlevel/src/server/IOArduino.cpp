#include "server/IOArduino.h"


void IOArduino::init(std::string port)
{
  arduino = Arduino(port);
}

int IOArduino::Send(Payload* in)
{
  int bytes_sent;
  int size = in->getPacketSize();
  byte buffer[size];
  in->getPacket(buffer, size);

  return arduino.writePacket(buffer, size);
}

int IOArduino::Recv(StreamHandler* out)
{
  int bytes_recv;
  byte buffer[pak::MAX];

  bytes_recv = arduino.readPacket(buffer);
  out->load(buffer, bytes_recv);

  return bytes_recv;
}

void IOArduino::reconnect()
{
  try
  {
    init(port);
    std::cout << "Serial reestablished" << std::endl;
  } catch (std::runtime_error e) { }
}
