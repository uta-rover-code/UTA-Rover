#include "server/TArduino.h"
#include "server/DataHandler.h"

#include <iostream>
#include <unistd.h>


TArduino::TArduino(ITransceiver* parent, std::string port)
{
  this->parent = parent;
  device = new Arduino(port);
}

TArduino::~TArduino()
{
  delete device;
}

void TArduino::Start()
{
  if (!device)
    throw "Arduino didnt connect";

  is_running = true;
  sending = std::thread(&TArduino::Sending, this);
  Receiving();
}

void TArduino::process(std::vector<byte> input)
{
  //queue_packet(input);

  //writePacket function to communicate back to arduino
}

void TArduino::Sending()
{
  std::cout << "##Arduino send process started" << std::endl;

  std::vector<byte> packet;
  std::vector<ITransceiver*>::iterator it;

  while(is_running)
  {
<<<<<<< HEAD
    sleep(2);
    if (!sending_packets.empty())
    {
      packet = sending_packets.front();
      sending_packets.pop();

      int res = device->writePacket(packet);
      if (res > 0)
      {
        std::cout << "Sent arduino packet: ";
        for (int i = 0; i < packet.size(); i++)
        {
          std::cout << std::hex << (int)packet[i] << " ";
        }
        std::cout << std::endl;
      }
    //for(it = messengers.begin(); it != messengers.end(); it++)
    //  (*it)->queue_packet(packet);
    }
    else
    {
      packet.clear();
      packet.push_back(0xFA);
      int res = device->writePacket(packet);
      if (res > 0)
      {
        std::cout << "Sent arduino packet: ";
        for (int i = 0; i < packet.size(); i++)
        {
          std::cout << std::hex << (int)packet[i] << " ";
        }
        std::cout << std::endl;
      }
      sleep(2);
    }
=======
    sleep(1);

    if (sending_packets.empty())
      continue;

    packet = sending_packets.front();
    sending_packets.pop();

    int res = device->writePacket(packet);
    if (res > 0)
    {
      std::cout << "Sent arduino packet: ";
      for (int i = 0; i < packet.size(); i++)
      {
        std::cout << std::hex << (int)packet[i] << " ";
      }
      std::cout << std::endl;
    }
    //for(it = messengers.begin(); it != messengers.end(); it++)
    //  (*it)->queue_packet(packet);
>>>>>>> Server_branch
  }
}

void TArduino::Receiving()
{
  std::cout << "##Arduino recv process started" << std::endl;

  byte buffer[MAX_BYTES];
  std::vector<byte> packet;

  while(is_running)
  {
<<<<<<< HEAD
    sleep(1);
=======
>>>>>>> Server_branch
    if (device->IsOpen())
    {
      int res = device->readPacket(buffer);
      if (res > 0)
      {
        std::cout << "Recv arduino packet: ";
        for (int i = 0; i < res; i++)
        {
<<<<<<< HEAD
          std::cout << std::hex << (int)buffer[i] << " ";
=======
          std::cout << std::hex << (int)buffer[i];
>>>>>>> Server_branch
        }
        std::cout << std::endl;
        packet = deserialize(buffer, res);
      }

      if (!recving_packets.empty())
      {
        std::vector<byte> recv;
        recv = recving_packets.front();
        recving_packets.pop();

        // DO work here
      }

      //packet = deserialize(buffer, MAX_BYTES);
      if (parent == NULL)
        continue;

      parent->add_recv_packet(packet);
      //packet = deserialize(buffer, MAX_BYTES);
      //process(packet);
    }
  }
}
