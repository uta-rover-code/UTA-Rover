//Author - Ivan Leontiev

//Editors - Gregory Ferguson.
//
//
//
//
//

#include <iostream>
#include <string>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "server/Communications.h"


Communications::Communications() : m_sock ( -1 )
{

  //memset ( &m_addr, 0, sizeof ( m_addr ) );
  //std::cout << "memSet \n";
}

Communications::~Communications()
{
  struct sockaddr_in addr;
  socklen_t addr_size = sizeof(struct sockaddr_in);
  int res = getpeername(m_sock, (struct sockaddr *)&addr, &addr_size);
  std::cout << "Closing: " << inet_ntoa(addr.sin_addr) << std::endl;
  if ( is_valid() ){
   // ::close ( m_sock );
  }
//	std::cout << "closed" << std::endl;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
			Function creates and sets socket parameters.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool Communications::create()
{
  //Creating socket file descriptor.
  //socket(domain, type, protocol)
  //TCP connection.
  //IPv4 protocol
  m_sock = socket ( AF_INET, SOCK_STREAM, 0 );

  if ( ! is_valid() )
    return false;


  // TIME_WAIT - argh (only the only fucking comment, thanks ivan)
  //Setsockopt is optional, helps reuse of address and port.
  int on = 1;
  if ( setsockopt ( m_sock,
    SOL_SOCKET,
    SO_REUSEADDR | SO_REUSEPORT,
    ( const char* ) &on, sizeof ( on ) ) == -1)

      return false;


  return true;

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
`         `Bind function binds the socket to the address and port number.
          Address and port specified in m_addr (custom data structure).
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool Communications::bind ( const int port )
{

  if ( ! is_valid() )
    {
      return false;
    }


  //IP protocol
  m_addr.sin_family = AF_INET;

  //local host.
  m_addr.sin_addr.s_addr = INADDR_ANY;

  //port for address.
  m_addr.sin_port = htons (8080);

  int bind_return = ::bind ( m_sock,
			     ( struct sockaddr * ) &m_addr,
			     sizeof ( m_addr ) );


  if ( bind_return == -1 )
    {
      return false;
    }

  return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  Function puts server socket in a passive mode,
  where it waits for the client to approach the server to make a connection.


*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool Communications::listen() const
{
  if ( ! is_valid() )
    {
      return false;
    }

//int listen(int sockfd, int backlog)
//int backlog defines max length of queue of pending connections.
  int listen_return = ::listen ( m_sock, MAXCONNECTIONS );


  if ( listen_return == -1 )
    {
      return false;
    }

  return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

  Function extracts connection request from queue and creates a new connected socket.
  Ready to transfer data between client and server.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool Communications::accept ( Communications& new_Communications ) const
{
  int addr_length = sizeof ( m_addr );
  new_Communications.m_sock = ::accept ( m_sock, ( sockaddr * ) &m_addr, ( socklen_t * ) &addr_length );

  if ( new_Communications.m_sock <= 0 )
    return false;
  else
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

  Function to write string.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool Communications::send ( const std::string s )
{
  //send(socket, message, message.length, .... )
  int status = ::send ( m_sock, s.c_str(), s.size(), MSG_NOSIGNAL );

  //std::cout << "Bytes sent: " << status << std::endl;
  if ( status == -1 )
    {
      return false;
    }
  else
    {
      return true;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

  Function to write buffer (packet)

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool Communications::send (const void *buf, size_t len)
{

  //send(socket, buffer, buffer.length, ...)
  int status = ::send ( m_sock, buf, len, MSG_NOSIGNAL );

  //std::cout << "Bytes sent: " << status << std::endl;
  if ( status == -1 )
    {
      return false;
    }
  else
    {
      return true;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

  Function to read string.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int Communications::recv ( std::string &s )
{
  char buf [ MAXRECV + 1 ];

  s = "";

  memset ( buf, 0, MAXRECV + 1 );

  int status = ::recv ( m_sock, buf, MAXRECV, 0 );

  if ( status == -1 )
    {
      std::cout << "status == -1   errno == " << errno << "  in Communications::recv\n";
      return -1;
    }
  else if ( status == 0 )
    {
      std::cout<<"Returning 0"<<std::endl;
      return 0;
    }
  else
    {
      s = buf;
      std::cout<<s<<std::endl;
      return status;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

Function to read buffer (packet)

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int Communications::recv ( void *buf, size_t len )
{

  int status = ::recv ( m_sock, buf, len,MSG_WAITALL );

  if ( status == -1 )
    {
      std::cout << "status == -1   errno == " << errno << "  in Communications::recv\n";
      return -1;
    }
  else if ( status == 0 )
    {
      return 0;
    }
  else
    {
      return status;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
    *Not used for server.
    Function is for client to connect to the server.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool Communications::connect ( const std::string host, const int port )
{
  if ( ! is_valid() ) return false;

  m_addr.sin_family = AF_INET;
  m_addr.sin_port = htons ( port );

  int status = inet_pton ( AF_INET, host.c_str(), &m_addr.sin_addr );

  if ( errno == EAFNOSUPPORT ) return false;

  status = ::connect ( m_sock, ( sockaddr * ) &m_addr, sizeof ( m_addr ) );

  if ( status == 0 )
    return true;
  else
    return false;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

  *Assuming it works the same as java.
  Read functions return immediately with any available data.
  Returns 0 if no data available

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Communications::set_non_blocking ( const bool b )
{

  int opts;

  opts = fcntl ( m_sock,
		 F_GETFL );

  if ( opts < 0 )
    {
      return;
    }

  if ( b )
    opts = ( opts | O_NONBLOCK );
  else
    opts = ( opts & ~O_NONBLOCK );

  fcntl ( m_sock,
	  F_SETFL,opts );

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

  Function closes the socket.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool Communications::close(){
  if ( is_valid() ){
   ::close ( m_sock );
   return true;
   }
   return false;
}
