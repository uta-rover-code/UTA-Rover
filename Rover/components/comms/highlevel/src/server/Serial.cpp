#include "server/Serial.h"
#include <unistd.h>


Serial::Serial(std::string port)
{
  io = new IOArduino(port);
  stream = new StreamHandler(64);
}

void Serial::Start()
{
  is_running = true;
  sending = std::thread(&Serial::Sending, this);
  Receiving();
}

void Serial::Sending()
{
  while (is_running)
  {
    if (in.isEmpty())
      continue;

    if ((io->Send(in.pop())) <= 0)
      continue;
  }
}

void Serial::Receiving()
{
  while (is_running)
  {
    if ((io->Recv(stream)) <= 0)
      continue;

    while(stream->hasPackets())
    {
      try
      {
        out.push(stream->next());
      } catch (std::range_error e) { usleep(500); }
    }
  }
}
