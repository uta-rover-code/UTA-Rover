#include "server/ServerProtocol.h"

#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <queue>

int server_fd = 0;
int client_fd = 0;
struct sockaddr_in address;
int addrlen = 0;

bool is_running = false;

std::queue<Packet> arduino_queue;
std::queue<Packet> client_queue;
Arduino rover;
int socket_id;


void init(int port, std::string arduino_port)
{
	rover = Arduino(arduino_port);

	//Setting up a TCP with IPv4
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
		throw "Server socket creation failed";

	int on = 1;
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
		throw "Server options failed";


	address.sin_family = AF_INET; //AF_INET is the IPv4 internet protocol
	address.sin_addr.s_addr = INADDR_ANY; //INADDR_ANY is for localhost
	address.sin_port = htons(port);
	addrlen = sizeof(address);

	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
		throw "Server binding failed";

	if (listen(server_fd, 1))
		throw "Server start failed";

	is_running = true;

	std::cout << "Server: Starting up" << std::endl;
}

void process(Packet packet)
{
	std::vector<byte> payload;

	std::vector<byte> contents = packet.get_data();
	byte packet_size = contents[1];
	byte command = contents[2];

	switch (command)
	{
		case 00:
			std::cout << "Server: Turning off . . . " << std::endl;
			is_running = false;
			break;

		default:
		{
			payload.push_back(0);
		}
	}

	arduino_queue.push(Packet(payload));
}

//Handles connect, disconnect and reading from client
void start(int port, std::string arduino_port)
{
	fd_set readfds;	//Used to track client disconnects
	byte buffer[MAX_BYTES];

	try
	{
		init(port, arduino_port);
	}
	catch(const char* e)
	{
		throw e;
	}

	while (is_running)
	{
		//Sets up socket set
		FD_ZERO(&readfds);
		FD_SET(server_fd, &readfds);
		int max_sd = server_fd;

		//If client is valid then add to set
		if (client_fd > 0)
		{
			FD_SET(client_fd, &readfds);
			max_sd = client_fd;
		}

		//Wait for activity on one of the sockets
		int activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
		if ((activity < 0) && (errno != EINTR))
		{
			throw "Server Error: select error";
		}

		//Checks if an incoming connection is happening on server_fd
		if (FD_ISSET(server_fd, &readfds))
		{
			//Obtain new client
			int new_socket = 0;
			if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
					(socklen_t *)&addrlen)) < 0)
				throw "Server Error: accept error";

			//Try to add new client
			if (client_fd == 0)
			{
				//setConnection(true);

				std::cout << "Server: Accepted new client " << new_socket << ":" <<
						inet_ntoa(address.sin_addr) << ":" <<ntohs(address.sin_port) << std::endl;

				//gets client ID
				client_fd = new_socket;

				//Create accept packet
				std::vector<byte> payload;
				payload.push_back(1);
				Packet a = Packet(payload);
				a.serialize(buffer, a.get_size());

				if (send(new_socket, buffer, MAX_BYTES, 0) != MAX_BYTES)
					throw "Server Error: send error";
				std::cout << "Server: Sent accept message" << std::endl;


			}
			else
			{
				//Send a default message (tells the client we denied)
				std::vector<byte> payload;
				payload.push_back(0);
				Packet a = Packet(payload);
				a.serialize(buffer, a.get_size());

				if (send(new_socket, buffer, MAX_BYTES, 0) != MAX_BYTES)
					throw "Server Error: send error";
				std::cout << "Server: Sent denial message" << std::endl;
				close(new_socket);
			}
		}

		//Try to do IO operations
		if(FD_ISSET(client_fd, &readfds))
		{
			//Check for client disconnect
			int valread = 0;
			if ((valread = read(client_fd, buffer, MAX_BYTES)) <= 0)
			{
				getpeername(client_fd, (struct sockaddr *)&address,
						(socklen_t *)sizeof(address));
				std::cout << "Server: Client disconnect:" << inet_ntoa(address.sin_addr) << ":" <<
						ntohs(address.sin_port) << std::endl;

				//Erase the disconnected socket
				close(client_fd);
				client_fd = 0;

				//Replace with autonomy process trigger
				//this->connection_lock.lock();
				//this->connected = false;
				//this->connection_lock.unlock();

			}
			else
			{
				Packet in;
				//Takes buffer from read function above
				in.deserialize(buffer, MAX_BYTES);

				//Send client response packet based on buffer
			 	process(in);
			}
		}
	}
}

//Used to send pending packets to arduino
void write_arduino()
{
	byte heartbeat[1] = {0};
	while (is_running)
	{
		if (arduino_queue.empty())
		{
			rover.writePacket(heartbeat, 1);
			sleep(1);
		}
		else
		{
			Packet out = arduino_queue.front();
			byte buffer[out.get_size()];
			out.serialize(buffer, out.get_size());

			rover.writePacket(buffer, out.get_size());

			arduino_queue.pop();
		}
	}
}

//Used to read and detect arduino connections
void read_arduino()
{
	//std::chrono::time_point<std::chrono::system_clock> start, startms, end;
	byte buffer[MAX_BYTES];
	while (is_running)
	{
		//start = std::chrono::system_clock::now();
		int c = 0;
		if (c = rover.readPacket(buffer) > 0)
		{
			Packet out = Packet(buffer, MAX_BYTES);

			switch(buffer[0])
			{

				default: client_queue.push(Packet(buffer)); //Adding new packet to queue

				break;
			}
		}
	}
}

void send_to_client()
{

	if (client_queue.empty())
	{
			//Add heartbeat function for client.
	}
	else
	{
		Packet out = client_queue.front();
		byte buffer[out.get_size()];
		out.serialize(buffer, out.get_size());

		//Need to get socket_id for client (need to test).
		if (send(client_fd, buffer, MAX_BYTES, 0) != MAX_BYTES)
			throw "Server Error:";

		client_queue.pop();
	}

}
