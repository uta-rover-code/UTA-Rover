#include "server/DataHandler.h"
<<<<<<< HEAD
#include <iostream>
=======
>>>>>>> Server_branch

void serialize(std::vector<byte> data, byte buffer[], int buffer_size)
{
  int total_size = data.size() + OVERHEAD_BYTES;
  if (total_size > MAX_BYTES || buffer_size < total_size)
    throw "size error";

  buffer[0] = START_BYTE;
  buffer[1] = total_size;
  for (int i = 0; i < data.size(); i++)
  {
    buffer[i+2] = data[i];
  }
  buffer[total_size - 1] = calculate_checksum(buffer, total_size);
}

std::vector<byte> deserialize(byte buffer[], int buffer_size)
{
<<<<<<< HEAD
  std::cout << "DESERIALIZE: " << std::endl;
  for(int i = 0; i < buffer_size; i++)
  {
    std::cout << std::hex << (int)buffer[i] << " ";
  }
  std::cout << std::endl;

  std::vector<byte> output;
  if (buffer_size < MIN_BYTES || buffer_size > MAX_BYTES)
  {
    std::cout << "size error" << std::endl;
    return output;
  }

  if (buffer[0] != START_BYTE)
  {
    std::cout << "bad handshake" << std::endl;
    return output;
  }
  if (buffer[1] < MIN_BYTES || buffer[1] > MAX_BYTES)
  {
    std::cout << "bad size" << std::endl;
    return output;
  }
  byte checksum = calculate_checksum(buffer, buffer[1]);
  if (buffer[buffer[1] - 1] != checksum)
  {
    std::cout << "bad checksum" << std::endl;
    return output;
  }

  //std::vector<byte> output;
=======
  if (buffer_size < MIN_BYTES || buffer_size > MAX_BYTES)
    throw "size error";

  if (buffer[0] != START_BYTE)
    throw "bad handshake";

  if (buffer[1] < MIN_BYTES || buffer[1] > MAX_BYTES)
    throw "bad size";

  byte checksum = calculate_checksum(buffer, buffer[1]);
  if (buffer[buffer[1] - 1] != checksum)
    throw "bad checksum";

  std::vector<byte> output;
>>>>>>> Server_branch

  int data_size = buffer[1] - OVERHEAD_BYTES;
  for (int i = 0; i < data_size; i++)
  {
    output.push_back(buffer[i + 2]);
  }

  return output;
}

void add_float(std::vector<byte> &data, float val)
{
  int total_size = data.size() + sizeof(float);
  if (total_size > MAX_BYTES)
    throw "size error";

  fbytes convert;
  convert.num = val;
  for (int i = 0; i < sizeof(float); i++)
  {
    data.push_back(convert.array[i]);
  }
}

byte calculate_checksum(byte buffer[], int buffer_size)
{
  byte checksum = buffer[0];
  for (int i = 1; i < buffer_size - 1; i++)
  {
    checksum ^= buffer[i];
  }
  return checksum;
}
