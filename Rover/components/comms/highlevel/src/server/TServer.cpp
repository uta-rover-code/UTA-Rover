#include "server/TServer.h"
#include "server/DataHandler.h"
#include <iostream>

#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>

TServer::TServer(int port, std::string aport)
{
  //Setting up a TCP with IPv4
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    throw "Server socket creation failed";

  int on = 1;
  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
    throw "Server options failed";


  address.sin_family = AF_INET; //AF_INET is the IPv4 internet protocol
  address.sin_addr.s_addr = INADDR_ANY; //INADDR_ANY is for localhost
  address.sin_port = htons(port);
  addrlen = sizeof(address);

  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    throw "Server binding failed";

  if (listen(server_fd, 1))
    throw "Server start failed";

  std::cout << "Connecting . . ." << std::endl;

<<<<<<< HEAD
  try
  {
    TArduino* arduino = new TArduino(this, aport);
    this->add_child(arduino);
  }
  catch(char const* e)
  {
    std::cout << e << std::endl;
  }
=======
  TArduino* arduino = new TArduino(this, aport);
  this->add_child(arduino);
>>>>>>> Server_branch
}

void TServer::Start()
{
  is_running = true;
  sending = std::thread(&TServer::Sending, this);
  arduino_process = std::thread(&TArduino::Start, (TArduino*)children[0]);
  Receiving();
}

void TServer::process_client(std::vector<byte> input)
{
  switch(input[0])
  {
<<<<<<< HEAD
    case 0xAB: //SYS
      ((TArduino*)children[0])->add_send_packet(input);
      break;
    case 0xCA: //LED
      ((TArduino*)children[0])->add_send_packet(input);
      break;
    case 0xBB: //Drive
      ((TArduino*)children[0])->add_send_packet(input);
      break;
      /*
    default:
      std::cout << "Unknown command byte" << std::endl;
      break;*/
=======
    case 0xAB:
      ((TArduino*)children[0])->add_send_packet(input);
      break;
>>>>>>> Server_branch
  }
}

void TServer::process_arduino(std::vector<byte> input)
{
  switch(input[0])
  {
<<<<<<< HEAD
    case 0xAB: //SYS
      add_send_packet(input);
      break;
    case 0xCA: //LED
      add_send_packet(input);
      break;
    case 0xBB:
      add_send_packet(input);
      break;
  /*  default:
      std::cout << "Unknown command byte" << std::endl;
      break;*/
=======
    case 0xAB:
      add_send_packet(input);
      break;
>>>>>>> Server_branch
  }
}

void TServer::Sending()
{
  std::cout << "##Server send process started" << std::endl;

  byte buffer[MAX_BYTES];
  std::vector<byte> packet;

  while(is_running)
  {
    if (!sending_packets.empty())
    {
      packet = sending_packets.front();
      sending_packets.pop();

      client_lock.lock();
      if (client_fd > 0)
      {
        serialize(packet, buffer, MAX_BYTES);
        if (send(client_fd, buffer, MAX_BYTES, 0) != MAX_BYTES)
          std::cout << "Failed Send" << std::endl;
      }
      client_lock.unlock();
      std::cout << "Sent client message" << std::endl;
    }

    if (!recving_packets.empty())
    {
      std::vector<byte> packet;
      packet = recving_packets.front();
      recving_packets.pop();

      process_arduino(packet);
    }
  }
}

void TServer::Receiving()
{
  std::cout << "##Server recv process started" << std::endl;

  fd_set readfds;
  byte buffer[MAX_BYTES];
  std::vector<byte> packet;

  while (is_running)
	{
		//Sets up socket set
		FD_ZERO(&readfds);
		FD_SET(server_fd, &readfds);
		int max_sd = server_fd;

		//If client is valid then add to set
		if (client_fd > 0)
		{
			FD_SET(client_fd, &readfds);
			max_sd = client_fd;
		}

		//Wait for activity on one of the sockets
		int activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
		if ((activity < 0) && (errno != EINTR))
		{
			throw "Server Error: select error";
		}

		//Checks if an incoming connection is happening on server_fd
		if (FD_ISSET(server_fd, &readfds))
		{
			//Obtain new client
			int new_socket = 0;
			if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
					(socklen_t *)&addrlen)) < 0)
				throw "Server Error: accept error";

      int valread = 0;
      if ((valread = read(new_socket, buffer, MAX_BYTES)) <= 0)
      {
        getpeername(new_socket, (struct sockaddr *)&address,
						(socklen_t *)sizeof(address));
				std::cout << "Server: New client disconnect: " << inet_ntoa(address.sin_addr) << ":" <<
						ntohs(address.sin_port) << std::endl;

				//Erase the disconnected socket
				close(new_socket);
      }

			//Try to add new client
			if (client_fd == 0)
			{
				std::cout << "Server: Accepted new client " << new_socket << ":" <<
						inet_ntoa(address.sin_addr) << ":" <<ntohs(address.sin_port) << std::endl;

        std::vector<byte> out = deserialize(buffer, MAX_BYTES);
        send(new_socket, buffer, MAX_BYTES, 0);

        client_lock.lock();
        client_fd = new_socket;
        client_lock.unlock();
			}
			else
			{
				//Send a default message (tells the client we denied)
				std::vector<byte> payload;
				payload.push_back(0);
				serialize(payload, buffer, MAX_BYTES);

				if (send(new_socket, buffer, MAX_BYTES, 0) != MAX_BYTES)
					throw "Server Error: send error";
				std::cout << "Server: Sent denial message" << std::endl;
				close(new_socket);
			}
		}

<<<<<<< HEAD
    std::fill(std::begin(buffer), std::begin(buffer)+MAX_BYTES, 'r');
=======
>>>>>>> Server_branch
		//Try to do IO operations
		if(FD_ISSET(client_fd, &readfds))
		{
			//Check for client disconnect
			int valread = 0;
			if ((valread = read(client_fd, buffer, MAX_BYTES)) <= 0)
			{
				getpeername(client_fd, (struct sockaddr *)&address,
						(socklen_t *)sizeof(address));
				std::cout << "Server: Client disconnect:" << inet_ntoa(address.sin_addr) << ":" <<
						ntohs(address.sin_port) << std::endl;

				//Erase the disconnected socket
				close(client_fd);
				client_fd = 0;

				//autonomy process trigger
			}
			else
			{
        std::vector<byte> out = deserialize(buffer, MAX_BYTES);

			 	process_client(out);
        std::cout << "Recv client message" << std::endl;
			}
		}
	}
}
