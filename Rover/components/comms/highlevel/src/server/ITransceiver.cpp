#include "server/ITransceiver.h"
#include <iostream>


ITransceiver::ITransceiver() { is_running = false; }

ITransceiver::~ITransceiver()
{
  is_running = false;
  sending.join();
}

void ITransceiver::Sending()
{
  // sends pending packets to its client
  // pass_message function
}

void ITransceiver::Receiving()
{
  // receives packets and handles connection with the client
}

void ITransceiver::Start()
{
  is_running = true;
  sending = std::thread(&ITransceiver::Sending, this);
  Receiving();
}

void ITransceiver::add_send_packet(std::vector<byte> input) { sending_packets.push(input);}
void ITransceiver::add_recv_packet(std::vector<byte> input) { recving_packets.push(input);}

void ITransceiver::add_child(ITransceiver* t) { children.push_back(t);}

void ITransceiver::tell_children(std::vector<byte> input)
{
  for (int i = 0; i < children.size(); i++)
  {
    children[i]->add_recv_packet(input);
  }
}

void ITransceiver::tell_parent(std::vector<byte> input)
{
  if(parent != NULL)
  {
    parent->add_recv_packet(input);
  }
}
