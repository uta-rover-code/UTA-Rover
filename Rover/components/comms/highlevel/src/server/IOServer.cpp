#include "server/IOServer.h"

#include <stdexcept>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>


IOServer::~IOServer()
{
  std::cout << "Server Ended" << std::endl;
}

void IOServer::init(int port)
{
  if ((server = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    throw std::logic_error("Socket initialization failed");

  int on = 1;
  if (setsockopt(server, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
    throw std::logic_error("Socket options failed");

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(port);

  if (bind(server, (struct sockaddr *)&address, sizeof(address)) < 0)
    throw std::logic_error("Socket binding failed");
}

void IOServer::Start()
{
  std::cout << "Server Started . . . " << std::endl;

  if (listen(server, 1))
    throw std::logic_error("Socket start failed");

  handleNewClient();
}

int IOServer::Send(Payload* in)
{
  int byte_sent = 0;
  int size = in->getPacketSize();
  byte buffer[size];

  in->getPacket(buffer, size);
  if (byte_sent = send(client, buffer, size, 0) < 0)
    return 0;

  return byte_sent;
}

int IOServer::Recv(StreamHandler* out)
{
  setActivity();

  if (FD_ISSET(server, &readfds))
    handleNewClient();

  if (FD_ISSET(client, &readfds))
  {
    int bytes_recv = 0;
    byte buffer[pak::MAX];

    if ((bytes_recv = read(client, buffer, pak::MAX)) <= 0)
    {
      printClientMsg(client, "disconnected");

      close(client);
      client = 0;
      return 0;
    }

    out->load(buffer, bytes_recv);
    return bytes_recv;
  }
}

void IOServer::setActivity()
{
  int max_fd;
  FD_ZERO(&readfds);
  FD_SET(server, &readfds);
  max_fd = server;

  if (client > 0)
  {
    FD_SET(client, &readfds);
    max_fd = client;
  }

  //If this goes wrong then something must have happened to one of the sockets
  int activity = select(max_fd + 1, &readfds, NULL, NULL, NULL);
  if ((activity < 0) && (errno != EINTR))
    throw std::logic_error("Socket failed to determine activity");
}

void IOServer::handleNewClient()
{
  byte buffer[pak::MAX];
  int new_client = 0;
  int addrlen = sizeof(address);
  if ((new_client = accept(server, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    throw std::logic_error("Socket new client failed");

  //check if client has requested connection
  int bytes_recv = 0;
  getpeername(new_client, (struct sockaddr *)&address, (socklen_t *)&addrlen);
  if ((bytes_recv = read(new_client, buffer, pak::MAX)) <= 0)
  {
    printClientMsg(new_client, "disconnected");

    close(new_client);
    return;
  }

  Payload request = Payload(64);
  if (client <= 0)
  {
    //Not using Pending which is unsafe but assumed okay
    if (!request.loadPacket(buffer, bytes_recv))
      printClientMsg(new_client, "failed");

    if (request.getSize() < 0 || request[0] != 0x01)
      printClientMsg(new_client, "failed handshake");

    request.clear();
    request.push(0x01);
    request.getPacket(buffer, request.getPacketSize());
    send(new_client, buffer, request.getPacketSize(), 0);

    printClientMsg(new_client, "accepted handshake");

    client = new_client;
    return;
  }

  request.push(0x00);
  request.getPacket(buffer, request.getPacketSize());
  send(new_client, buffer, request.getPacketSize(), 0);

  printClientMsg(new_client, "denied handshake");

  close(new_client);
}

void IOServer::printClientMsg(int sock, std::string msg)
{
  int addrlen = sizeof(address);
  getpeername(sock, (struct sockaddr *)&address, (socklen_t *)&addrlen);

  std::cout << inet_ntoa(address.sin_addr) << ":" << ntohs(address.sin_port) <<
  " " << msg << std::endl;
}
