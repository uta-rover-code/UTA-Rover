#include "server/Payload.h"

#include <stdexcept>


Payload::Payload(byte size)
{
  if (size < pak::MIN)
    throw std::invalid_argument("Invalid size");

  this->max_size = size - pak::OVERHEAD;
  vector.reserve(this->max_size);
  vector.clear();
}

Payload::Payload(byte* in, int in_size, byte max_size)
{
  this->max_size = max_size;
  vector.reserve(this->max_size);
  vector.clear();
  if (!loadPacket(in, in_size))
    throw std::invalid_argument("Bad packet");
}

Payload::~Payload()
{

}

//Assumes out size is not equal to payload size
int Payload::getPacket(byte* out, int out_size)
{
  if (out_size < vector.size() + pak::OVERHEAD)
    throw std::invalid_argument("Invalid size");

  out[0] = pak::START;
  out[1] = vector.size() + pak::OVERHEAD;
  for (int i = 0; i < vector.size(); i++)
    out[i + 2] = vector[i];

  byte checksum = pak::getChecksum(out, out[1]);
  out[out[1] - 1] = checksum;

  return out[1];
}

bool Payload::loadPacket(byte* in, int in_size)
{
  vector.clear();
  if (!pak::validate(in, in_size))
    return false;

  if (in[1] > max_size)
    return false;

  for (int i = 2; i < in[1] - 1; i++)
    vector.push_back(in[i]);

  return true;
}

void Payload::push (byte val)
{
  if (vector.size() + pak::OVERHEAD + 1 > max_size)
    throw std::length_error("Payload push exceeds max size");

  vector.push_back(val);
}

byte& Payload::operator[] (int i)
{
  if (i >= vector.size())
    throw std::out_of_range("Payload invalid index");

  return vector[i];
}

const byte& Payload::operator[] (int i) const
{
  if (i >= vector.size())
    throw std::out_of_range("Payload invalid index");

  return vector[i];
}

std::ostream& operator<<(std::ostream& os, const Payload& p)
{
  os << std::hex << "[ ";
  for (int i = 0; i < p.getSize(); i++)
    os << (int)p[i] << " ";
  os << std::dec << "]";

  return os;
}
