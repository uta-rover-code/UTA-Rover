void *client_listener_img(void * ptr){
	Communications* c_cmd = (Communications*) ptr;
	std::cout << "Starting img listener \n";
	//Communications new_sock;
	//img_clients.push_back(new_sock);
	Communications new_sock;
	int client_count = 0;
	while(true){

		if(c_cmd->accept(new_sock)){
			isReady = true;
			std::cout << "Adding Client \n";
			img_clients.push_back(new_sock);
			client_count++;
			pthread_t client_thread;
			//pthread_create(&client_thread,NULL,client_img,new ClientWrapper(client_count,new_sock));
		//	std::cout << img_clients.size() << std::endl;
		//	Communications new_sock_1;
			//img_clients.push_back(new_sock_1);
		}else{
			std::cerr << "Client failed to connect" << std::endl;
		}

		/*std::cout << "loop thread" << std::endl;
		std::string in;
		if(new_sock->recv(in) < 0 ){
			break;
		}
		std::cout << in;*/
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*																														*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void *client_img(void * ptr){
	std::chrono::time_point<std::chrono::system_clock> start,startms, end;
	ClientWrapper* wrap = (ClientWrapper*) ptr;
	int id = wrap->threadId;
	Communications client = wrap->o;
	clock_t begin = clock();
     	start = std::chrono::system_clock::now();
        int frames = 0;
        int bytess = 0;
	while(true){
		char buffer [PACKET_ARRAY_SIZE];
		startms =  std::chrono::system_clock::now();
		struct sockaddr_in addr;
    		socklen_t addr_size = sizeof(struct sockaddr_in);
    		int res = getpeername(client.m_sock, (struct sockaddr *)&addr, &addr_size);
	//	std::cout << "Sending Image to Client: " << inet_ntoa(addr.sin_addr) << std::endl;
	//	std::string in;
		//if(client.recv(i	n) <= 0){
		//	cerr << "couldn't read bytes" << std::endl;
		//	break;
		//}
		imencode(".jpg", raw_img, buff, params);
		int total_pack = 1 + (buff.size() - 1) / PACKET_SIZE;

		int ibuf[1];
            	ibuf[0] = total_pack;
		//std::cout << total_pack << std::endl;
	//	snprintf ( buffer, 10, "%d", buff.size());
		if (client.send(ibuf, sizeof(int)) == 0){
                     	std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                     	break;
                }
		/*for (int i = 0; i < total_pack; i++){
			if ((client.send(&buff[i*PACKET_SIZE],PACKET_SIZE)) == 0){
                    		std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                     		break;
                 	}
			//std::cout << i*PACKET_SIZE << std::endl;
		}*/
		/*if (client.send(buffer, strlen(buffer)) == 0){
                     std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                     break;
                }*/
		if ((client.send(&buff[0], buff.size())) == 0){
                      std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                     break;
                 }
		/*if(client.send("1000\n") == 0){
			std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
			break;
		}*/
		//std::cout << "a" << std::endl;
		end = std::chrono::system_clock::now();
		int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
		int elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-startms).count();
		//std::cout << "- " << elapsed_seconds << " - " <<elapsed_milliseconds <<" - "<<freq<<std::endl;
		//sleep(1);
		while( elapsed_milliseconds < freq*1000){
 			usleep(50);
			end = std::chrono::system_clock::now();
			elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-startms).count();
			//std::cout << "elapsed frames: " <<frames <<  "-" << freq*1000<< " " << elapsed_milliseconds<< "\n";
		}
		if( elapsed_seconds >= 1){
   			start = std::chrono::system_clock::now();
    		      //  std::cout << "elapsed time: " << elapsed_seconds << "\n";
 		//	std::cout << "elapsed frames: " <<frames <<  " - IP: "<< inet_ntoa(addr.sin_addr) << " "  << img_clients.size()<< " " << elapsed_milliseconds<< " -- " << "\n";
			frames = 0;
		//	bytess = 0;
		}else {
			frames ++;
		//	bytess += bytes;
		}
		//std::cout << client.send("1000\n") << std::endl;
		//std::cout << client.send("123123\n") << std::endl;
		//std::cout << in;
	}
	Communications *temp = &client;
	temp = NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*																														*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void *camera_listener(void *){
	std::chrono::time_point<std::chrono::system_clock> start,startms, end;
	clock_t begin = clock();
     	start = std::chrono::system_clock::now();
        int frames = 0;
        int bytess = 0;
	int avg_fps = 0;
	std::cout << "Starting Loop\n";
	int cam = SELECTED_CAMERA;
	int fps = CAMERA_FPS;
	int qual = CAMERA_QUALITY;
	int res_width =CAMERA_WIDTH;
	int res_height =CAMERA_HEIGHT;


	while(true){

			if( !isReady){
				usleep(1000);
				continue;
			}
			if(img_clients.size() ==0){
				usleep(1000);
				continue;
			}
			//std::cout <<" Width: "<<CAMERA_WIDTH << std::endl;
			if(SELECTED_CAMERA != cam){
			//	if(!cap.open(SELECTED_CAMERA)){
					cap.open(SELECTED_CAMERA);

					cap.set(CV_CAP_PROP_FRAME_WIDTH,640);
					cap.set(CV_CAP_PROP_FRAME_HEIGHT,480);
					std::cout <<SELECTED_CAMERA << "\n";
			//		SELECTED_CAMERA = cam;
			//	}else{
					cam = SELECTED_CAMERA;
			//	}
			}

			if(CAMERA_FPS != fps){
				freq = 1.0/((double)CAMERA_FPS);
				fps = CAMERA_FPS;
			//	cap.set(CV_CAP_PROP_FPS, fps);
			}
			if(CAMERA_QUALITY != qual){
				params.clear();
				params.push_back(cv::IMWRITE_JPEG_QUALITY);
				params.push_back(CAMERA_QUALITY);
				qual = CAMERA_QUALITY;
			}
			if(CAMERA_HEIGHT != res_height){
				raw_img = Mat::zeros(CAMERA_HEIGHT , CAMERA_WIDTH, CV_8UC3);
				res_width =CAMERA_WIDTH;
				res_height =CAMERA_HEIGHT;
			}
			//std::cout << "Getting Img\n";
			cap >> raw_img;

			//raw_img = getImage();
			//std::cout << "Image is empty: " << raw_img.empty() << std::endl;
			if(raw_img.empty()){
				raw_img = Mat(640,480,CV_8UC1,Scalar(0,0,0));

				if(ui_img){
					putText(raw_img, std::to_string(SELECTED_CAMERA+1), cvPoint(640/2,480/2),FONT_HERSHEY_COMPLEX_SMALL, 10, cvScalar(255,255,255), 1, CV_AA);
				}
			}else if(toggle_hsv){

				int scale = sc;
				Mat frame_threshold;
				Mat frame;
				Mat utmp = raw_img.clone();
				Mat tmp;


				utmp.copyTo(tmp);
				resize(tmp,tmp,Size(tmp.cols/scale,tmp.rows/scale),0,0,CV_INTER_AREA );
				tmp.copyTo(frame);

				cvtColor(frame,frame,COLOR_BGR2HSV);
        			inRange(frame,Scalar(low_b,low_g,low_r), Scalar(high_b,high_g,high_r),frame_threshold);
  				if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
  				else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
  				else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }
 				Mat element = getStructuringElement( dilation_type,
                                       Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                       Point( dilation_size, dilation_size ) );
  /// Apply the dilation operation
  				dilate( frame_threshold, frame_threshold, element );
				morphologyEx(frame_threshold,frame_threshold,MORPH_CLOSE,element);
				GaussianBlur(frame_threshold,frame_threshold,Size(5,5),0);


				if(toggle_track && !frame_threshold.empty()){
				std::vector<Vec3f> circles;
				HoughCircles(frame_threshold,circles,CV_HOUGH_GRADIENT,2,120,75,15,0,100);
        //-- Show the frames
				raw_img.copyTo(tmp);
				resize(tmp,tmp,Size(tmp.cols/scale,tmp.rows/scale),0,0,CV_INTER_AREA );
				tmp.copyTo(raw_img);
				for( size_t i = 0; i < circles.size(); i++ )
				{
				Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
   				int radius = cvRound(circles[i][2]);
   // circle center
   				circle( raw_img, center, 3, Scalar(0,255,0), -1, 8, 0 );
   // circle outline
  				 circle( raw_img, center, radius, Scalar(0,0,255), 3, 8, 0 );
 				}
				//raw_img = frame_threshold.clone();
				}
				raw_img.copyTo(tmp);
				resize(tmp,tmp,Size(640,480),0,0,CV_INTER_CUBIC );
				tmp.copyTo(raw_img);

				frame_threshold.copyTo(tmp);
				resize(tmp,tmp,Size(640/3,480/3));
				cvtColor(tmp,tmp,CV_GRAY2BGR);
				tmp.copyTo(raw_img(cv::Rect((raw_img.cols/3)*2,(raw_img.rows/3)*2,tmp.cols, tmp.rows)));

			}

			std::stringstream str;
			str << "FPS: " << avg_fps;
			if(ui_img){
				putText(raw_img, str.str(), cvPoint(20,20),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255,255,255), 1, CV_AA);
			}
			std::stringstream res;
			res << "Resolution: " << CAMERA_WIDTH << "x" << CAMERA_HEIGHT;
			if(ui_img){
				putText(raw_img, res.str(), cvPoint(20,40),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255,255,255), 1, CV_AA);
			}
			time_t tt;
			tt = std::chrono::system_clock::to_time_t ( std::chrono::system_clock::now() );
			std::string time = ctime(&tt);
			time.erase(std::remove(time.begin(), time.end(), '\n'), time.end());
			if(ui_img){
				putText(raw_img, time, cvPoint(20,raw_img.size().height-20),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255,255,255), 1, CV_AA);
			}
			startms =  std::chrono::system_clock::now();
			Mat tmp;


			//raw_img.copyTo(tmp);
			resize(raw_img,raw_img,Size(CAMERA_WIDTH,CAMERA_HEIGHT));
			//tmp.copyTo(raw_img);
			for(int i = 0; i < img_clients.size();i++){

				struct sockaddr_in addr;
    				socklen_t addr_size = sizeof(struct sockaddr_in);
    				int res = getpeername(img_clients.at(i).m_sock, (struct sockaddr *)&addr, &addr_size);
				//std::cout << "Client " << i << " is " << (img_clients.at(i).is_valid()) << std::endl;
				imencode(".jpg", raw_img, buff, params);
				int total_pack = 1 + (buff.size() - 1) / PACKET_SIZE;

				int ibuf[1];
    				ibuf[0] = buff.size();//total_pack;
    				int32_t  converted_number = htonl(buff.size());

				int packet[PACKET_ARRAY_SIZE];
				packet[0] = converted_number;
				float temp = -3.14645;
				unsigned char * c = (unsigned char *) &temp;
				packet[1] = c[0];
				packet[2] = c[1];
				packet[3] = c[2];
				packet[4] = c[3];

				//std::cout << "Total pack = " <<   packet[1] << " " <<   packet[2] << " "<<   packet[3] << " "<<   packet[4] << " " <<std::endl;
	//	snprintf ( buffer, 10, "%d", buff.size());
				if (img_clients.at(i).send(&packet[0], sizeof(int)*PACKET_ARRAY_SIZE) == 0){
                    			std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
					img_clients.erase(img_clients.begin() + i);
					i--;
					continue;
                     		//break;
                		}
			//for (int a = 0; a < total_pack; a++){
				if ((img_clients.at(i).send(&buff[0],buff.size())) == 0){
                    			std::cerr << "Failed to img send bytes, assuming client has disconnected" << std::endl;
					img_clients.erase(img_clients.begin() + i);
					i--;
					break;
                     	//	break;
                 		}
			//	std::cout << a*PACKET_SIZE << std::endl;
			//}
			}

		end = std::chrono::system_clock::now();
		int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
		int elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-startms).count();
		//std::cout << "- " << elapsed_seconds << " - " <<elapsed_milliseconds <<" - "<<freq<<std::endl;
		//sleep(1);

		while( elapsed_milliseconds < freq*1000){
	 		usleep(50);
			end = std::chrono::system_clock::now();
			elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-startms).count();
			//std::cout << "elapsed frames: " <<frames <<  "-" << freq*1000<< " " << elapsed_milliseconds<< "\n";
		}

		if( elapsed_seconds >= 1){
	   		start = std::chrono::system_clock::now();
			std::cout << "Cam fps: " << cap.get(CV_CAP_PROP_FPS) << "\n";
	    	        std::cout << "elapsed time: " << elapsed_seconds << "\n";
	 		std::cout << "elapsed frames: " <<frames << " "  << img_clients.size()<< " " << elapsed_milliseconds<< " -- " << "\n";
			avg_fps = frames;
			frames = 0;

// " - IP: "<< inet_ntoa(addr.sin_addr) <<
		//	bytess = 0;
		}else {
			frames ++;
		//	bytess += bytes;
		}

      	//	usleep(200);

	}
}
