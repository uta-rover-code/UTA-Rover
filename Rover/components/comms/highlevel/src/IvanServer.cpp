//Author - Ivan Leontiev

//Editors - Greg Ferguson
//
//
//
//
//

//Adding comments to the code would be great Ivan.

//#include "opencv2/opencv.hpp"
//#include "opencv2/videoio.hpp"
//#include <opencv2/core/ocl.hpp>
#include <iostream>
#include <cstdlib>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

#include <net/if.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <chrono>
#include <queue>
#include <unistd.h>
#include <string>
#include <fstream>
#include <pthread.h>
#include <sstream>
#include "sys/types.h"
#include "sys/sysinfo.h"
//#include "pixy.h"

#include "utils/Communications.cpp"
#include "../include/defs/global_defs.h"
#include "../include/utils/CommunicationException.h"
#include "../include/utils/Arduino.h"
#include "utils/Arduino.cpp"
Arduino core;
#include "../include/utils/Packet.h"

//using defined namespace
//using namespace cv;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*												Declaring Functions and Variables. 								*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void interpolateBayer(unsigned int width, unsigned int x, unsigned int y, unsigned char *pixel, unsigned int &r, unsigned int &g, unsigned int &b);
//Mat renderBA81(uint8_t renderFlags, uint16_t width, uint16_t height, uint32_t frameLen, uint8_t *frame);
//Mat getImage();

void *client_listener_cmd(void *);
void *client_cmd_read(void *);
void *client_cmd_write(void *);
void *camera_listener(void *);
void *network_listener(void *);
void *arduino_writer(void * ptr);
void *arduino_reader(void * ptr);
void *arduino_heart(void * ptr);
Communications cmd;


std::vector<u_char> buff;
std::vector<int> params;

//Mat raw_img;
//double freq = 1.0f/((double)CAMERA_FPS);
bool isReady = false;
bool ui_img = true;
//VideoCapture cap;
int numClients_cmd;
int numClients_img;

float eth0_usage_tx;
float eth0_usage_rx;
float wlan_usage_tx;
float wlan_usage_rx;



static unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;

/////////////////////////////////////////////////////////////////////////////////////////////////////






//////////////////////////////////////////////////////////////////////////////////////////////////////

class ClientWrapper
{
    public: int threadId;
    public: Communications o;

    public: ClientWrapper(int& threadId, Communications& o)
    {
        this->threadId = threadId;
        this->o = o;
    }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////






//////////////////////////////////////////////////////////////////////////////////////////////////////

class PacketWrapper
{
    public: int size;
    public: unsigned char * ptr;

    public: PacketWrapper(int s, unsigned char * obj)
    {
        this->size = s;
        this->ptr = obj;
    }
};

std::queue<PacketWrapper> arduino_queue;

	int dilation_elem = 0;
	int dilation_size = 0;
	int const max_elem = 2;
	int const max_kernel_size = 21;
	int radius = 5;
	int low_r=30;
	int low_g=30;
	int low_b=30;
	int high_r=100;
	int high_g=100;
	int high_b=100;
	int param_1 = 120;
	int param_2 = 100;

	int sc = 1;

	bool toggle_track = false;
	bool toggle_hsv = false;

//figure out what this does.
//int dilation_type = MORPH_RECT;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
							Function initializes the server functions.
							Sets up the socket on the server and waits for the client to connect.
							After successful connection from the client, connects server to the core.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int init(){

	//first phase of connecting server to client.
	cmd = Communications();

	//create and set the socket.
	if (!cmd.create() )
   	{
	  return 101;
	}

	//binds socket to the address and port number.
	if (!cmd.bind (CONTROL_PORT) )
   	{
	  return 102;
	}

	//waits for the client to make a connection to the server.
	if (!cmd.listen() )
   	{
	  return 103;
	}

	std::cout << "Command socket created" << std::endl;

	FILE* file = fopen("/proc/stat", "r");
    	fscanf(file, "cpu %llu %llu %llu %llu", &lastTotalUser, &lastTotalUserLow, &lastTotalSys, &lastTotalIdle);
    	fclose(file);

		//connects to the arduino on the specified serial port.
        core = Arduino("/dev/ttyACM0");
	sleep(1);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*																														*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double getCurrentCPUValue(){
    double percent;1
    std::string line;
    std::string tmp;
    FILE* file;
    unsigned long long totalUser, totalUserLow, totalSys, totalIdle, total;
	std::ifstream read("/proc/stat");
	getline (read,line);
	std::istringstream iss(line);
	iss >> tmp >> totalUser >> totalUserLow >> totalSys >> totalIdle;
	read.close();
  /*  file = fopen("/proc/stat", "r");
	if(file == NULL){
		return 0.0;
	}
    fscanf(file, "cpu %llu %llu %llu %llu", &totalUser, &totalUserLow,
        &totalSys, &totalIdle);
if(file != NULL)
    fclose(file);
*/
    if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow ||
        totalSys < lastTotalSys || totalIdle < lastTotalIdle){
        //Overflow detection. Just skip this value.
        percent = -1.0;
    }
    else{
        total = (totalUser - lastTotalUser) + (totalUserLow - lastTotalUserLow) +
            (totalSys - lastTotalSys);
        percent = total;
        total += (totalIdle - lastTotalIdle);
        percent /= total;
        percent *= 100;
    }

    lastTotalUser = totalUser;
    lastTotalUserLow = totalUserLow;
    lastTotalSys = totalSys;
    lastTotalIdle = totalIdle;

    return percent;
}





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*															Driver Code															*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(){
	int error;

	//test connection.
	if((error = init()) > 0){
	    return error;
	}



	//ocl::setUseOpenCL(1);
	std::cout << "init complete" << std::endl;

	std::cout << "client accepted" << std::endl;

	//create threads for different server functions.
	//pthread_create(thread_name, attr, start_routine, arg)

	pthread_t cmd_thread;
	pthread_t img_thread;
	pthread_t camera_thread;
	pthread_t network_thread;
	pthread_t read_arduino;
	pthread_t write_arduino;
	pthread_t arduino_heartbeat;
	pthread_create(&cmd_thread,NULL,client_listener_cmd,&cmd);
	//pthread_create(&img_thread,NULL,client_listener_img,&img);
	//pthread_create(&camera_thread,NULL,camera_listener,NULL);
	pthread_create(&network_thread,NULL,network_listener,NULL);
	std::cout << "Arduino: " << core.IsOpen() << std::endl;

	if(core.IsOpen() > 0){

      std::cout<<"Core connected"<<std::endl;
			//if core is connected, starts the read and write threads for the core.
      pthread_create(&write_arduino,NULL,arduino_writer,NULL);
      pthread_create(&read_arduino,NULL,arduino_reader,NULL);
			pthread_create(&arduino_heartbeat,NULL,arduino_heart,NULL); //starts arduino heartbeat,.
	}

	pthread_join(cmd_thread,NULL);
	//std::string in;
	//new_sock.recv(in);
	//std::cout << in;
	std::cout << PACKET_SIZE << std::endl;
	//std::cout << CAMERA_WIDTH << std::endl;
	//std::cout << CAMERA_HEIGHT << std::endl;
	return 0;
}

int drv = 0;
unsigned char * send_buff = new unsigned char[10];


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
							Acts similar to the data handler class in the java server.
							Getting data from the client, determines what to do with said data based off headerbyte.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void processData(char data []){

	//getting command byte and sub-command byte.
  char temp[3];
  temp[0] = data[0];
  temp[1] = data[1];
  temp[2] = data[2];
	int cmd = atoi(temp);	// ntohl() function converts the unsigned integer netlong from network byte order to host byte order i.e. network to host long.

	std::cout << "CMD " << cmd << std::endl;

	switch(cmd){

		case 0x0A:{
			send_buff[0] = 0xBA;
			PacketWrapper wrap (1,send_buff);
			std::cout << "Writing toggle auto cmd " << (int)send_buff[1] << "\n";
			if(arduino_queue.size() < MAXQUEUE){
				arduino_queue.push(wrap);
			}
		}
		break;


		case DRV:
      send_buff[0] = DRIVE;
      send_buff[2];
      send_buff[1];
      PacketWrapper wrap (3,send_buff);
      std::cout << "Writing drv cmd " << (int)send_buff[1] << " - " <<  (int)send_buff[2] << "\n";
      if(arduino_queue.size() < MAXQUEUE){
        arduino_queue.push(wrap);
      }

    case ARM_CONTROL:

      break;

    case SAR_CONTROL:

      break;
		break;
	}

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*				Takes the heartbeat of the core.
				Tests to make sure the core is still connected.
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void *arduino_heart(void * ptr){
	while(true){
		unsigned char * send_buff = new unsigned char[10];
		send_buff[0] = SYS_INFO;
		PacketWrapper wrap (1,send_buff);
		if(arduino_queue.size() < MAXQUEUE){
			arduino_queue.push(wrap);
		}

		//Note, this delay is in microseconds. Only 1 second
		usleep(1000000);
	}

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
						Write Thread for the core.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void *arduino_writer(void * ptr){


 	unsigned char send_buff[255];
  	int length_send_buff = 1;

	//send_buff[3] = 3;
  int count = 0;
	while(true){
		time_t t = time(0);   // get time now
   		struct tm * now = localtime( & t );


      //checks to see if queue is empty.
      //queue will be empty until data is received from the client.
		 if(!arduino_queue.empty())
  		{

        //assign packetwrapper object to the front of the arduion_queue
    		PacketWrapper wrp = arduino_queue.front();

			//std::cout << "Queue size: " << arduino_queue.size() << std::endl;

      //checks packet and sends tot he core.
			SendPacket(wrp.size,wrp.ptr);

      //remove the front object in queue.
			arduino_queue.pop();
  		}

		//usleep(35000);
                usleep(25000);
                count++;
		//sleep(1);
		//std::cout << "Writing Packet" << std::endl;1
	}
}

//setting packet parameters.
const unsigned int PACKET_OVERHEAD_BYTES = 3;
const unsigned int PACKET_MIN_BYTES = PACKET_OVERHEAD_BYTES + 1;
const unsigned int PACKET_MAX_BYTES = 255;

float lat,lon,roll,pitch,yaw = 0;



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
'						Read Thread for the core.
						Reads 4 unsigned chars (act as bytes) into a array.
						Then converts to float.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void *arduino_reader(void * ptr){
	//why are we using char?
    unsigned char * read_buff = new unsigned char[1];
	unsigned char * read_packet;
	unsigned char buffer[255];
	std::string s;
	int res;
	int count = 0;
	int packetSize = 3;

	//time point using the system clock.
	std::chrono::time_point<std::chrono::system_clock> start,startms, end;

	//starts time point.
	start = std::chrono::system_clock::now();
	int packet_c = 0;

	while(true){

        //usleep(1000);
        int c = 0;

		//LookForPacket reads data into buffer.
        if( (c = core.readPacket(buffer)) > 0){
			int count = 1;
			std::cout << (int)buffer[0] << std::endl;
			switch(buffer[0]){

					case 0xBF:
						std::cout << "NO GPS FIX" << std::endl;
					break;

					case SYS_INFO:
          /*

						unsigned char temp_f[4]; //temp char array to read in data.
						temp_f[0] = buffer[count++];
						temp_f[1] = buffer[count++];
						temp_f[2] = buffer[count++];
						temp_f[3] = buffer[count++];
						lat = byteToFloat(temp_f); //convert bytes to float.

						temp_f[0] = buffer[count++];
						temp_f[1] = buffer[count++];
						temp_f[2] = buffer[count++];
						temp_f[3] = buffer[count++];
						lon = byteToFloat(temp_f);

						temp_f[0] = buffer[count++];
						temp_f[1] = buffer[count++];
						temp_f[2] = buffer[count++];
						temp_f[3] = buffer[count++];
						roll = byteToFloat(temp_f);

						temp_f[0] = buffer[count++];
						temp_f[1] = buffer[count++];
						temp_f[2] = buffer[count++];
						temp_f[3] = buffer[count++];
						pitch = byteToFloat(temp_f);

						temp_f[0] = buffer[count++];
						temp_f[1] = buffer[count++];
						temp_f[2] = buffer[count++];
						temp_f[3] = buffer[count++];
						yaw = byteToFloat(temp_f);


						std::cout << "Lat = " << lat << " lon: " << lon << std::endl;
            */
					break;

					case COMM_PING:
						std::cout << "The arduino is green \n";
					break;
				}

				packet_c ++;
        	}

		//ends time point.
		end = std::chrono::system_clock::now();

		//gives time for elapsed time for reading a packet from the core.
		int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
		int elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-startms).count();
		if( elapsed_seconds >= 1){
   			start = std::chrono::system_clock::now();
    		      //  std::cout << "elapsed time: " << elapsed_seconds << "\n";
	//		std::cout << "packets read: " << packet_c << "\n";
 		//	std::cout << "elapsed frames: " <<frames <<  " - IP: "<< inet_ntoa(addr.sin_addr) << " "  << img_clients.size()<< " " << elapsed_milliseconds<< " -- " << "\n";
			packet_c = 0;
		//	bytess = 0;
		}else {

		//	bytess += bytes;
		}

	}

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
										Function accepts client connection.
										If successfully connected, starts the client read and write threads.
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void *client_listener_cmd(void * ptr){
	Communications* c_cmd = (Communications*) ptr;
	Communications new_sock;
	int client_count = 0;
	while(true){

		if(c_cmd->accept(new_sock)){
			client_count++;
			pthread_t client_thread;

			//creates threads for read and write functionally.
			pthread_create(&client_thread,NULL,client_cmd_read,new ClientWrapper(client_count,new_sock));
      //pthread_create(&client_thread,NULL,client_cmd_write,new ClientWrapper(client_count,new_sock));

		}else{
			std::cerr << "Client failed to connect" << std::endl;
		}
		/*std::cout << "loop thread" << std::endl;
		std::string in;
		if(new_sock->recv(in) < 0 ){
			break;
		}
		std::cout << in;*/
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
							Function reads data from client.
							Binds socket address of the connected client.
							Sends array (packet) to the processData function.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



void *client_cmd_read(void * ptr){

	//creating clientwrapper object.
	ClientWrapper* wrap = (ClientWrapper*) ptr;

	//getting the threadId.
	int id = wrap->threadId;

	//creating communications object.
	Communications client = wrap->o;
	char buffer[1024];
	int size = PACKET_ARRAY_SIZE;
	int longbuf[size];
	while(true){


		struct sockaddr_in addr;

			//binding socket to address.
    		socklen_t addr_size = sizeof(struct sockaddr_in);
    		int res = getpeername(client.m_sock, (struct sockaddr *)&addr, &addr_size);
	//	std::cout << "Reading Commands from Client: " << inet_ntoa(addr.sin_addr) << std::endl;
	//	std::string in;
		//if(client.recv(in) <= 0){
		//	std::cerr << "couldn't read bytes" << std::endl;
		//	break;
		//}
		//if(client.recv(buffer,sizeof(int)) <= 0){
		//	std::cerr << "couldn't read bytes" << std::endl;
		//	break;
		//}


		//client.recv returns an int.
		// recv(void * buffer, size_t len)

/*

		if(client.recv(&longbuf,size*sizeof(int)) <= 0){
			std::cerr << "couldn't read bytes" << std::endl;
			break;
		}
*/

    int result = read(client.m_sock, buffer, 1024);

/*
    for(int i = 0; i< 20; i++)
    {
      std::cout << buffer[i];
    }
    std::cout << std::endl;

/*
    std::string message = "h";

    if(client.recv(message) <= 0){
      std::cerr << "couldn't read bytes" << std::endl;
      break;
    }
*/


		processData(buffer);
	//	std::cout << in << std::endl;
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
				Function sends data to the client.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



void *client_cmd_write(void * ptr){

	ClientWrapper* wrap = (ClientWrapper*) ptr;

	//getting threadId.
	int id = wrap->threadId;

	//creating communications object.
	Communications client = wrap->o;
	while(true){


		struct sockaddr_in addr;

			//binding socket to address.
    		socklen_t addr_size = sizeof(struct sockaddr_in);
    		int res = getpeername(client.m_sock, (struct sockaddr *)&addr, &addr_size);

		std::stringstream nat_data;
		char buffer[100];
		snprintf(buffer, 100,"%f %f %f %f",eth0_usage_tx, eth0_usage_rx,wlan_usage_tx,wlan_usage_rx);
		nat_data << "INFO - NET ";
		nat_data << buffer;
		//std::cout << "Writing data to Client: " << inet_ntoa(addr.sin_addr) << std::endl;
		//std::cout << nat_data.str()<< std::endl;
		//std::string in;
		//if(client.send(nat_data.str()) <= 0){
		//	std::cerr << "couldn't write bytes" << std::endl;
		//	break;
		//}
		struct sysinfo memInfo;

		sysinfo (&memInfo);
		long long physMemUsed = memInfo.totalram - memInfo.freeram;
    float ram_usage = (((float)physMemUsed/(float)memInfo.totalram)*100);
//Multiply in next statement to avoid int overflow on right hand side...
		physMemUsed *= memInfo.mem_unit;

		std::stringstream sys_data;
		//snprintf(buffer, 100,"%f %f",getCurrentCPUValue(),ram_usage);
		//sys_data << "INFO - SYS ";
		//sys_data << buffer;
		//std::cout << sys_data.str()<< std::endl;
	//	if(client.send(sys_data.str()) <= 0){
	//		std::cerr << "couldn't write bytes" << std::endl;
	//		break;
	//	}
		int packet[PACKET_ARRAY_SIZE];

		//prepares packet with data from the core?.
		packet[0] = htonl(INFO);
		packet[1] = NET;
		packet[2] = eth0_usage_tx;
		packet[3] = eth0_usage_rx;
		packet[4] = wlan_usage_tx;
		packet[5] = wlan_usage_rx;
		int ibuf[1];
            	ibuf[0] = 6;

		//std::cout << total_pack << std::endl;
	//	snprintf ( buffer, 10, "%d", buff.size());
		//if (client.send(ibuf, sizeof(int)*1) == 0){
                 //    	std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                  //   	break;
             //   }//

		//sends packet.
		if (client.send(&packet, sizeof(int)*PACKET_ARRAY_SIZE) == 0){
                     	std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                     	break;
        }

		//prepares packet with data about server system resources.
		float cpu_usage = getCurrentCPUValue();
		packet[0] = htonl(INFO);
		packet[1] = SYS;
		packet[2] = cpu_usage;
		packet[3] = ram_usage;

		//std::cout << "CPU: " << cpu_usage << std::endl;
		//if (client.send(ibuf, sizeof(int)) == 0){
             //        	std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
             //        	break;
             //   }

		//sends packet.
		if (client.send(&packet[0], sizeof(int)*PACKET_ARRAY_SIZE) == 0){
                     	std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                     	break;
        }

		//prepares packet with data about GPS.
		packet[0] = htonl(INFO);
		packet[1] = GPS;
		packet[2] = lat;
		packet[3] = lon;


		//sends packet.
		if (client.send(&packet[0],  sizeof(int)*PACKET_ARRAY_SIZE) == 0){
                     	std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                     	break;
        }

		//prepares packet with data about Rover orientation?.
		packet[0] = htonl(INFO);
		packet[1] = IMU;
		packet[2] = roll;
		packet[3] = pitch;
		packet[4] = yaw;


		//sends packet.
		if (client.send(&packet[0],  sizeof(int)*PACKET_ARRAY_SIZE) == 0){
                     	std::cerr << "Failed to send bytes, assuming client has disconnected" << std::endl;
                     	break;
        }


		roll++;
		usleep(10000);
	}
	std::cout << "Exiting Thread\n";
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
				Function does something.

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






void *network_listener(void *){
	while(true){
		int wlan_tx_a,wlan_tx_b,wlan_rx_a,wlan_rx_b,eth0_tx_a,eth0_tx_b,eth0_rx_a,eth0_rx_b;
		FILE* file;
		file = fopen("/sys/class/net/eth0/statistics/rx_bytes", "r");
                if(file != NULL){
                    fscanf(file, "%d", &eth0_rx_a);
                    fclose(file);
                }
		file = fopen("/sys/class/net/eth0/statistics/tx_bytes", "r");
                if(file != NULL){
                    fscanf(file, "%d", &eth0_tx_a);
                    fclose(file);
                }

		file = fopen("/sys/class/net/wlan0/statistics/rx_bytes", "r");
		if(file != NULL){
    			fscanf(file, "%d", &wlan_rx_a);
    			fclose(file);
		}
		file = fopen("/sys/class/net/wlan0/statistics/tx_bytes", "r");
		if(file != NULL){
    			fscanf(file, "%d", &wlan_tx_a);
    			fclose(file);
		}
		sleep(1);


		file = fopen("/sys/class/net/eth0/statistics/rx_bytes", "r");
		if(file != NULL){
			fscanf(file, "%d", &eth0_rx_b);
    			fclose(file);
		}
		file = fopen("/sys/class/net/eth0/statistics/tx_bytes", "r");
		if(file != NULL){
    			fscanf(file, "%d", &eth0_tx_b);
    			fclose(file);
		}

		file = fopen("/sys/class/net/wlan0/statistics/rx_bytes", "r");
		if(file != NULL){
    			fscanf(file, "%d", &wlan_rx_b);
    			fclose(file);
		}
		file = fopen("/sys/class/net/wlan0/statistics/tx_bytes", "r");
		if(file != NULL){
			fscanf(file, "%d", &wlan_tx_b);
	    		fclose(file);
		}
		eth0_usage_tx = (eth0_tx_b-eth0_tx_a)/1024.0;
		eth0_usage_rx = (eth0_rx_b-eth0_rx_a)/1024.0;

		wlan_usage_tx = (wlan_tx_b-wlan_tx_a)/1024.0;
		wlan_usage_rx = (wlan_rx_b-wlan_rx_a)/1024.0;

	}
}
