//Author - Luis Najera 

//Editors -
// Ivan Leontiev
//
//
//
//

#include "utils/ConfigLoader.h"
using namespace std;

ConfigLoader::ConfigLoader()
{
}

ConfigLoader::ConfigLoader(string fileName)
{
	load(fileName);
}


string ConfigLoader::get(string key)
{
	map<string, string>::const_iterator it = m.find(key);

	if (it == m.end())
	{
		cout << "ERROR: No value could be found.";
		return "";
	}

	return it->second;
}

void ConfigLoader::add(string key, string value)
{
	m[key] = value;
}

void ConfigLoader::erase(string key)
{
	m.erase(key);
}

void ConfigLoader::load(string fileName)
{
	file.open(fileName.c_str());
	string a, b;

	while (file >> a >> b)
	{
	   m.insert(pair<string, string>(a, b));
	}
	file.close();

	for(map<string, string>::const_iterator it = m.begin(); it != m.end(); ++it)
	{
		cout << it->first << " " << it->second << "\n";
	}
}

void ConfigLoader::load(string fileName, bool clear)
{
	if(clear)
		m.clear();
	load(fileName);
}

void ConfigLoader::save(string fileName)
{
	file.open(fileName.c_str());

	for(map<string, string>::const_iterator it = m.begin(); it != m.end(); ++it)
	{
		file << it->first << " " << it->second << "\n";
	}

	file.close();
}
/*
int main()
{
	ConfigLoader config;
	config.load("config.txt");
	cout << config.get("NotLoaded") << "\n";
	cout << config.get("Loaded") << "\n";
	config.add("PowerLevel", "9001");
	config.add("Delete", "This");
	config.erase("Delete");
	config.load("config.txt", 1);
	config.save("config.txt");
	cout << "End\n";
	return 0;
}
*/
