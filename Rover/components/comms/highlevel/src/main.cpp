/*
	Editors: Donny

	Last edit: Donny
	Reason: Creation
*/
#include "server/TServer.h"

int port = 4060;
std::string arduino_port = "/dev/ttyACM0";
bool is_logging = false; //To create a log of the server

void setup(int argc, char** argv)
{
	switch(argc)
	{
		case 1:
			break;
		case 2:

		case 3:

		case 4:

		default:
			break;
	}
}

int main(int argc, char** argv)
{
	setup(argc, argv);

	try
	{
		TServer* server = new TServer(port, arduino_port);

		server->Start();
	}
	catch(const char* e)
	{
		std::cout << e << std::endl;
	}

	return 0;
}
