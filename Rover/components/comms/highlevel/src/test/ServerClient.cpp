#include "server/Server.h"
#include <iostream>

int sock = 0, valread;
struct addrinfo hints, *servinfo, *p;
char s[INET6_ADDRSTRLEN];
int status = 0;
byte buffer[pak::MAX];
bool gotit = false;

void setup()
{
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	//127.0.0.1
	//192.168.1.21
	if ((status = getaddrinfo("127.0.0.1","4060", &hints, &servinfo)) != 0)
	{
		cout << "getaddrinfo error!" << endl;
	}

	if ((sock = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol)) == -1)
	{
		cout << "unabled to create socket" << endl;
	}

	if ((connect(sock, servinfo->ai_addr, servinfo->ai_addrlen)) != -1)
	{
		cout << "Connection successful" << endl;
	}
	freeaddrinfo(servinfo);
}

void tryRead()
{
	gotit = false;
	read(sock, buffer, pak::MAX);
	gotit = true;
}

int main()
{
	setup();

	Payload* out = new Payload(64);
	out->push(1);

	//Structure used to make sure things are sent across
	while(valread <= 0)
	{
		out->getPacket(buffer, out->getPacketSize());
		send(sock, buffer, out->getPacketSize(), 0);

		valread = 0;
		buffer[0] = 0;
		if (valread = read(sock, buffer, pak::MAX) > 0)
		{
			cout << valread << endl;
			out->loadPacket(buffer, pak::MAX);
			cout << *out << endl;
		}
	}

	out->getPacket(buffer, out->getPacketSize());
	send(sock, buffer, out->getPacketSize(), 0);

	buffer[0] = 0;
	if (valread = read(sock, buffer, pak::MAX) > 0)
	{
		cout << valread << endl;
		out->loadPacket(buffer, pak::MAX);
		cout << *out << endl;
	}
	cout << "Client Complete" << endl;

	return 0;
