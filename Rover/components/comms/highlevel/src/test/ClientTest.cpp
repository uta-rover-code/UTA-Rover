/*
	Editors: Donny

	Last edit: Donny
	Reasons: Testing Server
*/
#include "server/TServer.h"
#include "server/DataHandler.h"
#include <iostream>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <thread>

using namespace std;

int sock = 0, valread;
struct addrinfo hints, *servinfo, *p;
char s[INET6_ADDRSTRLEN];
int status = 0;

void setup()
{
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	//127.0.0.1
	//192.168.1.21
	if ((status = getaddrinfo("127.0.0.1","4060", &hints, &servinfo)) != 0)
	{
		cout << "getaddrinfo error!" << endl;
	}

	if ((sock = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol)) == -1)
	{
		cout << "unabled to create socket" << endl;
	}

	if ((connect(sock, servinfo->ai_addr, servinfo->ai_addrlen)) != -1)
	{
		cout << "Connection successful" << endl;
	}
	freeaddrinfo(servinfo);
}

int main()
{
	setup();

	byte buffer[MAX_BYTES];
	std::vector<byte> send_bytes;
	std::vector<byte> recv;

	send_bytes.push_back(1);

	serialize(send_bytes, buffer, MAX_BYTES);
	send(sock, buffer, MAX_BYTES, 0);

	valread = read(sock, buffer, MAX_BYTES);
	recv = deserialize(buffer, MAX_BYTES);

	send_bytes.clear();
	send_bytes.push_back(0xAB);
	serialize(send_bytes, buffer, MAX_BYTES);
	send(sock, buffer, MAX_BYTES, 0);


	read(sock, buffer, MAX_BYTES);
	recv = deserialize(buffer, MAX_BYTES);
	std::cout << (int)recv.size() << std::endl;

	cout << "Client Recieved: ";
	for (int i = 0; i < recv.size(); i ++)
	{
		cout << std::hex << (int)recv[i] << " ";
		if (i + 1 == recv.size())
		{
			cout << std::dec << endl;
		}
	}

	cout << "Client Complete" << endl;



	return 0;
}
