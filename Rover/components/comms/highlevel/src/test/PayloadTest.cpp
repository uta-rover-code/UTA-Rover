#include "server/Payload.h"
#include <iostream>
#include <vector>

using namespace std;

int main()
{
  Payload x = Payload(64);
  cout << x << endl;
  x.push(0xBC);
  x.push(0xAA);
  x.push(0xDC);
  cout << x << std::endl;

  byte buffer[64] = {};
  int size = x.getPacket(buffer, x.getPacketSize());
  for (int i = 0; i < size; i++)
  {
    cout << hex << (int)buffer[i] << " ";
  }
  cout << endl;
  x.clear();
  cout << x << endl;

  buffer[0] = 0xaa;
  buffer[1] = 0x06;
  buffer[2] = 0x05;
  buffer[3] = 0x06;
  buffer[4] = 0x07;
  buffer[5] = 0xa8;
  bool check = x.loadPacket(buffer, 64);
  check ? cout << x << endl : cout << "failed" << endl;

	return 0;
}
