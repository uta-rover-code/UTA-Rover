#include "server/DataHandler.h"
#include <iostream>

using namespace std;

void test_serialize(vector<byte> data)
{
  cout << "Serialize Test: " << endl;

  byte buffer[MAX_BYTES];

  serialize(data, buffer, MAX_BYTES);

  int current_size = data.size() + OVERHEAD_BYTES;
  for (int i = 0; i < current_size; i ++)
  {
    cout << std::hex << (int)buffer[i] << " ";
    if (i + 1 == current_size)
    {
      cout << std::dec << endl;
    }
  }

  cout << endl;
}

void test_deserialize(byte arr[], int arr_size)
{
  cout << "Deserialize Test:" << endl;

  vector<byte> out = deserialize(arr, arr_size);

  for (int i = 0; i < out.size(); i ++)
  {
    cout << std::hex << (int)out[i] << " ";
    if (i + 1 == out.size())
    {
      cout << std::dec << endl;
    }
  }
  cout << endl;
}

void test_add_float(float val)
{
  cout << "Add Float Test:" << endl;

  vector<byte> out;
  add_float(out, val);

  for (int i = 0; i < out.size(); i ++)
  {
    cout << std::hex << (int)out[i] << " ";
    if (i + 1 == out.size())
    {
      cout << std::dec << endl;
    }
  }

  fbytes convert;
  for(int i = 0; i < sizeof(float); i++)
  {
    convert.array[i] = out[i];
  }
  cout << convert.num << endl;
  cout << endl;
}

int main()
{
  vector<byte> serialize_vector;
  serialize_vector.push_back(1);
  serialize_vector.push_back(2);
  serialize_vector.push_back(3);
  test_serialize(serialize_vector);

  byte deserialize_arr[6];
  deserialize_arr[0] = START_BYTE;
  deserialize_arr[1] = 6;
  deserialize_arr[2] = 2;
  deserialize_arr[3] = 4;
  deserialize_arr[4] = 6;
  deserialize_arr[5] = calculate_checksum(deserialize_arr, 6);
  test_deserialize(deserialize_arr, 6);

  float add_float_num = 100.25;
  test_add_float(add_float_num);

	return 0;
}
