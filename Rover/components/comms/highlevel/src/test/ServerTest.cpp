#include "server/Server.h"

#include <thread>

using namespace std;


int main (int args, char** argv)
{
  Server* server = new Server(4060);
  std::thread server_thread = std::thread(&Server::Start(), server);

  Payload* in = new Payload(64);
  in->push(0x01);
  while (true)
  {
      cout << *(server->getMsg()) << endl;
      server->tellMsg(in);
  }

  server_thread.join();
  return 0;
}
