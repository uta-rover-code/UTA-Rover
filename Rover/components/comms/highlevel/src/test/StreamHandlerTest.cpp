#include "server/StreamHandler.h"
#include <iostream>
#include <vector>

using namespace std;

int main()
{
  byte stream[] = { 0x04, 0x02, 0xac, 0x04, 0xaa, 0x04, 0x20, 0x01, 0x03, 0xaa,
                    0x05, 0x01, 0x02, 0xac, 0x01, 0xaa };

  StreamHandler sh (64);

  sh.load(stream, 16);
  sh.load(stream, 16);
  sh.load(stream, 16);

  while (sh.hasPackets())
    cout << *(sh.next()) << endl;

	return 0;
}
