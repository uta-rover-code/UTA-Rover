#include "server/IOServer.h"
#include <iostream>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <thread>

using namespace std;

int sock = 0, valread;
struct addrinfo hints, *servinfo, *p;
char s[INET6_ADDRSTRLEN];
int status = 0;
byte buffer[pak::MAX];
bool gotit = false;

void setup()
{
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	//127.0.0.1
	//192.168.1.21
	if ((status = getaddrinfo("127.0.0.1","4060", &hints, &servinfo)) != 0)
	{
		cout << "getaddrinfo error!" << endl;
	}

	if ((sock = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol)) == -1)
	{
		cout << "unabled to create socket" << endl;
	}

	if ((connect(sock, servinfo->ai_addr, servinfo->ai_addrlen)) != -1)
	{
		cout << "Connection successful" << endl;
	}
	freeaddrinfo(servinfo);
}

void recving()
{
	while (true)
	{
		Payload* out = new Payload(64);
		int valread = 0;
		if (valread = read(sock, buffer, pak::MAX) > 0)
		{
			cout << valread << endl;
			out->loadPacket(buffer, pak::MAX);
			cout << *out << endl;
		}
	}
}

int main()
{
	setup();

	Payload* out = new Payload(64);
	out->push(1);

	//Structure used to make sure things are sent across
	while(valread <= 0)
	{
		out->getPacket(buffer, out->getPacketSize());
		send(sock, buffer, out->getPacketSize(), 0);

		valread = 0;
		buffer[0] = 0;
		if (valread = read(sock, buffer, pak::MAX) > 0)
		{
			cout << valread << endl;
			out->loadPacket(buffer, pak::MAX);
			cout << *out << endl;
		}
	}

	std::thread Recving = std::thread(&recving);

	out->clear();
	out->push(0xab);
	while (true)
	{
		out->getPacket(buffer, out->getPacketSize());
		send(sock, buffer, out->getPacketSize(), 0);
	}
	Recving.join();

	cout << "Client Complete" << endl;

	return 0;
}
