
#include "server/TServer.h"
#include <iostream>
#include <vector>
#include <unistd.h>

using namespace std;

int port = 4060;
std::string arduino_port = "/dev/ttyACM0";

TServer* server;

int main()
{
  try
  {
    server = new TServer(port, arduino_port);
    std::vector<byte> out;
    out.push_back(0xAA);
    server->add_recv_packet(out);

    out.clear();
    out.push_back(0xAB);

    server->Start();
  }
  catch(const char* e)
  {
    std::cout << e << std::endl;
  }

	return 0;
}
