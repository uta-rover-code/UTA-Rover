/*
	Editors: Donny

	Last edit: Donny
	Reason: Creation
*/


#include "server/TArduino.h"
#include <vector>
#include <thread>
#include <unistd.h>

using namespace std;

TArduino* arduino;

void sending()
{
	sleep(4);
	std::vector<byte> out;
	out.push_back(0xAB);
	for (int i = 0; i < 4; i++)
	{
		sleep(3);
		arduino->add_send_packet(out);
	}
}

void recving()
{
	sleep(3);
	std::vector<byte> out;
	out.push_back(1);
	for(int i = 0; i < 4; i++)
	{
		sleep(2);
		arduino->add_recv_packet(out);
	}
}

int main()
{
	try
	{
		arduino = new TArduino(NULL, "/dev/ttyACM0");

		std::vector<byte> out;
		out.push_back(0xAB);
		arduino->add_send_packet(out);

		std::thread x = std::thread(sending);
		std::thread y = std::thread(recving);
		arduino->Start();
		x.join();
		y.join();
	}
	catch(const char* e)
	{
		cout << e << endl;
	}
	return 0;
}
