#include "server/IOServer.h"

#include <iostream>

using namespace std;


int main (int args, char** argv)
{
  IOServer* server = new IOServer(4060);
  StreamHandler* out = new StreamHandler(64);
  server->Start();

  Payload* in = new Payload(64);
  in->push(0x01);

  server->Recv(out);
  while(out->hasPackets())
  {
    cout << *(out->next()) << endl;
  }

  server->Send(in);

  server->Recv(out);
  while(out->hasPackets())
  {
    cout << *(out->next()) << endl;
  }

  return 0;
}
