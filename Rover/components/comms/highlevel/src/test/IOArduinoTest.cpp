#include "server/IOArduino.h"
#include <thread>

using namespace std;

IOArduino* arduino = new IOArduino("/dev/ttyACM0");
StreamHandler* out = new StreamHandler(64);
bool check = false;

void sending()
{
  Payload* in = new Payload(64);
  in->push(0xab);
  int check = 0;

  for (int i = 0; i < 20; i++)
    arduino->Send(in);
}

int main(int args, char** argv)
{
  std::thread sthread = std::thread(sending);

  while(!out->hasPackets())
  {
    arduino->Recv(out);
    while(out->hasPackets())
      cout << *(out->next()) << endl;
  }
  sthread.join();
  return 0;
}
