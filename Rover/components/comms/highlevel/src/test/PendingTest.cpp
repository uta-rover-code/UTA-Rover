#include "server/Pending.h"
#include <iostream>
#include <stdlib.h>

using namespace std;


void print(Payload* in)
{
  int size = in->getSize() + pak::OVERHEAD;
  byte buff[size] = {};

  size = in->getPacket(buff, size);

  for (int i = 0; i < size; i++)
  {
    cout << hex << (int)buff[i] << "\t";
  }
  cout << endl;
}

int main()
{
  Pending q = Pending(4);

  Payload* x = new Payload(64);
  x->push(10);

  q.push(x);
  cout << *(q.pop()) << endl;

  for (int i = 0; i < 32; i++)
  {
    if (!q.isFull())
    {
      Payload* temp = new Payload(64);
      int limit = rand()%16 + 1;
      for (int i = 0; i < limit; i++)
        temp->push(rand()%255);
      q.push(temp);
    }

    if (q.isFull())
      print(q.pop());
  }

	return 0;
}
