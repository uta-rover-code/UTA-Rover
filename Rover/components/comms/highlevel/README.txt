The best way to learn what the codes does (especially in the big picture) is
to look at the header files (.h) first. Once you learn what the class does
through the header, you can reuse the code it to make your life easier. \(^o^)/

Look at the test files for classes to see how they are used to help you learn
how to use the code. \(^o^)/  (*Much Helps*)  \(^o^)/

The only times when you need to look at the implementation files (.cpp) is to
fix bugs or implement a new feature to the class.

The vision for the server project is to create a friendly encapsulation of
lower level details and provide a dummy proof interface to it. This will
ensure future ease in adding new features to the rover without any worry
about past implementations.

Multithreading notes:
Threads should be used in similar situations like:
  #Splitting up computational work (factorials, math functions)
  #Preventing CPU blocks (server waiting for connection)
  #Increasing response time while still running (GUI, games)
Threads can greatly speed up to slow down your application and anything in
between so use wisely as they are limited resources.
