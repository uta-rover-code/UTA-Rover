//Author - Ivan Leontiev

//Editors -
//
//
//
//
//

#ifndef DEFS_MANAGER_H_
#define DEFS_MANAGER_H_

#include "global_defs.h"
class Definiton_Manager {
public:
	Definiton_Manager();
	virtual ~Definiton_Manager();
	loadConfig();
	saveConfig();
	getProperty();
	setProperty(int var,int value);
private:
	
};

#endif
