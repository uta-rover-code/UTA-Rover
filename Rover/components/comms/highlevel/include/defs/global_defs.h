//Author - Ivan Leontiev

//Editors -
//
//
//
//
//

#ifndef GLOBAL_DEFS_H_
#define GLOBAL_DEFS_H_

enum Global_Definitions{
	NORMAL = 0,
	ALERT = 1,
	ERROR = 2,
	PACKET_SIZE= 4096,
	PACKET_ARRAY_SIZE = 15,
	CAMERA_PORT =  4050,
	CONTROL_PORT = 4060,
	JOYSTICK_MAX = 32767,
	JOYSTICK_MIN = -32767,
	MAXHOSTNAME = 200,
	MAXCONNECTIONS = 5,
	MAXRECV = 700,

	MAXQUEUE = 100,
	INFO = 0xAA,
	NET = 0xAB,
	SYS = 0xAC,
	GPS = 0xAD,
	IMU = 0xAE,

	CMD = 0xBA,
	CAM = 0xBB,
	DRV = 0xBC,
	ARM_CONTROL = 0,
	SAR_CONTROL = 0,

	IMG = 0xCA,
	FPS = 0xCB,
	QLT = 0xCC,
	RES = 0xCD,
	UIS = 0xCE,

	AUT = 0xDA,
	STP = 0xDB,



	START			= 0xAA,
	/* Arduino Reporting Commands */
	SYS_INFO		= 0xAB,	// Ultrasound Sensors, GPS, IMU, Temperature, *Fan Speed, Kit data
//	CAM				= 0xAC,
//	* Denotes toggled debug data
	/* Arduino Pinging the Pi */
	COMM_PING		= 0xFA, // Just a comms check, can trigger a NOACK or REACK event
							//		Arduino writes (COMM_PING), Pi responds with (COMM_PING)

	/* Raspi Issuable Commands */
	AUTOPILOT		= 0xBA,
	DRIVE			= 0xBB,
	GPS_LOCATION		= 0xBC,
	DRILL			= 0xBD,
	ARM 			= 0xBE,
	RESET			= 0x00
};

extern int CAMERA_WIDTH;
extern int CAMERA_HEIGHT;
extern int CAMERA_FPS;
extern int SELECTED_CAMERA;
extern int CAMERA_QUALITY;
//};

#endif
