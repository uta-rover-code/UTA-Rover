#ifndef __IO_H
#define __IO_H

#include "Payload.h"
#include "StreamHandler.h"


class IO
{
public:
  IO() { };
  ~IO() { };

  virtual int Send(Payload* in) = 0;
  virtual int Recv(StreamHandler* out) = 0;
};
#endif
