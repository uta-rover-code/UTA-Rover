//Author - Ivan Leontiev

//Editors -
//
//
//
//
//

#ifndef _ARDUINO__H
#define _ARDUINO__H
#include <vector>
#include <iostream>
#include "defs/packet_def.h"

class Arduino {
public:

	Arduino(){};
	Arduino(std::string port);
	int writePacket(std::vector<byte> buff);
  int writePacket(void *buff, int size);
	int writePacket ( const std::string s );
  int readPacket(void *buff);
	int readPacket ( std::string& s );
	int initPort();
	int IsOpen();

private:
	int ser_dev = 0;
	int fd;
	int baud = 0;
};

#endif
