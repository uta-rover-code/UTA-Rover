#ifndef __SERVER_H
#define __SERVER_H

#include "IOServer.h"
#include "StreamHandler.h"
#include "Transceiver.h"


class Server : public Transceiver
{
public:
  Server(int port);

  void Start();

protected:
  void Sending();
  void Receiving();

private:
  IOServer* io;
  StreamHandler* stream;
};
#endif
