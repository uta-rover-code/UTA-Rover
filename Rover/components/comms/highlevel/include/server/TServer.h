#pragma once

#include "TArduino.h"

#include <netinet/in.h>
#include <thread>
#include <mutex>

class TServer : public ITransceiver
{
public:
  TServer(int port, std::string aport);

  void Start();

protected:
  void process_client(std::vector<byte> input);
  void process_arduino(std::vector<byte> input);

  void Sending();
  void Receiving();

  //TArduino* arduino;
  std::thread arduino_process;
  std::mutex client_lock;

  int server_fd;
  int client_fd;
  struct sockaddr_in address;
  int addrlen;
};
