/*
  NOTE: Transceivers will CPU block your current thread

*/
#pragma once

#include "DataHandler.h"
#include <queue>
#include <vector>
#include <thread>

class ITransceiver
{
public:
  ITransceiver();
  ~ITransceiver();
  virtual void Start() = 0;

  void add_send_packet(std::vector<byte> input);
  void add_recv_packet(std::vector<byte> input);

  void add_child(ITransceiver* device);

  void tell_parent(std::vector<byte> input);
  void tell_children(std::vector<byte> input);
protected:
  virtual void Sending() = 0;
  virtual void Receiving() = 0;

  bool is_running;
  std::queue<std::vector<byte>> sending_packets;
  std::queue<std::vector<byte>> recving_packets;

  ITransceiver* parent;
  std::vector<ITransceiver*> children;

  std::thread sending;
};
