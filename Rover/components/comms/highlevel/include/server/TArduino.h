#pragma once

#include "ITransceiver.h"
#include "server/Arduino.h"
#include <string>

class TArduino : public ITransceiver
{
public:
  TArduino(ITransceiver* parent, std::string port);
  ~TArduino();

  void Start();

protected:
  void process(std::vector<byte> input);

  void Sending();
  void Receiving();

  Arduino* device;
};
