#ifndef __SERIAL_H
#define __SERIAL_H

#include "IOArduino.h"
#include "StreamHandler.h"
#include "Transceiver.h"

#include <string>


class Serial : public Transceiver
{
public:
  Serial(std::string port);

  void Start();

protected:
  void Sending();
  void Receiving();

private:
  IOArduino* io;
  StreamHandler* stream;
};
#endif
