#ifndef __PENDING_H
#define __PENDING_H

#include "Payload.h"

#include <deque>


class Pending
{
public:
  Pending() { max_size = 16; }
  Pending(int size) : max_size(size) { }

  Payload* pop();
  void push(Payload* in);

  bool isEmpty() { return p_queue.empty(); }
  bool isFull() { return (p_queue.size() >= max_size) ? true : false; }
  int getSize() { return p_queue.size(); }


protected:
  std::deque<Payload*> p_queue;
  std::vector<byte> memory;
  int max_size;
};
#endif
