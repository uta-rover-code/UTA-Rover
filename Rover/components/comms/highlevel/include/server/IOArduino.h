#ifndef __IOSERIAL_H
#define __IOSERVER_H

#include "IO.h"
#include "Arduino.h"

#include <string>
#include <fstream>


class IOArduino : public IO
{
public:
  IOArduino(std::string port) { init(port); }
  ~IOArduino() { }

  int Send(Payload* in);
  int Recv(StreamHandler* out);

  bool isAlive() { return (arduino.IsOpen() > 2); }
  void reconnect();

protected:
  Arduino arduino;
  std::string port;

private:
  void init(std::string port);
};
#endif
