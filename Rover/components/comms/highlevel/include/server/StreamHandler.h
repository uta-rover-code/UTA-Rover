#ifndef __StreamHandler_H
#define __StreamHandler_H

#include "Pending.h"
#include "Payload.h"
#include <vector>


class StreamHandler
{
public:
  StreamHandler(int max_size);

  Payload* next();
  void load(byte* in, int in_size);

  bool hasPackets() { return !pending.isEmpty(); }

protected:
  Pending pending;
  std::vector<byte> buffer;

private:
  void process();
};
#endif
