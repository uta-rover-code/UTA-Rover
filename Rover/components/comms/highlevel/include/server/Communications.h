/*
  Editors: Ivan, Donny

  Last edit: Donny
  Reason: Clarifying some elements

  An interface to sockets

  Not currently in use
*/

#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>
//#include "../defs/global_defs.h"

#define MAXCONNECTIONS 5
#define MAXRECV 700

class Communications {
public:
  Communications();
  virtual ~Communications();

  // Server initialization
  bool create();
  bool bind ( const int port );
  bool listen() const;
  bool accept ( Communications& new_Communications) const;

  // Client initialization
  bool connect ( const std::string host, const int port );

  // Data Transimission
  bool send ( const std::string s);
  bool send (const void *buf, size_t len);
  int recv ( std::string &s );
  int recv ( void *buf, size_t len );


  void set_non_blocking ( const bool b);
  bool close();
  bool is_valid() const { return m_sock != -1; }


  int m_sock;
  sockaddr_in m_addr;


};
