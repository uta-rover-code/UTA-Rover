#ifndef __TRANSCEIVER_H
#define __TRANSCEIVER_H

#include "Pending.h"

#include <thread>


class Transceiver
{
public:
  Transceiver() { is_running = true; }
  ~Transceiver();
  virtual void Start() = 0;

  void tellMsg(Payload* in);
  Payload* getMsg();

  bool isRunning() { return is_running; }

protected:
  virtual void Sending() = 0;
  virtual void Receiving() = 0;

  std::thread sending;

  bool is_running;
  Pending in;
  Pending out;
};
#endif
