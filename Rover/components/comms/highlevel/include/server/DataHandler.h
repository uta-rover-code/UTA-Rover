#pragma once

#include <vector>
#include "defs/packet_def.h"

union fbytes
{
  float num;
  byte array[sizeof(float)];
};

void serialize(std::vector<byte> data, byte buffer[], int buffer_size);
std::vector<byte> deserialize(byte buffer[], int buffer_size);
void add_float(std::vector<byte> &data, float val);
byte calculate_checksum(byte buffer[], int buffer_size);
