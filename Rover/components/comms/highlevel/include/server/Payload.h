/*
  Max size is in regards of max size of a packet
*/

#ifndef __PAYLOAD_H
#define __PAYLOAD_H

#include "Packet.h"

#include <iostream>
#include <vector>

class Payload
{
public:
  Payload(byte max_size);
  Payload(byte* in, int in_size, byte max_size);
  ~Payload();

  int getPacket(byte* out, int out_size);
  bool loadPacket(byte* in, int in_size);

  void push(byte val);
  void clear() { vector.clear(); }

  byte& operator[] (int i);
  const byte& operator[] (int i) const;

  int getSize() const { return vector.size(); }
  int getPacketSize() const { return vector.size() + pak::OVERHEAD; }
  int getMaxsize() const { return max_size; }

  friend std::ostream& operator<<(std::ostream& os, const Payload& p);

protected:
  std::vector<byte> vector;
  byte max_size;
};
#endif
