#ifndef __IOSERVER_H
#define __IOSERVER_H

#include "IO.h"
#include "Payload.h"

#include <netinet/in.h>
#include <string>


class IOServer : public IO
{
public:
  IOServer(int port) { init(port); }
  ~IOServer();

  void Start();

  int Send(Payload* in);
  int Recv(StreamHandler* out);

protected:
  struct sockaddr_in address;
  fd_set readfds;

  int client = 0;
  int server = 0;

private:
  void init(int port);
  void setActivity();
  void handleNewClient();
  void printClientMsg(int sock, std::string msg);
};
#endif
