#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include "SoftwareSerial.h"
#include "global_defs.h"
#include "Packet.h"

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

#define MIN_PULSE_WIDTH 650
#define MAX_PULSE_WIDTH 2350
#define DEFAULT_PULSE_WIDTH 1500
#define FREQUENCY 50

static byte ReadBuffer [64]; // The Arduino only has a 64 byte buffer
byte MotorState [] = {127, 127};

int pulseWidth(int angle) {
  int pulse_wide, analog_value;
  pulse_wide = map(angle, 0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH);
  analog_value = int(float(pulse_wide) / 1000000 * FREQUENCY * 4096);
  // Serial.println(analog_value);
  return analog_value;
}

bool ParseComms (){
    int PayloadSize = 0;
    if (LookForPacket (ReadBuffer) == 0) // If this channel has no data coming in...
        return false; //  ...don't waste our time processing.
        
    MotorState[0] = ReadBuffer [1];
    MotorState[1] = ReadBuffer [2];
    return true;
	}

void setup () {
  Serial.begin (115200); // This is 115200 so that the GPS doesn't miss any data
  Serial.flush ();
  pwm.begin();
  pwm.setPWMFreq(FREQUENCY);
}

//                                   --[[ Loop ]]--
void loop() {
    if (ParseComms());
   pwm.setPWM(0, 0, pulseWidth(MotorState[0]));
}
