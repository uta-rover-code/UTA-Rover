#ifndef PACKET_H
#define PACKET_H

#define OverheadBytes 3
#define MaxBytes 255
#define MinBytes OverheadBytes + 1

bool ValidatePacket(size_t packetSize, byte *Packet) {

	byte Checksum = 0x00;

	/* TESTING FOR INVALID PACKET */

	if((packetSize < MinBytes) ||(packetSize > MaxBytes)) // packetSize out of range
		return false;
	else if(Packet[0] != START) // Invalid handshake
		return false;
	else if(Packet[1] != packetSize) // Wrong size
		return false;
	/* TESTING FOR CORRUPT DATA */

	for(int i = 0; i < packetSize - 1; i++)
		Checksum = Checksum ^ Packet[i];
	if(Checksum != Packet[packetSize - 1])
		return false;
	/* TESTING COMPLETE */
	return true;

	}

int LookForPacket(byte *Returnbuffer) {
	size_t count = 0;
	size_t packetSize = MinBytes;
	byte buffer[MaxBytes];
	byte Payload[MaxBytes];

	while(Serial.available()) {	// Changed this from "while(true)" so that the rover won't get caught in an infinite loop.
		if(Serial.available())
				{
				byte RX = Serial.read();
				delay(10);
				//Serial.print("BYTE RECIEVED: ");Serial.println(RX);
				if (count == 0 && RX == START)	// New packet
					{
					buffer [count] = RX;
					count++;
					continue;
					}
				else if(count == 0)	// First byte invalid, ignore and move on.
						continue;
					else if(count == 1)
							{
							buffer[count] = RX;		// This is the packet length
							if((buffer[1] < MinBytes) || (buffer[1] > MaxBytes))	// If the packet is NOT in the correct size range...
									count = 0;			// ...reset the count, throw out the packet, and move on.
								else
										{
										packetSize = RX;	// ...else store the packet size
										count++;
										}
							continue;
							}
						else if(count < packetSize)
								{
								buffer[count] = RX;
								count++;
								}
				if (count >= packetSize)
					if (ValidatePacket (packetSize, buffer))
						{
						for (int i = 2, j = 0; j < (packetSize - OverheadBytes); i++, j++)
							{
							//Serial.print ("PROCESSED: "); Serial.println (buffer [i]);
							Returnbuffer [j] = buffer [i];
							}
						return (packetSize - OverheadBytes);
						}
				}
			else
					{
					Serial.flush();
					delay(5);
					continue;
					}
		}
	return 0;
	}

bool SendPacket(size_t PayloadSize, byte * Payload) {
	if((PayloadSize + OverheadBytes) > MaxBytes)
		return false;

	size_t packetSize = PayloadSize + OverheadBytes;
	static byte Packet[MaxBytes];	// Create the buffer

									/* POPULATE THE BUFFER */
	Packet[0] = START;
	Packet[1] = packetSize;
	byte Checksum = Packet[0] ^ Packet[1];

	for(int i = 0; i < PayloadSize; i++) {
		Packet[i + 2] = Payload[i];
		Checksum = Checksum ^ Packet[i + 2];
		}
	Packet[packetSize - 1] = Checksum;

	/* PACKET COMPLETE */

	Serial.write(Packet, packetSize);
	Serial.flush();
	return true;
	}

#endif /* header guard */
