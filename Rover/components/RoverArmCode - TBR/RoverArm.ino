#include <Wire.h>
#include <math.h>
#include "readPinFast.h"
//#include "Kalman.h"

#define TestingState true

/****************************************************************************************************************************/
//														--[[ Setup ]]--														//
/****************************************************************************************************************************/

#define ArmAddress 0x09							//	I2C Address
#define ArmVars	7								//	This should reflect the number of variable angles
#define CS_Rate .034466912						//	[A / 10-bit unit] (Calculated as (5000 [mV] / 1024 [units]) / ~140 [mV / A] (taken from the motor driver CS data))
#define AngleRate .3703181019					//	[ Degrees / Step ]
#define AngleCorrection -184.1644897			//	[ Degrees ]
#define BaseAngleCorrection -25.06524501		//  [ Degrees ]
#define ShoulderAngleCorrection -131.7445605	//	[ Degrees ]
#define Q2AngleCorrection 19.0					//	[ Degrees ]
#define WristPitchCorrection -194.6709289		//  [ Degrees ]
#define ElbowAngleCorrection -10.99317123		//  [ Degrees ]
#define ShoulderConvertSlope -.4523				//	This was calculated using Linear Regression.
#define ShoulderConvertOffset 286.2				//	This was calculated using Linear Regression.
#define DiffBetweenShoulder 42.9454823			//	[ Degrees ]
#define LAMaxExtension .4191					//	[ Meters ]
#define LAMinExtension .2667					//	[ Meters ]
#define LASensorMaxVal 1023						//	[ Steps ]
#define LASensorMinVal 0						//	[ Steps ]
#define SIZE_T_MAX 65535
#define CalculationTimer .1						//	[ Seconds ]
#define WristTicksPerCycle 8000
#define WristVelConstraint 2000.0
#define WristRotationTopSpeed 40				//	[ PWM ]
#define NumZElements 905

//													--[[ Pin Definitions ]]--

#define BasePWMPin				12
#define ShoulderPWMPin			11
#define ElbowPWMPin				10
#define WristRotatePWMPin		9
#define WristPitchPWMPin		8
#define GripperPWMPin			7
#define FingerPin				40

#define BaseAngleSense			A0	// The sensors have a resolution of about (345 / 1024) = .33691406 [degrees / unit].
#define ShoulderAngleSense		A1	//		So... we multiply the analogRead() value with .33691406 to get an off-center
#define ElbowAngleSense			A2	//		estimate of arm position. To re-center the angle estimate, add 7.5 degrees
#define WristRotateAngleSenseA	3	//		to the estimate. KEEP IN MIND that this re-centers the estimate to the previous
#define WristRotateAngleSenseB	4
#define WristRotateSenseReset	2
#define WristPitchAngleSense	A4	//		link's vector direction and NOT to the ground plane.
#define GripperAngleSense		A5
#define LinearActuatorSense		A6

#define BaseCurrentSense		A8
#define ShoulderCurrentSense	A9
#define ElbowCurrentSense		A10
#define WristRotateCurrentSense	A11
#define WristPitchCurrentSense	A12
#define GripperCurrentSense		A13

#define BasePositive			23
#define ShoulderPositive		25
#define ElbowPositive			26
#define WristRotatePositive		29
#define WristPitchPositive		31
#define GripperPositive			33

#define BaseNegative			22
#define	ShoulderNegative		24
#define	ElbowNegative			27
#define	WristRotateNegative		28
#define	WristPitchNegative		30
#define	GripperNegative			32

double TgtCoords [3] = {1.0, 1.0, .0};		//	This should be initialized to the gripper's starting position in 3D space.
double CurrCoords [3] = {1.0, 1.0, .0};	//	Using Inverse Kinematics, we convert CurrPose angles to a position in 3D space.
volatile double * CurrPose;				//	The arm's current link angles as floats.
volatile double * PrevPose;
volatile double * TempPose;
volatile int TickCount = 1;
String TempString;

//												--[[ Dynamics Definitions ]]--

double KP = .25;		/* Gain Proportionality Constant. Determines how the arm moves {Sluggish <-> Snappy} */
#define G 9.81
#define GR1 .0000947946
#define GR2 -.000102855
#define GR3 .0007271855
#define GR4 .0056137987
#define GR5 .0001404364
#define IA22 0.00398530
#define IB11 0.00060561
#define IB12 -0.00075287
#define IB13 -0.00000021
#define IB22 0.00910391
#define IB23 0.00000009
#define IB33 0.00942240
#define IC11 0.00003252
#define IC12 0
#define IC13 0
#define IC22 0.00382821
#define IC23 0
#define IC33 0.00382836
#define ID11 0.00614450
#define ID12 -0.00350704
#define ID13 -0.00024724
#define ID22 0.06768722
#define ID23 0.00005548
#define ID33 0.07171756
#define IE11 0.04599897
#define IE12 0.00975499
#define IE13 0.01060662
#define IE22 0.03038136
#define IE23 0.02181586
#define IE33 0.02607942
#define IF11 0.00045810
#define IF12 0.00002683
#define IF13 -0.00002021
#define IF22 0.00038729
#define IF23 0.00000847
#define IF33 0.00065883
#define IG11 0.00086274
#define IG12 -0.00074802
#define IG13 0.00686242
#define IG22 0.00723792
#define IG23 -0.00007476
#define IG33 0.00686242
#define IM133 0.00013047
#define IM211 0.00003948
#define IM222 0.00032647
#define IM233 0.00032647
#define IM311 0.00013047
#define IM322 0.00001244
#define IM333 0.00013047
#define IM411 0.00013047
#define IM422 0.00001244
#define IM433 0.00013047
#define IM511 0.00013047
#define IM522 0.00001244
#define IM533 0.00013047
#define L1A .0246
#define L1B .1295
#define L1BASE .0604
#define L1C .1324
#define L1C2 .1253
#define L1D .1779
#define L1E .1127
#define L1F .0408
#define L1G .1533
#define L1L .0925
#define L1M1 .0388
#define L1M2 .0221
#define L1M3 .2400
#define L1M4 .0074
#define L1M5 .0117
#define L1P .1536
#define L1U .0513
#define L1V .0690
#define L1W .3062
#define L1X .3924
#define L1Z .1615
#define L2A .0243
#define L2B .0021
#define L2D .1177
#define L2E .0153
#define L2F .0398
#define L2G .0069
#define L2L .0214
#define L2M1 .0298
#define L2M2 .0305
#define L2M3 .0313
#define L2M4 .0069
#define L2M5 .0320
#define L2P .0193
#define L2S .0788
#define L2U .0135
#define L2V .0359
#define L2W .0833
#define L2X .0833
#define L2Z .0976
#define L3A .000846
#define L3D .00065
#define L3E .0001
#define L3F .00065
#define L3G .0037
#define L3M1 .1349
#define MA 0.6398
#define MB 0.723
#define MC 0.213
#define MD 1.49672418
#define ME 0.81489954
#define MF 0.42208832
#define MG 1.0647
#define MM2 .185
#define MM3 .185
#define MM4 .185
#define MM5 .378
#define TriangleSideA 0.254
#define TriangleSideB 0.2794
#define LinearActuatorMinExtension 0.33517227

// These are the Stall Torques for each motor, taken from the motor's documentation.
#define KT_Base 21.0844
#define KT_Shoulder 1467.7131
#define KT_Elbow 4.8054
#define KT_WristRotation 9.5126
#define KT_WristPitch 21.0844

// These are the Stall Currents for each motor, taken from the motor's documentation.
#define KI_Base 20
#define KI_Shoulder 4
#define KI_Elbow 20
#define KI_WristRotation 20
#define KI_WristPitch 20

// These are the Resistances for each motor, taken from the motor's documentation.
#define R_Base 2.5
#define R_Shoulder 1.8
#define R_Elbow .8
#define R_WristRotation 1.8
#define R_WristPitch 2.5

size_t CurrTime;
size_t PrevTime;
float TimeDifference;

double Q1 = 0.0;	//	Base potentiometer
double Q2 = 0.0;	//	Linear Actuator Tilt
double Q3 = 0.0;	//	Linear Actuator Extension
double Q4 = 0.0;	//	Shoulder Potentiometer
double Q5 = 0.0;	//	Elbow
double Q6 = 0.0;	//	Wrist Rotation
double Q7 = 0.0;	//	Wrist Pitch

double U1 = 0.0;	// base angular velocity [ Rads/sec ]
double U2 = 0.0;	// angular velocity
double U3 = 0.0;	// linear actuator velocity	[ meters/sec ]
double U4 = 0.0;	// angular velocity
double U5 = 0.0;	// angular velocity
double U6 = 0.0;	// wrist (rotation?) angular velocity
double U7 = 0.0;	// angular velocity

double TAU1 = 0.1;
double TAU2 = 0.1;
double TAU3 = 0.1;
double TAU4 = 0.1;
double TAU5 = 0.1;

double Dummy1 = 0.0; 
double Dummy2 = 0.0; 
double Dummy3 = 0.0; 
double Dummy4 = 0.0; 
double Dummy5 = 0.0; 
double Dummy6 = 0.0; 
double Dummy7 = 0.0; 
double Dummy8 = 0.0; 
double Dummy9 = 0.0;
double Dummy10 = 0.0;
double Dummy11 = 0.0;
double Dummy12 = 0.0;

float KV = 2 * pow (KP, .5);
double z [NumZElements];


/****************************************************************************************************************************/
//													--[[ Functions ]]--														//
/****************************************************************************************************************************/

#if TestingState == false
void ReadCommand (int NumBytes)
	{
	float * fTemp;
	int i, j;

	if (Wire.available ())
		for (i = 0; i < 3; i++)	//	3 floats (X, Y, and Z)
			{
			for (j = 0; j < 4; j++)		//	4 bytes per float
				fTemp [j] = Wire.read ();

			TgtCoords [i] = *fTemp;
//			if (i < 2) {Serial.print (TgtCoords[i]); Serial.print (", ");} else Serial.println (TgtCoords [i]);	//	debug code
			}

	return;
	}

void SendArmData()
	{
	byte * Temp;
	for (int i = 0; i < ArmVars; i++)
		{
		Temp = (byte *) & CurrPose [i];
		for (int j = 0; j < 4; j++)	// 4 bytes per float
			Wire.write (Temp [j]);
		}

	return;
	}
#endif

void ManageMotor (int Motor, int PosPin, int NegPin, int VoltageAsPWM)
	{
	digitalWrite (PosPin, LOW);
	digitalWrite (NegPin, LOW);
	if (VoltageAsPWM >= 0)
			digitalWrite (PosPin, HIGH);
		else digitalWrite (NegPin, HIGH);

	analogWrite (Motor, abs (VoltageAsPWM));

	return;
	}

float SenseCurrent (int Pin)
	{
	return analogRead (Pin) * CS_Rate;	// Read CS_Pin, convert units -> Amps
	}

float ReadSensor (int Pin)
	{
	float Temp;

//	Serial.print (Pin); Serial.print (" is reading: "); Serial.println (analogRead (Pin));
	switch (Pin)
		{
		case WristPitchAngleSense:
			Temp = (analogRead (Pin) * AngleRate) + WristPitchCorrection + AngleCorrection;
			break;
		case ShoulderAngleSense:
			Temp = ((analogRead (Pin) * AngleRate) + ShoulderAngleCorrection) * .928255948;		// Corrective factor matching 345* of sensing to 360* of physical rotation
			break;
		case ElbowAngleSense:
			Temp = (analogRead (Pin) * AngleRate) + ElbowAngleCorrection + AngleCorrection;
			break;
		case BaseAngleSense:
			Temp = ((1023 - analogRead (Pin)) * AngleRate) + BaseAngleCorrection + AngleCorrection;
			break;
		default:
			Temp = (analogRead (Pin) * AngleRate) + AngleCorrection;
		}

	if (Temp == -0.0)
			Temp = 0.0;
 		else if (Temp < 0.0)
 				Temp += 360.0;
		
	return Temp;
	}

double ConvertToRadians (double Angle)
	{
	return (Angle * PI / 180.0);
	}

//	[ Meters ]
double LawOfCosinesSide (double SideA, double SideB, double AngleC)
	{
	return (sqrt (pow (SideA, 2.0) + pow (SideB, 2.0) - (2.0 * SideA * SideB * cos (ConvertToRadians (AngleC))))) * 1.13 - .03674;	// These constants were determined using linear regression.
	}

//	[ Radians ]
double LawOfCosinesAng (double SideA, double SideB, double SideC)
	{
	return acos ((pow (SideB, 2.0) + pow (SideC, 2.0) - pow (SideA, 2.0)) / (2 * SideB * SideC));
	}

//	[ Degrees ]
double CalculateTheTriangleAngle (double Angle)
	{
	return -.979 * Angle + 118.1;  // These numbers were calculated using Linear Regression.
	}

double CalculateVelocity (double CurrPos, double PrevPos, double TimeDiff)
	{
//	Serial.print ("Vars: "); Serial. print (CurrPos, 8); Serial.print (", "); Serial.println (PrevPos, 8);
  
	return (CurrPos - PrevPos) / TimeDiff;
	}

void TrackWristRotation ()
	{
	if (readPinFast (WristRotateAngleSenseA) != readPinFast (WristRotateAngleSenseB))	//	Counterclockwise
			if (TickCount >= WristTicksPerCycle)
					TickCount = 0;
				else TickCount++;
		else if (readPinFast (WristRotateAngleSenseA) == readPinFast (WristRotateAngleSenseB))	// Clockwise
				if (TickCount <= 0)
						TickCount = WristTicksPerCycle;
					else TickCount--;
	}

void ResetWristCounter ()
	{
	TickCount = 0;
	CurrPose [4] = 0.0;
	}

bool ExtendFinger ()
	{
	bool FingerState = digitalRead (FingerPin);

	digitalWrite (FingerPin, 1 - FingerState);
	FingerState = digitalRead (FingerPin);
	return FingerState;
	}

int ConvertTorqueToPWM (double Torque, double StallTorque, double StallCurrent, double MotorResistance, double Velocity)
	{
	int Temp = map ((Torque / (StallTorque * MotorResistance)) + (StallCurrent * Velocity), -12, 12, -255, 255);
	if (& Torque == & TAU4)		// If i'm looking at the wrist rotation
			if (Temp >= WristRotationTopSpeed)
					return WristRotationTopSpeed;
				else if (Temp <= -WristRotationTopSpeed)
						return -WristRotationTopSpeed;
	return Temp;
	}


/****************************************************************************************************************************/
//													 --[[ Dynamics ]]--														//
/****************************************************************************************************************************/

void CalculateZConstants ()
	{
	z[1] = 1/GR1;
	z[2] = 1/GR2;
	z[3] = 1/GR3;
	z[4] = 1/GR4;
	z[5] = 1/GR5;
	z[6] = -L1A - L1Z;
	z[11] = L1B + L1C;
	z[14] = L1L - L1A;
	z[19] = L1W + L1X;
	z[20] = L2X - L2W;
	z[25] = L1E + L1V;
	z[26] = L2E - L2V;
	z[42] = L1F + z[25];
	z[50] = L1F + L1U;
	z[51] = L2F + L2U;
	z[68] = z[25] + z[50];
	z[85] = L1B - L1M2;
	z[86] = L2B + L2M2;
	z[88] = L1M3 + L1X;
	z[89] = L2X - L2M3;
	z[91] = L1E + L1M4;
	z[92] = L2E + L2M4;
	z[94] = L1F - L1M5;
	z[95] = L2F + L2M5;
	z[97] = z[25] + z[94];
	z[103] = L1G + L1P;
	z[104] = -L2G - L2P;
	z[368] = L2D + L2X;
	z[369] = L1D - L1X;
	z[370] = L1BASE - L1B - L1C - L1C2;
	z[432] = MM5*z[95];
	z[589] = IM133*z[1];
	z[595] = IM211*z[2];
	z[609] = IM311*z[3];
	z[622] = IM411*z[4];
	z[640] = IM522*z[5];
	z[648] = IA22 + MA*(pow(L1A,2)+pow(L3A,2)) + z[1]*z[589];
	z[649] = pow(z[6],2);
	z[650] = pow(z[14],2);
	z[651] = pow(L3D,2);
	z[652] = pow(L3E,2);
	z[655] = L3D*MD;
	z[656] = L3E*ME;
	z[663] = MC + z[2]*z[595];
	z[665] = L2B*MC;
	z[668] = IE33 + IM433;
	z[669] = L2E*z[19];
	z[670] = pow(L1E,2);
	z[671] = pow(L2E,2);
	z[672] = L1E*z[19];
	z[673] = L1E*z[20];
	z[674] = L2E*z[20];
	z[675] = z[19]*z[92];
	z[676] = pow(z[91],2);
	z[677] = pow(z[92],2);
	z[678] = z[19]*z[91];
	z[679] = z[20]*z[91];
	z[680] = z[20]*z[92];
	z[681] = z[19]*z[26];
	z[682] = z[25]*z[42];
	z[683] = pow(z[26],2);
	z[684] = z[19]*z[42];
	z[685] = z[20]*z[26];
	z[686] = z[20]*z[42];
	z[687] = z[25]*z[97];
	z[688] = z[19]*z[97];
	z[689] = z[20]*z[97];
	z[690] = z[25]*z[68];
	z[691] = z[19]*z[68];
	z[692] = z[20]*z[68];
	z[694] = IE33 + IM433 + ME*(pow(L1E,2)+pow(L2E,2)) + z[3]*z[609] + MM4*(pow(z[91],2)+pow(z[92],2));
	z[695] = pow(z[26],2) + pow(z[97],2);
	z[696] = pow(z[42],2);
	z[697] = pow(z[68],2);
	z[699] = MM5*z[95]*z[97];
	z[700] = MF*z[42];
	z[701] = z[51]*z[68];
	z[703] = L1G*z[68];
	z[704] = L2G*z[68];
	z[707] = IF11 + 0.4131759111665347*IM522 + 0.5868240888334652*IM511 + MF*(pow(L2F,2)+pow(L3F,2)) + z[4]*z[622] + MM5*pow(z[95],2);
	z[708] = pow(z[51],2);
	z[711] = z[19]*z[51];
	z[712] = z[20]*z[51];
	z[713] = z[25]*z[51];
	z[714] = L2F*z[19];
	z[715] = L2F*z[20];
	z[716] = L2F*z[25];
	z[717] = L3F*z[19];
	z[718] = L3F*z[20];
	z[719] = L3F*z[25];
	z[724] = IG33 + MG*(pow(L1G,2)+pow(L2G,2)) + z[5]*z[640];
	z[726] = L1G*z[20];
	z[727] = L2G*z[20];
	z[728] = L2G*z[26];
	z[729] = L1G*z[19];
	z[730] = L1G*z[25];
	z[731] = L1G*z[26];
	z[732] = L2G*z[19];
	z[733] = L2G*z[25];
	z[738] = IB33 + IC33 + IM233 + MB*(pow(L1B,2)+pow(L2B,2)) + MM2*(pow(z[85],2)+pow(z[86],2));
	z[739] = pow(L2B,2);
	z[743] = ID33 + IE33 + IM333 + IM433 + MD*(pow(L1X,2)+pow(L2X,2)) + MM3*(pow(z[88],2)+pow(z[89],2));
	z[744] = pow(z[19],2);
	z[745] = pow(z[20],2);
	z[746] = pow(z[25],2);
	z[747] = z[19]*z[25];
	z[748] = z[20]*z[25];
	z[772] = IM133*pow(z[1],2);
	z[773] = MA*(pow(L1A,2)+pow(L3A,2));
	z[905] = z[1]*z[2];
	z[906] = z[3]*z[4];
	z[907] = z[2]*z[3];
	z[908] = z[4]*z[5];
	z[909] = z[1]*z[3];
	z[910] = z[3]*z[5];
	}

void CalculateZEquations ()
	{
	z[8] = sin(Q2);
	z[9] = cos(Q2);
	z[10] = L2B*z[8] - L1B*z[9];
	z[12] = z[11] + Q3 - L1BASE;
	z[13] = L2B*z[8] - z[9]*z[12];
	z[16] = sin(Q4);
	z[17] = cos(Q4);
	z[18] = L2X*z[16] - L1X*z[17];
	z[21] = cos(Q4+Q5);
	z[22] = sin(Q4+Q5);
	z[23] = z[20]*z[16] - z[19]*z[17];
	z[24] = L2E*z[22] - L1E*z[21];
	z[27] = cos(Q5);
	z[28] = sin(Q5);
	z[29] = cos(Q6);
	z[30] = z[28]*z[29];
	z[31] = sin(Q6);
	z[32] = z[28]*z[31];
	z[33] = z[27]*z[29];
	z[34] = z[27]*z[31];
	z[35] = z[17]*z[27] - z[16]*z[28];
	z[36] = -z[16]*z[33] - z[17]*z[30];
	z[37] = z[16]*z[34] + z[17]*z[32];
	z[38] = z[16]*z[27] + z[17]*z[28];
	z[39] = z[17]*z[33] - z[16]*z[30];
	z[40] = z[16]*z[32] - z[17]*z[34];
	z[41] = -z[14] - L1F*z[35];
	z[43] = z[26]*z[22] - z[25]*z[21];
	z[44] = -L2F*z[29] - L3F*z[31];
	z[45] = -L2F*z[40] - L3F*z[39];
	z[46] = L1F*z[29];
	z[47] = L3F*z[38];
	z[48] = L2F*z[38];
	z[49] = L1F*z[31];
	z[52] = cos(Q7);
	z[53] = sin(Q7);
	z[54] = z[29]*z[53];
	z[55] = z[29]*z[52];
	z[56] = z[31]*z[53];
	z[57] = z[31]*z[52];
	z[58] = z[27]*z[52] - z[28]*z[54];
	z[59] = -z[27]*z[53] - z[28]*z[55];
	z[60] = z[27]*z[54] + z[28]*z[52];
	z[61] = z[27]*z[55] - z[28]*z[53];
	z[62] = z[17]*z[58] - z[16]*z[60];
	z[63] = z[17]*z[59] - z[16]*z[61];
	z[64] = z[16]*z[58] + z[17]*z[60];
	z[65] = z[16]*z[59] + z[17]*z[61];
	z[66] = -z[14] - z[50]*z[35];
	z[67] = L3G*z[31] - z[26];
	z[69] = z[51]*z[29];
	z[70] = z[51]*z[40];
	z[71] = z[50]*z[29];
	z[72] = z[51]*z[38];
	z[73] = z[50]*z[31];
	z[74] = L2G*z[29] + L3G*z[57];
	z[75] = L2G*z[29];
	z[76] = L2G*z[40] + L3G*z[65];
	z[77] = L3G*z[53];
	z[78] = L1G*z[29] - L3G*z[56];
	z[79] = L1G*z[29];
	z[80] = L1G*z[40] - L3G*z[64];
	z[81] = L3G*z[52];
	z[82] = L1G*z[53] - L2G*z[52];
	z[83] = -L1G*z[57] - L2G*z[56];
	z[84] = -L1G*z[65] - L2G*z[64];
	z[87] = z[86]*z[8] - z[85]*z[9];
	z[90] = z[89]*z[16] - z[88]*z[17];
	z[93] = z[92]*z[22] - z[91]*z[21];
	z[96] = -z[14] - z[94]*z[35];
	z[98] = z[95]*z[29];
	z[99] = z[95]*z[40];
	z[100] = z[94]*z[29];
	z[101] = z[95]*z[38];
	z[102] = z[94]*z[31];
	z[105] = z[104]*z[29];
	z[106] = z[104]*z[40];
	z[107] = z[103]*z[29];
	z[108] = z[103]*z[40];
	z[109] = z[103]*z[53] + z[104]*z[52];
	z[110] = z[104]*z[56] - z[103]*z[57];
	z[111] = z[104]*z[64] - z[103]*z[65];
	z[112] = z[1]*U1;
	z[113] = U1*U2;
	z[114] = z[9]*z[113];
	z[115] = z[8]*z[113];
	z[116] = z[2]*U3;
	z[117] = U2*z[116];
	z[118] = z[9]*U1*z[116];
	z[119] = -z[117] - z[8]*z[113];
	z[120] = U1*U4;
	z[121] = z[17]*z[120];
	z[122] = z[16]*z[120];
	z[123] = U1*(U4+U5);
	z[124] = z[21]*z[123];
	z[125] = z[22]*z[123];
	z[126] = z[3]*U5;
	z[127] = 0.6264677816866908*z[17] + 0.7794473160570615*z[16];
	z[128] = 0.7794473160570615*z[17] - 0.6264677816866908*z[16];
	z[129] = 0.6264677816866908*z[16] - 0.7794473160570615*z[17];
	z[130] = U4*z[126];
	z[131] = z[127]*U1*z[126];
	z[132] = z[127]*z[120];
	z[133] = z[128]*z[120] - z[130];
	z[134] = U6*(U4+U5);
	z[135] = z[21]*U1*U6;
	z[136] = z[35]*z[123];
	z[137] = z[29]*z[134] + z[36]*z[123] - z[31]*z[135];
	z[138] = z[37]*z[123] - z[29]*z[135] - z[31]*z[134];
	z[139] = z[4]*U6;
	z[140] = (U4+U5)*z[139];
	z[141] = z[21]*U1*z[139];
	z[142] = -z[140] - z[22]*z[123];
	z[143] = U7*(z[31]*U4+z[31]*U5+z[39]*U1);
	z[144] = U7*(U6+z[38]*U1);
	z[145] = z[52]*z[143] + z[54]*z[134] + z[62]*z[123] - z[53]*z[144] - z[56]*z[135];
	z[146] = z[55]*z[134] + z[63]*z[123] - z[52]*z[144] - z[53]*z[143] - z[57]*z[135];
	z[147] = z[5]*U7;
	z[148] = 0.766044443118978*z[27] - 0.6427876096865393*z[28]*z[29];
	z[149] = -0.6427876096865393*z[27] - 0.766044443118978*z[28]*z[29];
	z[150] = 0.766044443118978*z[28] + 0.6427876096865393*z[27]*z[29];
	z[151] = 0.766044443118978*z[27]*z[29] - 0.6427876096865393*z[28];
	z[152] = z[17]*z[148] - z[16]*z[150];
	z[153] = z[17]*z[149] - z[16]*z[151];
	z[154] = z[16]*z[148] + z[17]*z[150];
	z[155] = z[16]*z[149] + z[17]*z[151];
	z[156] = (z[29]*U4+z[29]*U5+z[40]*U1)*z[147];
	z[157] = (1.19175359259421*U6+z[31]*U4+z[31]*U5+1.555723826860413*z[154]*U1)*z[147];
	z[158] = z[156] + z[152]*z[123] + 0.6427876096865393*z[29]*z[134] - 0.6427876096865393*z[31]*z[135];
	z[159] = z[153]*z[123] + 0.766044443118978*z[29]*z[134] - 0.766044443118978*z[31]*z[135];
	z[160] = z[37]*z[123] - 0.6427876096865393*z[157] - z[29]*z[135] - z[31]*z[134];
	z[161] = L3A*U1;
	z[162] = L1A*U1;
	z[163] = U1*z[162];
	z[164] = U1*z[161];
	z[165] = z[6]*U1;
	z[166] = L2B*U2;
	z[167] = L1B*U2;
	z[168] = z[10]*U1;
	z[169] = (L1B*z[8]+L2B*z[9])*U2;
	z[170] = U1*z[169];
	z[171] = U1*(z[165]-z[168]);
	z[172] = U2*z[167];
	z[173] = U2*z[166];
	z[174] = z[170] + z[8]*U1*z[167] + z[9]*U1*z[166];
	z[175] = U3 - L2B*U2;
	z[176] = z[12]*U2;
	z[177] = z[13]*U1;
	z[178] = U2*U3;
	z[179] = L2B*z[9]*U2 + z[8]*z[12]*U2 - z[9]*U3;
	z[180] = U1*z[179];
	z[181] = U1*(z[165]-z[177]);
	z[182] = U2*z[176];
	z[183] = z[178] + U2*z[175];
	z[184] = z[180] + z[8]*U1*z[176] - z[9]*U1*z[175];
	z[185] = L3D*U1;
	z[186] = z[14]*U1;
	z[187] = L2X*U4;
	z[188] = L1X*U4;
	z[189] = z[18]*U1;
	z[190] = (L1X*z[16]+L2X*z[17])*U4;
	z[191] = U1*z[190];
	z[192] = U1*(z[186]-z[189]);
	z[193] = U1*z[185];
	z[194] = U4*z[188];
	z[195] = U4*z[187];
	z[196] = z[191] + z[16]*U1*z[188] + z[17]*U1*z[187];
	z[197] = L3E*U1;
	z[198] = z[20]*U4;
	z[199] = z[19]*U4;
	z[200] = z[23]*U1;
	z[201] = L2E*(U4+U5);
	z[202] = L1E*(U4+U5);
	z[203] = z[24]*U1;
	z[204] = (z[19]*z[16]+z[20]*z[17])*U4;
	z[205] = U1*z[204];
	z[206] = z[21]*(U4+U5);
	z[207] = z[22]*(U4+U5);
	z[208] = L1E*z[207] + L2E*z[206];
	z[209] = U1*z[208];
	z[210] = U1*(z[186]-z[200]-z[203]);
	z[211] = U1*z[197];
	z[212] = U4*z[199];
	z[213] = U4*z[198];
	z[214] = z[205] + z[16]*U1*z[199] + z[17]*U1*z[198];
	z[215] = (U4+U5)*z[202];
	z[216] = (U4+U5)*z[201];
	z[217] = z[209] + z[21]*U1*z[201] + z[22]*U1*z[202];
	z[218] = z[41]*U1;
	z[219] = z[26]*(U4+U5);
	z[220] = z[25]*U4 + z[42]*U5;
	z[221] = z[43]*U1;
	z[222] = z[44]*U4 + z[44]*U5 + z[45]*U1;
	z[223] = L3F*U6 + z[46]*U4 + z[47]*U1;
	z[224] = L2F*U6 + z[48]*U1 - z[49]*U4;
	z[225] = -z[16]*z[27]*U4 - z[16]*z[27]*U5 - z[17]*z[28]*U4 - z[17]*z[28]*U5;
	z[226] = L1F*U1*z[225];
	z[227] = z[25]*z[207] + z[26]*z[206];
	z[228] = U1*z[227];
	z[229] = (L2F*z[31]-L3F*z[29])*U6;
	z[230] = z[27]*z[31]*U5 + z[28]*z[29]*U6;
	z[231] = z[27]*z[29]*U6 - z[28]*z[31]*U5;
	z[232] = z[16]*z[34]*U4 + z[17]*z[32]*U4 + z[16]*z[230] - z[17]*z[231];
	z[233] = -z[27]*z[31]*U6 - z[28]*z[29]*U5;
	z[234] = z[27]*z[29]*U5 - z[28]*z[31]*U6;
	z[235] = z[17]*z[233] - z[16]*z[33]*U4 - z[17]*z[30]*U4 - z[16]*z[234];
	z[236] = -L2F*z[232] - L3F*z[235];
	z[237] = U1*z[236] + U4*z[229] + U5*z[229];
	z[238] = L1F*z[31]*U6;
	z[239] = z[17]*z[27]*U4 + z[17]*z[27]*U5 - z[16]*z[28]*U4 - z[16]*z[28]*U5;
	z[240] = L3F*U1*z[239] - U4*z[238];
	z[241] = L1F*z[29]*U6;
	z[242] = L2F*U1*z[239] - U4*z[241];
	z[243] = U1*(z[200]+z[218]+z[221]);
	z[244] = -z[226] - z[35]*U1*z[222];
	z[245] = (U4+U5)*z[220];
	z[246] = U5*z[222] - (U4+U5)*z[219];
	z[247] = z[228] + z[21]*U1*z[219] + z[22]*U1*z[220];
	z[248] = z[237] + (z[31]*U4+z[31]*U5+z[39]*U1)*z[224] - (z[29]*U4+z[29]*U5+z[40]*U1)*z[223];
	z[249] = z[240] + z[29]*U4*z[222] - (U6+z[38]*U1)*z[224];
	z[250] = z[242] + (U6+z[38]*U1)*z[223] - z[31]*U4*z[222];
	z[251] = z[66]*U1;
	z[252] = z[67]*U5 - z[26]*U4;
	z[253] = z[25]*U4 + z[68]*U5;
	z[254] = -z[69]*U4 - z[69]*U5 - z[70]*U1;
	z[255] = z[71]*U4;
	z[256] = z[51]*U6 + z[72]*U1 - z[73]*U4;
	z[257] = L2G*U7 + z[74]*U4 + z[75]*U5 + z[76]*U1 - z[77]*U6;
	z[258] = L1G*U7 + z[78]*U4 + z[79]*U5 + z[80]*U1 - z[81]*U6;
	z[259] = z[82]*U6 + z[83]*U4 + z[83]*U5 + z[84]*U1;
	z[260] = z[50]*U1*z[225];
	z[261] = L3G*z[29]*U6;
	z[262] = U5*z[261];
	z[263] = z[51]*z[31]*U6;
	z[264] = U4*z[263] + U5*z[263] - z[51]*U1*z[232];
	z[265] = z[50]*z[31]*U6;
	z[266] = U4*z[265];
	z[267] = z[50]*z[29]*U6;
	z[268] = z[51]*U1*z[239] - U4*z[267];
	z[269] = z[29]*z[52]*U6 - z[31]*z[53]*U7;
	z[270] = L3G*z[269] - L2G*z[31]*U6;
	z[271] = L2G*z[31]*U6;
	z[272] = -z[29]*z[53]*U7 - z[31]*z[52]*U6;
	z[273] = z[28]*z[53]*U5 - z[27]*z[52]*U7 - z[27]*z[55]*U5 - z[28]*z[272];
	z[274] = z[27]*z[272] - z[27]*z[53]*U5 - z[28]*z[52]*U7 - z[28]*z[55]*U5;
	z[275] = z[17]*z[59]*U4 + z[16]*z[273] + z[17]*z[274] - z[16]*z[61]*U4;
	z[276] = L2G*z[232] + L3G*z[275];
	z[277] = L3G*z[52]*U7;
	z[278] = U1*z[276] + U4*z[270] - U5*z[271] - U6*z[277];
	z[279] = z[29]*z[53]*U6 + z[31]*z[52]*U7;
	z[280] = -L1G*z[31]*U6 - L3G*z[279];
	z[281] = L1G*z[31]*U6;
	z[282] = z[29]*z[52]*U7 - z[31]*z[53]*U6;
	z[283] = -z[27]*z[53]*U7 - z[27]*z[54]*U5 - z[28]*z[52]*U5 - z[28]*z[282];
	z[284] = z[27]*z[52]*U5 + z[27]*z[282] - z[28]*z[53]*U7 - z[28]*z[54]*U5;
	z[285] = z[17]*z[58]*U4 + z[16]*z[283] + z[17]*z[284] - z[16]*z[60]*U4;
	z[286] = L1G*z[232] - L3G*z[285];
	z[287] = L3G*z[53]*U7;
	z[288] = U1*z[286] + U4*z[280] + U6*z[287] - U5*z[281];
	z[289] = (L1G*z[52]+L2G*z[53])*U7;
	z[290] = -L1G*z[269] - L2G*z[279];
	z[291] = -L1G*z[275] - L2G*z[285];
	z[292] = U1*z[291] + U4*z[290] + U5*z[290] + U6*z[289];
	z[293] = U1*(z[200]+z[221]+z[251]);
	z[294] = -z[260] - z[35]*U1*z[254];
	z[295] = z[262] + z[31]*U5*z[259] - (U4+U5)*z[253];
	z[296] = U5*z[254] + (U4+U5)*z[252];
	z[297] = z[228] + z[22]*U1*z[253] - z[21]*U1*z[252];
	z[298] = z[264] + (z[31]*U4+z[31]*U5+z[39]*U1)*z[256] - (z[29]*U4+z[29]*U5+z[40]*U1)*z[255];
	z[299] = z[29]*U4*z[254] - z[266] - (U6+z[38]*U1)*z[256];
	z[300] = z[268] + (U6+z[38]*U1)*z[255] - z[31]*U4*z[254];
	z[301] = z[278] - (U7+z[29]*U4+z[29]*U5+z[40]*U1)*z[258] - (z[53]*U6-z[57]*U4-z[65]*U1)*z[259];
	z[302] = z[288] + (U7+z[29]*U4+z[29]*U5+z[40]*U1)*z[257] - (z[52]*U6+z[56]*U4+z[64]*U1)*z[259];
	z[303] = z[292] + (z[52]*U6+z[56]*U4+z[56]*U5+z[64]*U1)*z[258] + (z[53]*U6-z[57]*U4-z[57]*U5-z[65]*U1)*z[257];
	z[304] = z[86]*U2;
	z[305] = z[85]*U2;
	z[306] = z[87]*U1;
	z[307] = (z[85]*z[8]+z[86]*z[9])*U2;
	z[308] = U1*z[307];
	z[309] = U1*(z[165]-z[306]);
	z[310] = U2*z[305];
	z[311] = U2*z[304];
	z[312] = z[308] + z[8]*U1*z[305] + z[9]*U1*z[304];
	z[313] = z[89]*U4;
	z[314] = z[88]*U4;
	z[315] = z[90]*U1;
	z[316] = (z[88]*z[16]+z[89]*z[17])*U4;
	z[317] = U1*z[316];
	z[318] = U1*(z[186]-z[315]);
	z[319] = U4*z[314];
	z[320] = U4*z[313];
	z[321] = z[317] + z[16]*U1*z[314] + z[17]*U1*z[313];
	z[322] = z[92]*(U4+U5);
	z[323] = z[91]*(U4+U5);
	z[324] = z[93]*U1;
	z[325] = z[91]*z[207] + z[92]*z[206];
	z[326] = U1*z[325];
	z[327] = U1*(z[186]-z[200]-z[324]);
	z[328] = (U4+U5)*z[323];
	z[329] = (U4+U5)*z[322];
	z[330] = z[326] + z[21]*U1*z[322] + z[22]*U1*z[323];
	z[331] = z[96]*U1;
	z[332] = z[25]*U4 + z[97]*U5;
	z[333] = -z[98]*U4 - z[98]*U5 - z[99]*U1;
	z[334] = z[100]*U4;
	z[335] = z[95]*U6 + z[101]*U1 - z[102]*U4;
	z[336] = z[94]*U1*z[225];
	z[337] = z[95]*z[31]*U6;
	z[338] = U4*z[337] + U5*z[337] - z[95]*U1*z[232];
	z[339] = z[94]*z[31]*U6;
	z[340] = U4*z[339];
	z[341] = z[94]*z[29]*U6;
	z[342] = z[95]*U1*z[239] - U4*z[341];
	z[343] = U1*(z[200]+z[221]+z[331]);
	z[344] = -z[336] - z[35]*U1*z[333];
	z[345] = (U4+U5)*z[332];
	z[346] = U5*z[333] - (U4+U5)*z[219];
	z[347] = z[228] + z[21]*U1*z[219] + z[22]*U1*z[332];
	z[348] = z[338] + (z[31]*U4+z[31]*U5+z[39]*U1)*z[335] - (z[29]*U4+z[29]*U5+z[40]*U1)*z[334];
	z[349] = z[29]*U4*z[333] - z[340] - (U6+z[38]*U1)*z[335];
	z[350] = z[342] + (U6+z[38]*U1)*z[334] - z[31]*U4*z[333];
	z[354] = z[104]*z[31]*U6;
	z[356] = z[103]*z[31]*U6;
	z[358] = (z[103]*z[52]-z[104]*z[53])*U7;
	z[359] = z[104]*z[279] - z[103]*z[269];
	z[360] = z[104]*z[285] - z[103]*z[275];
	z[373] = L2B*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[374] = L1B*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[375] = cos(Q1);
	z[377] = sin(Q1);
	z[381] = 1 - L2B*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[382] = z[12]*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[383] = L2X*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[384] = L1X*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[385] = z[17]*z[375];
	z[386] = z[17]*z[377];
	z[387] = z[16]*z[375];
	z[388] = z[16]*z[377];
	z[389] = z[20]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[390] = z[19]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[391] = L2E*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[392] = L1E*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[393] = z[27]*z[385] - z[28]*z[387];
	z[394] = z[28]*z[388] - z[27]*z[386];
	z[395] = -z[27]*z[387] - z[28]*z[385];
	z[396] = z[27]*z[388] + z[28]*z[386];
	z[397] = z[26]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[398] = z[25]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[399] = z[44]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[400] = z[46]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[401] = z[49]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[402] = z[29]*z[395] + z[31]*z[377];
	z[403] = z[29]*z[35];
	z[404] = z[29]*z[396] + z[31]*z[375];
	z[405] = z[29]*z[377] - z[31]*z[395];
	z[406] = z[31]*z[35];
	z[407] = z[29]*z[375] - z[31]*z[396];
	z[408] = z[69]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[409] = z[71]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[410] = z[73]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[411] = z[74]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[412] = z[78]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[413] = z[83]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[414] = z[52]*z[393] + z[53]*z[402];
	z[415] = z[38]*z[52] + z[53]*z[403];
	z[416] = z[52]*z[394] + z[53]*z[404];
	z[417] = z[52]*z[402] - z[53]*z[393];
	z[418] = z[52]*z[403] - z[38]*z[53];
	z[419] = z[52]*z[404] - z[53]*z[394];
	z[420] = z[86]*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[421] = z[85]*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[422] = z[89]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[423] = z[88]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[424] = z[92]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[425] = z[91]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[426] = z[98]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[427] = z[100]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[428] = z[102]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[429] = MM5*(z[38]*z[99]+z[101]*z[406]) - MF*(z[38]*z[45]+z[47]*z[403]-z[48]*z[406]) - MG*(z[76]*z[415]+z[80]*z[418]-z[38]*z[70]-z[72]*z[406]-z[84]*z[406]);
	z[430] = MB*(z[8]*z[373]-z[9]*z[374]) + MD*(z[16]*z[383]-z[17]*z[384]) + MM2*(z[8]*z[420]-z[9]*z[421]) + MM3*(z[16]*z[422]-z[17]*z[423]) + MM5*(z[16]*z[389]+z[38]*z[397]+z[38]*z[426]-z[17]*z[390]-z[35]*z[398]-z[403]*z[427]-z[406]*z[428]) + MF*(z[16]*z[389]+z[38]*z[397]-z[17]*z[390]-z[35]*z[398]-z[38]*z[399]-z[400]*z[403]-z[401]*z[406]) + MG*(z[16]*z[389]+z[38]*z[397]+z[38]*z[408]+z[406]*z[413]-z[17]*z[390]-z[35]*z[398]-z[403]*z[409]-z[406]*z[410]-z[411]*z[415]-z[412]*z[418]) - MC*(z[8]*z[381]+z[9]*z[382]) - ME*(z[17]*z[390]+z[35]*z[392]-z[16]*z[389]-z[38]*z[391]) - MM4*(z[17]*z[390]+z[35]*z[425]-z[16]*z[389]-z[38]*z[424]);
	z[431] = MF*(z[26]*z[38]-z[42]*z[35]-z[38]*z[44]) - ME*(L1E*z[35]-L2E*z[38]) - MM4*(z[91]*z[35]-z[92]*z[38]) - MM5*(z[97]*z[35]-z[26]*z[38]-z[38]*z[98]) - MG*(z[68]*z[35]+z[38]*z[67]+z[75]*z[415]+z[79]*z[418]-z[38]*z[69]-z[83]*z[406]);
	z[433] = z[432]*z[406] + MF*(L2F*z[406]-L3F*z[403]) + MG*(z[51]*z[406]+z[77]*z[415]+z[81]*z[418]+z[82]*z[406]);
	z[434] = MG*(L1G*z[418]+L2G*z[415]);
	z[435] = z[8]*U1;
	z[436] = z[9]*U1;
	z[437] = IB13*U2 + IB11*z[435] + IB12*z[436];
	z[438] = IB23*U2 + IB12*z[435] + IB22*z[436];
	z[439] = IB33*U2 + IB13*z[435] + IB23*z[436];
	z[440] = IB11*z[8];
	z[441] = IB11*z[114];
	z[442] = IB12*z[8];
	z[443] = IB12*z[114];
	z[444] = IB13*z[8];
	z[445] = IB13*z[114];
	z[446] = IB12*z[9];
	z[447] = IB12*z[115];
	z[448] = IB22*z[9];
	z[449] = IB22*z[115];
	z[450] = IB23*z[9];
	z[451] = IB23*z[115];
	z[452] = z[435]*z[438] - z[436]*z[437];
	z[453] = U2*z[437] - z[435]*z[439];
	z[454] = z[436]*z[439] - U2*z[438];
	z[455] = IC13*U2 + IC11*z[435] + IC12*z[436];
	z[456] = IC23*U2 + IC12*z[435] + IC22*z[436];
	z[457] = IC33*U2 + IC13*z[435] + IC23*z[436];
	z[458] = IC11*z[8];
	z[459] = IC11*z[114];
	z[460] = IC12*z[8];
	z[461] = IC12*z[114];
	z[462] = IC13*z[8];
	z[463] = IC13*z[114];
	z[464] = IC12*z[9];
	z[465] = IC12*z[115];
	z[466] = IC22*z[9];
	z[467] = IC22*z[115];
	z[468] = IC23*z[9];
	z[469] = IC23*z[115];
	z[470] = z[435]*z[456] - z[436]*z[455];
	z[471] = U2*z[455] - z[435]*z[457];
	z[472] = z[436]*z[457] - U2*z[456];
	z[473] = z[16]*U1;
	z[474] = z[17]*U1;
	z[475] = ID13*U4 + ID11*z[473] + ID12*z[474];
	z[476] = ID23*U4 + ID12*z[473] + ID22*z[474];
	z[477] = ID33*U4 + ID13*z[473] + ID23*z[474];
	z[478] = ID11*z[16];
	z[479] = ID11*z[121];
	z[480] = ID12*z[16];
	z[481] = ID12*z[121];
	z[482] = ID13*z[16];
	z[483] = ID13*z[121];
	z[484] = ID12*z[17];
	z[485] = ID12*z[122];
	z[486] = ID22*z[17];
	z[487] = ID22*z[122];
	z[488] = ID23*z[17];
	z[489] = ID23*z[122];
	z[490] = z[473]*z[476] - z[474]*z[475];
	z[491] = U4*z[475] - z[473]*z[477];
	z[492] = z[474]*z[477] - U4*z[476];
	z[493] = z[22]*U1;
	z[494] = z[21]*U1;
	z[495] = U4 + U5;
	z[496] = IE11*z[493] + IE12*z[494] + IE13*z[495];
	z[497] = IE12*z[493] + IE22*z[494] + IE23*z[495];
	z[498] = IE13*z[493] + IE23*z[494] + IE33*z[495];
	z[499] = IE11*z[22];
	z[500] = IE11*z[124];
	z[501] = IE12*z[22];
	z[502] = IE12*z[124];
	z[503] = IE13*z[22];
	z[504] = IE13*z[124];
	z[505] = IE12*z[21];
	z[506] = IE12*z[125];
	z[507] = IE22*z[21];
	z[508] = IE22*z[125];
	z[509] = IE23*z[21];
	z[510] = IE23*z[125];
	z[511] = z[493]*z[497] - z[494]*z[496];
	z[512] = z[495]*z[496] - z[493]*z[498];
	z[513] = z[494]*z[498] - z[495]*z[497];
	z[514] = U6 + z[38]*U1;
	z[515] = z[31]*U4 + z[31]*U5 + z[39]*U1;
	z[516] = z[29]*U4 + z[29]*U5 + z[40]*U1;
	z[517] = IF11*z[514] + IF12*z[515] + IF13*z[516];
	z[518] = IF12*z[514] + IF22*z[515] + IF23*z[516];
	z[519] = IF13*z[514] + IF23*z[515] + IF33*z[516];
	z[520] = IF11*z[38];
	z[521] = IF11*z[136];
	z[522] = IF12*z[38];
	z[523] = IF12*z[136];
	z[524] = IF13*z[38];
	z[525] = IF13*z[136];
	z[526] = IF12*z[31];
	z[527] = IF12*z[39];
	z[528] = IF12*z[137];
	z[529] = IF22*z[31];
	z[530] = IF22*z[39];
	z[531] = IF22*z[137];
	z[532] = IF23*z[31];
	z[533] = IF23*z[39];
	z[534] = IF23*z[137];
	z[535] = IF13*z[29];
	z[536] = IF13*z[40];
	z[537] = IF13*z[138];
	z[538] = IF23*z[29];
	z[539] = IF23*z[40];
	z[540] = IF23*z[138];
	z[541] = IF33*z[29];
	z[542] = IF33*z[40];
	z[543] = IF33*z[138];
	z[544] = z[514]*z[518] - z[515]*z[517];
	z[545] = z[516]*z[517] - z[514]*z[519];
	z[546] = z[515]*z[519] - z[516]*z[518];
	z[547] = z[52]*U6 + z[56]*U4 + z[56]*U5 + z[64]*U1;
	z[548] = z[57]*U4 + z[57]*U5 + z[65]*U1 - z[53]*U6;
	z[549] = U7 + z[29]*U4 + z[29]*U5 + z[40]*U1;
	z[550] = IG11*z[547] + IG12*z[548] + IG13*z[549];
	z[551] = IG12*z[547] + IG22*z[548] + IG23*z[549];
	z[552] = IG13*z[547] + IG23*z[548] + IG33*z[549];
	z[553] = IG11*z[52];
	z[554] = IG11*z[56];
	z[555] = IG11*z[64];
	z[556] = IG11*z[145];
	z[557] = IG12*z[52];
	z[558] = IG12*z[56];
	z[559] = IG12*z[64];
	z[560] = IG12*z[145];
	z[561] = IG13*z[52];
	z[562] = IG13*z[56];
	z[563] = IG13*z[64];
	z[564] = IG13*z[145];
	z[565] = IG12*z[57];
	z[566] = IG12*z[65];
	z[567] = IG12*z[53];
	z[568] = IG12*z[146];
	z[569] = IG22*z[57];
	z[570] = IG22*z[65];
	z[571] = IG22*z[53];
	z[572] = IG22*z[146];
	z[573] = IG23*z[57];
	z[574] = IG23*z[65];
	z[575] = IG23*z[53];
	z[576] = IG23*z[146];
	z[577] = IG13*z[29];
	z[578] = IG13*z[40];
	z[579] = IG13*z[138];
	z[580] = IG23*z[29];
	z[581] = IG23*z[40];
	z[582] = IG23*z[138];
	z[583] = IG33*z[29];
	z[584] = IG33*z[40];
	z[585] = IG33*z[138];
	z[586] = z[547]*z[551] - z[548]*z[550];
	z[587] = z[549]*z[550] - z[547]*z[552];
	z[588] = z[548]*z[552] - z[549]*z[551];
	z[590] = z[8]*U1 - z[2]*U3;
	z[591] = IM211*z[590];
	z[592] = IM222*z[436];
	z[593] = IM233*U2;
	z[594] = IM211*z[8];
	z[596] = IM211*z[114];
	z[597] = IM222*z[9];
	z[598] = IM222*z[119];
	z[599] = IM233*z[118];
	z[600] = z[590]*z[592] - z[436]*z[591];
	z[601] = U2*z[591] - z[590]*z[593];
	z[602] = z[436]*z[593] - U2*z[592];
	z[603] = z[129]*U1 - z[3]*U5;
	z[604] = z[127]*U1;
	z[605] = IM311*z[603];
	z[606] = IM322*z[604];
	z[607] = IM333*U4;
	z[608] = IM311*z[129];
	z[610] = IM311*z[132];
	z[611] = IM322*z[127];
	z[612] = IM322*z[133];
	z[613] = IM333*z[131];
	z[614] = z[603]*z[606] - z[604]*z[605];
	z[615] = U4*z[605] - z[603]*z[607];
	z[616] = z[604]*z[607] - U4*z[606];
	z[617] = z[22]*U1 - z[4]*U6;
	z[618] = IM411*z[617];
	z[619] = IM422*z[494];
	z[620] = IM433*z[495];
	z[621] = IM411*z[22];
	z[623] = IM411*z[124];
	z[624] = IM422*z[21];
	z[625] = IM422*z[142];
	z[626] = IM433*z[141];
	z[627] = z[617]*z[619] - z[494]*z[618];
	z[628] = z[495]*z[618] - z[617]*z[620];
	z[629] = z[494]*z[620] - z[495]*z[619];
	z[630] = 0.766044443118978*U6 + z[154]*U1 + 0.6427876096865393*z[31]*U4 + 0.6427876096865393*z[31]*U5;
	z[631] = z[155]*U1 + 0.766044443118978*z[31]*U4 + 0.766044443118978*z[31]*U5 - 0.6427876096865393*U6 - z[5]*U7;
	z[632] = IM511*z[630];
	z[633] = IM522*z[631];
	z[634] = IM533*z[516];
	z[635] = IM511*z[154];
	z[636] = IM511*z[31];
	z[637] = IM511*z[158];
	z[638] = IM522*z[155];
	z[639] = IM522*z[31];
	z[641] = IM522*z[159];
	z[642] = IM533*z[29];
	z[643] = IM533*z[40];
	z[644] = IM533*z[160];
	z[645] = z[630]*z[633] - z[631]*z[632];
	z[646] = z[516]*z[632] - z[630]*z[634];
	z[647] = z[631]*z[634] - z[516]*z[633];
	z[653] = z[648] + z[8]*z[440] + z[8]*z[446] + z[8]*z[458] + z[8]*z[464] + z[8]*z[594] + z[9]*z[442] + z[9]*z[448] + z[9]*z[460] + z[9]*z[466] + z[9]*z[597] + z[16]*z[478] + z[16]*z[484] + z[17]*z[480] + z[17]*z[486] + z[21]*z[501] + z[21]*z[507] + z[21]*z[624] + z[22]*z[499] + z[22]*z[505] + z[22]*z[621] + z[38]*z[520] + z[38]*z[527] + z[38]*z[536] + z[39]*z[522] + z[39]*z[530] + z[39]*z[539] + z[40]*z[524] + z[40]*z[533] + z[40]*z[542] + z[40]*z[563] + z[40]*z[574] + z[40]*z[584] + z[40]*z[643] + z[64]*z[555] + z[64]*z[566] + z[64]*z[578] + z[65]*z[559] + z[65]*z[570] + z[65]*z[581] + z[127]*z[611] + z[129]*z[608] + z[154]*z[635] + z[155]*z[638] + MM5*(pow(z[23],2)+pow(z[43],2)+pow(z[96],2)+pow(z[99],2)+pow(z[101],2)+2*z[23]*z[43]+2*z[23]*z[96]+2*z[43]*z[96]+2*z[23]*z[29]*z[101]+2*z[29]*z[43]*z[101]+2*z[29]*z[96]*z[101]) + MF*(pow(z[23],2)+pow(z[41],2)+pow(z[43],2)+pow(z[45],2)+pow(z[47],2)+pow(z[48],2)+2*z[23]*z[41]+2*z[23]*z[43]+2*z[41]*z[43]+2*z[23]*z[29]*z[48]+2*z[23]*z[31]*z[47]+2*z[29]*z[41]*z[48]+2*z[29]*z[43]*z[48]+2*z[31]*z[41]*z[47]+2*z[31]*z[43]*z[47]) - MB*(2*z[6]*z[10]-z[649]-pow(z[10],2)) - MC*(2*z[6]*z[13]-z[649]-pow(z[13],2)) - MM2*(2*z[6]*z[87]-z[649]-pow(z[87],2)) - MM3*(2*z[14]*z[90]-z[650]-pow(z[90],2)) - MD*(2*z[14]*z[18]-z[650]-z[651]-pow(z[18],2)) - MM4*(2*z[14]*z[23]+2*z[14]*z[93]-z[650]-2*z[23]*z[93]-pow(z[23],2)-pow(z[93],2)) - ME*(2*z[14]*z[23]+2*z[14]*z[24]-z[650]-z[652]-2*z[23]*z[24]-pow(z[23],2)-pow(z[24],2)) - MG*(2*z[52]*z[70]*z[76]-2*z[23]*z[43]-2*z[23]*z[66]-2*z[43]*z[66]-2*z[72]*z[84]-pow(z[23],2)-pow(z[43],2)-pow(z[66],2)-pow(z[70],2)-pow(z[72],2)-pow(z[76],2)-pow(z[80],2)-pow(z[84],2)-2*z[23]*z[29]*z[72]-2*z[23]*z[29]*z[84]-2*z[23]*z[56]*z[76]-2*z[23]*z[57]*z[80]-2*z[29]*z[43]*z[72]-2*z[29]*z[43]*z[84]-2*z[29]*z[66]*z[72]-2*z[29]*z[66]*z[84]-2*z[43]*z[56]*z[76]-2*z[43]*z[57]*z[80]-2*z[53]*z[70]*z[80]-2*z[56]*z[66]*z[76]-2*z[57]*z[66]*z[80]);
	z[654] = IB13*z[8] + IB23*z[9] + IC13*z[8] + IC23*z[9];
	z[657] = ID13*z[16] + ID23*z[17] + IE13*z[22] + IE23*z[21] + z[38]*z[526] + z[38]*z[535] + z[39]*z[529] + z[39]*z[538] + z[40]*z[532] + z[40]*z[541] + z[40]*z[562] + z[40]*z[573] + z[40]*z[583] + z[40]*z[642] + z[64]*z[554] + z[64]*z[565] + z[64]*z[577] + z[65]*z[558] + z[65]*z[569] + z[65]*z[580] + 0.6427876096865393*z[154]*z[636] + 0.766044443118978*z[155]*z[639] + z[655]*(L1X*z[16]+L2X*z[17]) + z[656]*(L1E*z[22]+L2E*z[21]+z[19]*z[16]+z[20]*z[17]) - MM5*(z[101]*z[102]+z[19]*z[28]*z[99]+z[19]*z[34]*z[101]+z[20]*z[32]*z[101]+z[25]*z[31]*z[101]+z[23]*z[29]*z[102]+z[29]*z[43]*z[102]+z[29]*z[96]*z[102]-z[26]*z[99]-z[98]*z[99]-z[20]*z[27]*z[99]-z[23]*z[31]*z[100]-z[31]*z[43]*z[100]-z[31]*z[96]*z[100]) - MF*(z[26]*z[45]+z[48]*z[49]+z[19]*z[34]*z[48]+z[20]*z[27]*z[45]+z[20]*z[32]*z[48]+z[25]*z[31]*z[48]+z[23]*z[29]*z[49]+z[29]*z[41]*z[49]+z[29]*z[43]*z[49]-z[44]*z[45]-z[46]*z[47]-z[19]*z[28]*z[45]-z[19]*z[33]*z[47]-z[20]*z[30]*z[47]-z[25]*z[29]*z[47]-z[23]*z[31]*z[46]-z[31]*z[41]*z[46]-z[31]*z[43]*z[46]) - MG*(z[72]*z[73]+z[73]*z[84]+z[19]*z[28]*z[70]+z[19]*z[34]*z[72]+z[19]*z[34]*z[84]+z[20]*z[32]*z[72]+z[20]*z[32]*z[84]+z[20]*z[58]*z[76]+z[20]*z[59]*z[80]+z[25]*z[31]*z[72]+z[25]*z[31]*z[84]+z[26]*z[52]*z[76]+z[23]*z[29]*z[73]+z[29]*z[43]*z[73]+z[29]*z[66]*z[73]+z[52]*z[69]*z[76]+z[52]*z[70]*z[74]-z[26]*z[70]-z[69]*z[70]-z[72]*z[83]-z[74]*z[76]-z[78]*z[80]-z[83]*z[84]-z[19]*z[60]*z[76]-z[19]*z[61]*z[80]-z[20]*z[27]*z[70]-z[25]*z[54]*z[76]-z[25]*z[55]*z[80]-z[26]*z[53]*z[80]-z[23]*z[29]*z[83]-z[23]*z[31]*z[71]-z[23]*z[56]*z[74]-z[23]*z[57]*z[78]-z[29]*z[43]*z[83]-z[29]*z[66]*z[83]-z[31]*z[43]*z[71]-z[31]*z[66]*z[71]-z[43]*z[56]*z[74]-z[43]*z[57]*z[78]-z[52]*z[71]*z[80]-z[53]*z[69]*z[80]-z[53]*z[70]*z[78]-z[53]*z[71]*z[76]-z[56]*z[66]*z[74]-z[57]*z[66]*z[78]);
	z[658] = IE13*z[22] + IE23*z[21] + z[38]*z[526] + z[38]*z[535] + z[39]*z[529] + z[39]*z[538] + z[40]*z[532] + z[40]*z[541] + z[40]*z[562] + z[40]*z[573] + z[40]*z[583] + z[40]*z[642] + z[64]*z[554] + z[64]*z[565] + z[64]*z[577] + z[65]*z[558] + z[65]*z[569] + z[65]*z[580] + 0.6427876096865393*z[154]*z[636] + 0.766044443118978*z[155]*z[639] + z[656]*(L1E*z[22]+L2E*z[21]) + MM5*(z[26]*z[99]+z[98]*z[99]-z[97]*z[31]*z[101]) + MF*(z[44]*z[45]+z[42]*z[29]*z[47]-z[26]*z[45]-z[42]*z[31]*z[48]) - z[609]*z[129] - MG*(z[67]*z[70]+z[68]*z[31]*z[72]+z[68]*z[31]*z[84]+z[52]*z[69]*z[76]+z[52]*z[70]*z[75]+z[53]*z[67]*z[80]-z[69]*z[70]-z[72]*z[83]-z[75]*z[76]-z[79]*z[80]-z[83]*z[84]-z[68]*z[54]*z[76]-z[68]*z[55]*z[80]-z[23]*z[29]*z[83]-z[23]*z[56]*z[75]-z[23]*z[57]*z[79]-z[29]*z[43]*z[83]-z[29]*z[66]*z[83]-z[43]*z[56]*z[75]-z[43]*z[57]*z[79]-z[52]*z[67]*z[76]-z[53]*z[69]*z[80]-z[53]*z[70]*z[79]-z[56]*z[66]*z[75]-z[57]*z[66]*z[79]);
	z[659] = IF11*z[38] + IF12*z[39] + IF13*z[40] + z[40]*z[561] + z[64]*z[553] + z[65]*z[557] + 0.766044443118978*IM511*z[154] + z[432]*(z[101]+z[23]*z[29]+z[29]*z[43]+z[29]*z[96]) + MF*(L2F*z[48]+L3F*z[47]+L2F*z[23]*z[29]+L2F*z[29]*z[41]+L2F*z[29]*z[43]+L3F*z[23]*z[31]+L3F*z[31]*z[41]+L3F*z[31]*z[43]) + MG*(z[51]*z[72]+z[51]*z[84]+z[72]*z[82]+z[82]*z[84]+z[51]*z[23]*z[29]+z[51]*z[29]*z[43]+z[51]*z[29]*z[66]+z[23]*z[29]*z[82]+z[29]*z[43]*z[82]+z[29]*z[66]*z[82]+z[52]*z[70]*z[77]-z[76]*z[77]-z[80]*z[81]-z[23]*z[56]*z[77]-z[23]*z[57]*z[81]-z[43]*z[56]*z[77]-z[43]*z[57]*z[81]-z[53]*z[70]*z[81]-z[56]*z[66]*z[77]-z[57]*z[66]*z[81]) - z[622]*z[22] - z[40]*z[575] - z[64]*z[567] - z[65]*z[571] - 0.6427876096865393*IM522*z[155];
	z[660] = IG13*z[64] + IG23*z[65] + IG33*z[40] - z[640]*z[155] - MG*(L2G*z[52]*z[70]-L1G*z[80]-L2G*z[76]-L1G*z[23]*z[57]-L1G*z[43]*z[57]-L1G*z[53]*z[70]-L1G*z[57]*z[66]-L2G*z[23]*z[56]-L2G*z[43]*z[56]-L2G*z[56]*z[66]);
	z[661] = z[595]*z[8];
	z[662] = z[8]*z[441] + z[8]*z[454] + z[8]*z[459] + z[8]*z[472] + z[8]*z[596] + z[8]*z[602] + z[9]*z[443] + z[9]*z[453] + z[9]*z[461] + z[9]*z[471] + z[9]*z[598] + z[9]*z[601] + z[16]*z[479] + z[16]*z[492] + z[17]*z[481] + z[17]*z[491] + z[21]*z[502] + z[21]*z[512] + z[21]*z[625] + z[21]*z[628] + z[22]*z[500] + z[22]*z[513] + z[22]*z[623] + z[22]*z[629] + z[38]*z[521] + z[38]*z[528] + z[38]*z[537] + z[38]*z[546] + z[39]*z[523] + z[39]*z[531] + z[39]*z[540] + z[39]*z[545] + z[40]*z[525] + z[40]*z[534] + z[40]*z[543] + z[40]*z[544] + z[40]*z[564] + z[40]*z[576] + z[40]*z[585] + z[40]*z[586] + z[40]*z[644] + z[40]*z[645] + z[64]*z[556] + z[64]*z[568] + z[64]*z[579] + z[64]*z[588] + z[65]*z[560] + z[65]*z[572] + z[65]*z[582] + z[65]*z[587] + z[127]*z[612] + z[127]*z[615] + z[129]*z[610] + z[129]*z[616] + z[154]*z[637] + z[154]*z[647] + z[155]*z[641] + z[155]*z[646] + MA*(L1A*z[164]-L3A*z[163]) + ME*(L3E*z[210]+z[23]*z[211]+z[23]*z[214]+z[23]*z[217]+z[24]*z[211]+z[24]*z[214]+z[24]*z[217]+L3E*z[17]*z[212]+L3E*z[21]*z[215]-z[14]*z[211]-z[14]*z[214]-z[14]*z[217]-L3E*z[16]*z[213]-L3E*z[22]*z[216]) - z[8]*z[447] - z[8]*z[465] - z[9]*z[449] - z[9]*z[467] - z[16]*z[485] - z[17]*z[487] - z[21]*z[508] - z[22]*z[506] - MB*(z[6]-z[10])*z[174] - MC*(z[6]-z[13])*z[184] - MM2*(z[6]-z[87])*z[312] - MM3*(z[14]-z[90])*z[321] - MM4*(z[14]*z[214]+z[14]*z[330]-z[23]*z[214]-z[23]*z[330]-z[93]*z[214]-z[93]*z[330]) - MD*(z[14]*z[193]+z[14]*z[196]+L3D*z[16]*z[195]-L3D*z[192]-z[18]*z[193]-z[18]*z[196]-L3D*z[17]*z[194]) - MM5*(z[99]*z[348]+z[31]*z[101]*z[346]+z[32]*z[101]*z[212]+z[35]*z[99]*z[343]-z[23]*z[214]-z[23]*z[344]-z[23]*z[347]-z[43]*z[214]-z[43]*z[344]-z[43]*z[347]-z[96]*z[214]-z[96]*z[344]-z[96]*z[347]-z[99]*z[345]-z[101]*z[350]-z[23]*z[29]*z[350]-z[23]*z[31]*z[349]-z[27]*z[99]*z[212]-z[28]*z[99]*z[213]-z[29]*z[43]*z[350]-z[29]*z[96]*z[350]-z[29]*z[101]*z[214]-z[29]*z[101]*z[344]-z[29]*z[101]*z[347]-z[31]*z[43]*z[349]-z[31]*z[96]*z[349]-z[34]*z[101]*z[213]-z[37]*z[101]*z[343]) - MF*(z[45]*z[245]+z[27]*z[45]*z[212]+z[28]*z[45]*z[213]+z[31]*z[48]*z[246]+z[32]*z[48]*z[212]+z[33]*z[47]*z[213]-z[23]*z[214]-z[23]*z[244]-z[23]*z[247]-z[41]*z[214]-z[41]*z[244]-z[41]*z[247]-z[43]*z[214]-z[43]*z[244]-z[43]*z[247]-z[45]*z[248]-z[47]*z[249]-z[48]*z[250]-z[23]*z[29]*z[250]-z[23]*z[31]*z[249]-z[29]*z[41]*z[250]-z[29]*z[43]*z[250]-z[29]*z[47]*z[246]-z[29]*z[48]*z[214]-z[29]*z[48]*z[244]-z[29]*z[48]*z[247]-z[30]*z[47]*z[212]-z[31]*z[41]*z[249]-z[31]*z[43]*z[249]-z[31]*z[47]*z[214]-z[31]*z[47]*z[244]-z[31]*z[47]*z[247]-z[34]*z[48]*z[213]-z[35]*z[45]*z[243]-z[36]*z[47]*z[243]-z[37]*z[48]*z[243]) - MG*(z[70]*z[295]+z[70]*z[298]+z[31]*z[72]*z[296]+z[31]*z[84]*z[296]+z[32]*z[72]*z[212]+z[32]*z[84]*z[212]+z[35]*z[70]*z[293]+z[52]*z[70]*z[301]+z[53]*z[80]*z[295]+z[53]*z[80]*z[298]+z[58]*z[76]*z[212]+z[59]*z[80]*z[212]+z[60]*z[76]*z[213]+z[61]*z[80]*z[213]-z[23]*z[214]-z[23]*z[294]-z[23]*z[297]-z[43]*z[214]-z[43]*z[294]-z[43]*z[297]-z[66]*z[214]-z[66]*z[294]-z[66]*z[297]-z[72]*z[300]-z[72]*z[303]-z[76]*z[301]-z[80]*z[302]-z[84]*z[300]-z[84]*z[303]-z[23]*z[29]*z[300]-z[23]*z[29]*z[303]-z[23]*z[31]*z[299]-z[23]*z[56]*z[301]-z[23]*z[57]*z[302]-z[27]*z[70]*z[212]-z[28]*z[70]*z[213]-z[29]*z[43]*z[300]-z[29]*z[43]*z[303]-z[29]*z[66]*z[300]-z[29]*z[66]*z[303]-z[29]*z[72]*z[214]-z[29]*z[72]*z[294]-z[29]*z[72]*z[297]-z[29]*z[84]*z[214]-z[29]*z[84]*z[294]-z[29]*z[84]*z[297]-z[31]*z[43]*z[299]-z[31]*z[66]*z[299]-z[34]*z[72]*z[213]-z[34]*z[84]*z[213]-z[37]*z[72]*z[293]-z[37]*z[84]*z[293]-z[43]*z[56]*z[301]-z[43]*z[57]*z[302]-z[52]*z[76]*z[295]-z[52]*z[76]*z[298]-z[52]*z[80]*z[299]-z[53]*z[70]*z[302]-z[53]*z[76]*z[299]-z[54]*z[76]*z[296]-z[55]*z[80]*z[296]-z[56]*z[66]*z[301]-z[56]*z[76]*z[214]-z[56]*z[76]*z[294]-z[56]*z[76]*z[297]-z[57]*z[66]*z[302]-z[57]*z[80]*z[214]-z[57]*z[80]*z[294]-z[57]*z[80]*z[297]-z[62]*z[76]*z[293]-z[63]*z[80]*z[293]);
	z[664] = z[2]*z[594];
	z[666] = -z[2]*(z[596]+z[602]) - MC*(z[182]+z[9]*z[181]);
	z[667] = z[503] + z[509] + z[29]*z[524] + z[29]*z[533] + z[29]*z[542] + z[29]*z[563] + z[29]*z[574] + z[29]*z[584] + z[29]*z[643] + z[31]*z[522] + z[31]*z[530] + z[31]*z[539] + z[56]*z[555] + z[56]*z[566] + z[56]*z[578] + z[57]*z[559] + z[57]*z[570] + z[57]*z[581] + 0.6427876096865393*z[31]*z[635] + 0.766044443118978*z[31]*z[638] + z[656]*(L1E*z[22]+L2E*z[21]) + MM5*(z[26]*z[99]+z[98]*z[99]-z[97]*z[31]*z[101]) + MF*(z[44]*z[45]+z[42]*z[29]*z[47]-z[26]*z[45]-z[42]*z[31]*z[48]) - z[3]*z[608] - MG*(z[67]*z[70]+z[68]*z[31]*z[72]+z[68]*z[31]*z[84]+z[52]*z[69]*z[76]+z[52]*z[70]*z[75]+z[53]*z[67]*z[80]-z[69]*z[70]-z[72]*z[83]-z[75]*z[76]-z[79]*z[80]-z[83]*z[84]-z[68]*z[54]*z[76]-z[68]*z[55]*z[80]-z[23]*z[29]*z[83]-z[23]*z[56]*z[75]-z[23]*z[57]*z[79]-z[29]*z[43]*z[83]-z[29]*z[66]*z[83]-z[43]*z[56]*z[75]-z[43]*z[57]*z[79]-z[52]*z[67]*z[76]-z[53]*z[69]*z[80]-z[53]*z[70]*z[79]-z[56]*z[66]*z[75]-z[57]*z[66]*z[79]);
	z[693] = z[668] + z[29]*z[532] + z[29]*z[541] + z[29]*z[562] + z[29]*z[573] + z[29]*z[583] + z[29]*z[642] + z[31]*z[529] + z[31]*z[538] + z[56]*z[554] + z[56]*z[565] + z[56]*z[577] + z[57]*z[558] + z[57]*z[569] + z[57]*z[580] + 0.4131759111665347*z[31]*z[636] + 0.5868240888334652*z[31]*z[639] - ME*(z[669]*z[28]-z[670]-z[671]-z[672]*z[27]-z[673]*z[28]-z[674]*z[27]) - MM4*(z[675]*z[28]-z[676]-z[677]-z[678]*z[27]-z[679]*z[28]-z[680]*z[27]) - MF*(z[681]*z[28]+2*z[26]*z[44]+z[20]*z[27]*z[44]-z[682]-z[683]-z[684]*z[27]-z[685]*z[27]-z[686]*z[28]-pow(z[44],2)-z[19]*z[28]*z[44]-z[42]*z[29]*z[46]-z[42]*z[31]*z[49]) - MM5*(z[681]*z[28]+z[19]*z[28]*z[98]-z[683]-z[687]-2*z[26]*z[98]-z[685]*z[27]-z[688]*z[27]-z[689]*z[28]-pow(z[98],2)-z[20]*z[27]*z[98]-z[97]*z[29]*z[100]-z[97]*z[31]*z[102]) - MG*(z[26]*z[67]+z[67]*z[69]+z[73]*z[83]+z[19]*z[28]*z[69]+z[19]*z[34]*z[83]+z[20]*z[27]*z[67]+z[20]*z[32]*z[83]+z[20]*z[58]*z[75]+z[20]*z[59]*z[79]+z[25]*z[31]*z[83]+z[26]*z[52]*z[75]+z[68]*z[31]*z[83]+z[52]*z[69]*z[74]+z[52]*z[69]*z[75]+z[53]*z[67]*z[78]-z[690]-z[26]*z[69]-z[691]*z[27]-z[692]*z[28]-pow(z[69],2)-z[74]*z[75]-z[78]*z[79]-pow(z[83],2)-z[19]*z[28]*z[67]-z[19]*z[60]*z[75]-z[19]*z[61]*z[79]-z[20]*z[27]*z[69]-z[25]*z[54]*z[75]-z[25]*z[55]*z[79]-z[26]*z[53]*z[79]-z[68]*z[29]*z[71]-z[68]*z[31]*z[73]-z[68]*z[54]*z[74]-z[68]*z[55]*z[78]-z[52]*z[67]*z[74]-z[52]*z[71]*z[79]-z[53]*z[69]*z[78]-z[53]*z[69]*z[79]-z[53]*z[71]*z[75]);
	z[698] = z[694] + z[29]*z[532] + z[29]*z[541] + z[29]*z[562] + z[29]*z[573] + z[29]*z[583] + z[29]*z[642] + z[31]*z[529] + z[31]*z[538] + z[56]*z[554] + z[56]*z[565] + z[56]*z[577] + z[57]*z[558] + z[57]*z[569] + z[57]*z[580] + 0.4131759111665347*z[31]*z[636] + 0.5868240888334652*z[31]*z[639] + MM5*(z[695]+pow(z[98],2)+2*z[26]*z[98]) - MF*(2*z[26]*z[44]-z[683]-z[696]-pow(z[44],2)) - MG*(2*z[67]*z[69]+2*z[68]*z[31]*z[83]+2*z[52]*z[69]*z[75]+2*z[53]*z[67]*z[79]-z[697]-pow(z[67],2)-pow(z[69],2)-pow(z[75],2)-pow(z[79],2)-pow(z[83],2)-2*z[68]*z[54]*z[75]-2*z[68]*z[55]*z[79]-2*z[52]*z[67]*z[75]-2*z[53]*z[69]*z[79]);
	z[702] = IF12*z[31] + IF13*z[29] + z[29]*z[561] + z[56]*z[553] + z[57]*z[557] + 0.492403876506104*IM511*z[31] + MG*(z[51]*z[83]+z[82]*z[83]+z[52]*z[69]*z[77]+z[53]*z[67]*z[81]-z[701]*z[31]-z[75]*z[77]-z[79]*z[81]-z[68]*z[31]*z[82]-z[68]*z[54]*z[77]-z[68]*z[55]*z[81]-z[52]*z[67]*z[77]-z[53]*z[69]*z[81]) - z[699]*z[31] - z[29]*z[575] - z[56]*z[567] - z[57]*z[571] - 0.492403876506104*IM522*z[31] - z[700]*(L2F*z[31]-L3F*z[29]);
	z[705] = IG13*z[56] + IG23*z[57] + IG33*z[29] + MG*(L1G*z[79]+L2G*z[75]+z[703]*z[55]+z[704]*z[54]+L1G*z[53]*z[69]+L2G*z[52]*z[67]-L1G*z[53]*z[67]-L2G*z[52]*z[69]) - 0.766044443118978*z[640]*z[31];
	z[706] = z[504] + z[511] + z[626] + z[627] + z[29]*z[525] + z[29]*z[534] + z[29]*z[543] + z[29]*z[544] + z[29]*z[564] + z[29]*z[576] + z[29]*z[585] + z[29]*z[586] + z[29]*z[644] + z[29]*z[645] + z[31]*z[523] + z[31]*z[531] + z[31]*z[540] + z[31]*z[545] + z[56]*z[556] + z[56]*z[568] + z[56]*z[579] + z[56]*z[588] + z[57]*z[560] + z[57]*z[572] + z[57]*z[582] + z[57]*z[587] + 0.6427876096865393*z[31]*z[637] + 0.6427876096865393*z[31]*z[647] + 0.766044443118978*z[31]*z[641] + 0.766044443118978*z[31]*z[646] + MM5*(z[26]*z[345]+z[97]*z[346]+z[98]*z[345]+z[26]*z[27]*z[212]+z[26]*z[28]*z[213]+z[97]*z[28]*z[212]+z[97]*z[29]*z[349]+z[27]*z[98]*z[212]+z[28]*z[98]*z[213]-z[26]*z[348]-z[98]*z[348]-z[26]*z[21]*z[343]-z[97]*z[22]*z[343]-z[97]*z[27]*z[213]-z[97]*z[31]*z[350]-z[35]*z[98]*z[343]) - z[510] - z[3]*(z[610]+z[616]) - ME*(L1E*z[216]+L1E*z[27]*z[213]-L2E*z[215]-L1E*z[22]*z[210]-L1E*z[28]*z[212]-L2E*z[21]*z[210]-L2E*z[27]*z[212]-L2E*z[28]*z[213]) - MM4*(z[91]*z[329]+z[91]*z[27]*z[213]-z[92]*z[328]-z[91]*z[22]*z[327]-z[91]*z[28]*z[212]-z[92]*z[21]*z[327]-z[92]*z[27]*z[212]-z[92]*z[28]*z[213]) - MF*(z[26]*z[248]+z[44]*z[245]+z[26]*z[21]*z[243]+z[42]*z[22]*z[243]+z[42]*z[27]*z[213]+z[42]*z[31]*z[250]+z[27]*z[44]*z[212]+z[28]*z[44]*z[213]-z[26]*z[245]-z[42]*z[246]-z[44]*z[248]-z[26]*z[27]*z[212]-z[26]*z[28]*z[213]-z[42]*z[28]*z[212]-z[42]*z[29]*z[249]-z[35]*z[44]*z[243]) - MG*(z[69]*z[295]+z[69]*z[298]+z[68]*z[22]*z[293]+z[68]*z[27]*z[213]+z[68]*z[31]*z[300]+z[68]*z[31]*z[303]+z[27]*z[67]*z[212]+z[28]*z[67]*z[213]+z[31]*z[83]*z[296]+z[32]*z[83]*z[212]+z[35]*z[69]*z[293]+z[52]*z[69]*z[301]+z[53]*z[67]*z[302]+z[53]*z[79]*z[295]+z[53]*z[79]*z[298]+z[58]*z[75]*z[212]+z[59]*z[79]*z[212]+z[60]*z[75]*z[213]+z[61]*z[79]*z[213]-z[68]*z[296]-z[67]*z[295]-z[67]*z[298]-z[75]*z[301]-z[79]*z[302]-z[83]*z[300]-z[83]*z[303]-z[68]*z[28]*z[212]-z[68]*z[29]*z[299]-z[68]*z[54]*z[301]-z[68]*z[55]*z[302]-z[21]*z[67]*z[293]-z[27]*z[69]*z[212]-z[28]*z[69]*z[213]-z[29]*z[83]*z[214]-z[29]*z[83]*z[294]-z[29]*z[83]*z[297]-z[34]*z[83]*z[213]-z[37]*z[83]*z[293]-z[52]*z[67]*z[301]-z[52]*z[75]*z[295]-z[52]*z[75]*z[298]-z[52]*z[79]*z[299]-z[53]*z[69]*z[302]-z[53]*z[75]*z[299]-z[54]*z[75]*z[296]-z[55]*z[79]*z[296]-z[56]*z[75]*z[214]-z[56]*z[75]*z[294]-z[56]*z[75]*z[297]-z[57]*z[79]*z[214]-z[57]*z[79]*z[294]-z[57]*z[79]*z[297]-z[62]*z[75]*z[293]-z[63]*z[79]*z[293]);
	z[709] = z[707] + z[52]*z[553] + z[53]*z[571] + MG*(z[708]+pow(z[77],2)+pow(z[81],2)+pow(z[82],2)+2*z[51]*z[82]) - z[52]*z[567] - z[53]*z[557];
	z[710] = z[520] + z[527] + z[536] + 0.766044443118978*z[635] + z[52]*z[555] + z[52]*z[566] + z[52]*z[578] + z[432]*(z[101]+z[23]*z[29]+z[29]*z[43]+z[29]*z[96]) + MF*(L2F*z[48]+L3F*z[47]+L2F*z[23]*z[29]+L2F*z[29]*z[41]+L2F*z[29]*z[43]+L3F*z[23]*z[31]+L3F*z[31]*z[41]+L3F*z[31]*z[43]) + MG*(z[51]*z[72]+z[51]*z[84]+z[72]*z[82]+z[82]*z[84]+z[51]*z[23]*z[29]+z[51]*z[29]*z[43]+z[51]*z[29]*z[66]+z[23]*z[29]*z[82]+z[29]*z[43]*z[82]+z[29]*z[66]*z[82]+z[52]*z[70]*z[77]-z[76]*z[77]-z[80]*z[81]-z[23]*z[56]*z[77]-z[23]*z[57]*z[81]-z[43]*z[56]*z[77]-z[43]*z[57]*z[81]-z[53]*z[70]*z[81]-z[56]*z[66]*z[77]-z[57]*z[66]*z[81]) - 0.6427876096865393*z[638] - z[4]*z[621] - z[53]*z[559] - z[53]*z[570] - z[53]*z[581];
	z[720] = z[526] + z[535] + 0.492403876506104*z[636] + z[52]*z[554] + z[52]*z[565] + z[52]*z[577] + MG*(z[51]*z[83]+z[82]*z[83]+z[20]*z[58]*z[77]+z[20]*z[59]*z[81]+z[26]*z[52]*z[77]+z[52]*z[69]*z[77]-z[51]*z[73]-z[711]*z[34]-z[712]*z[32]-z[713]*z[31]-z[73]*z[82]-z[74]*z[77]-z[78]*z[81]-z[19]*z[34]*z[82]-z[19]*z[60]*z[77]-z[19]*z[61]*z[81]-z[20]*z[32]*z[82]-z[25]*z[31]*z[82]-z[25]*z[54]*z[77]-z[25]*z[55]*z[81]-z[26]*z[53]*z[81]-z[52]*z[71]*z[81]-z[53]*z[69]*z[81]-z[53]*z[71]*z[77]) - 0.492403876506104*z[639] - z[53]*z[558] - z[53]*z[569] - z[53]*z[580] - z[432]*(z[102]+z[19]*z[34]+z[20]*z[32]+z[25]*z[31]) - MF*(L2F*z[49]+z[714]*z[34]+z[715]*z[32]+z[716]*z[31]-L3F*z[46]-z[717]*z[33]-z[718]*z[30]-z[719]*z[29]);
	z[721] = z[526] + z[535] + 0.492403876506104*z[636] + z[52]*z[554] + z[52]*z[565] + z[52]*z[577] + MG*(z[51]*z[83]+z[82]*z[83]+z[52]*z[69]*z[77]+z[53]*z[67]*z[81]-z[701]*z[31]-z[75]*z[77]-z[79]*z[81]-z[68]*z[31]*z[82]-z[68]*z[54]*z[77]-z[68]*z[55]*z[81]-z[52]*z[67]*z[77]-z[53]*z[69]*z[81]) - 0.492403876506104*z[639] - z[699]*z[31] - z[53]*z[558] - z[53]*z[569] - z[53]*z[580] - z[700]*(L2F*z[31]-L3F*z[29]);
	z[722] = 0.6427876096865393*z[640] + IG13*z[52] - IG23*z[53] - MG*(L1G*z[81]+L2G*z[77]);
	z[723] = z[521] + z[528] + z[537] + z[546] + 0.766044443118978*z[637] + 0.766044443118978*z[647] + z[52]*z[556] + z[52]*z[568] + z[52]*z[579] + z[52]*z[588] + MG*(z[51]*z[300]+z[51]*z[303]+z[82]*z[300]+z[82]*z[303]+z[51]*z[29]*z[214]+z[51]*z[29]*z[294]+z[51]*z[29]*z[297]+z[51]*z[34]*z[213]+z[51]*z[37]*z[293]+z[29]*z[82]*z[214]+z[29]*z[82]*z[294]+z[29]*z[82]*z[297]+z[34]*z[82]*z[213]+z[37]*z[82]*z[293]+z[53]*z[81]*z[295]+z[53]*z[81]*z[298]+z[58]*z[77]*z[212]+z[59]*z[81]*z[212]+z[60]*z[77]*z[213]+z[61]*z[81]*z[213]-z[77]*z[301]-z[81]*z[302]-z[51]*z[31]*z[296]-z[51]*z[32]*z[212]-z[31]*z[82]*z[296]-z[32]*z[82]*z[212]-z[52]*z[77]*z[295]-z[52]*z[77]*z[298]-z[52]*z[81]*z[299]-z[53]*z[77]*z[299]-z[54]*z[77]*z[296]-z[55]*z[81]*z[296]-z[56]*z[77]*z[214]-z[56]*z[77]*z[294]-z[56]*z[77]*z[297]-z[57]*z[81]*z[214]-z[57]*z[81]*z[294]-z[57]*z[81]*z[297]-z[62]*z[77]*z[293]-z[63]*z[81]*z[293]) - 0.6427876096865393*z[641] - 0.6427876096865393*z[646] - z[53]*z[560] - z[53]*z[572] - z[53]*z[582] - z[53]*z[587] - z[4]*(z[623]+z[629]) - z[432]*(z[31]*z[346]+z[32]*z[212]-z[350]-z[29]*z[214]-z[29]*z[344]-z[29]*z[347]-z[34]*z[213]-z[37]*z[343]) - MF*(L2F*z[31]*z[246]+L2F*z[32]*z[212]+L3F*z[33]*z[213]-L2F*z[250]-L3F*z[249]-L2F*z[29]*z[214]-L2F*z[29]*z[244]-L2F*z[29]*z[247]-L2F*z[34]*z[213]-L2F*z[37]*z[243]-L3F*z[29]*z[246]-L3F*z[30]*z[212]-L3F*z[31]*z[214]-L3F*z[31]*z[244]-L3F*z[31]*z[247]-L3F*z[36]*z[243]);
	z[725] = 0.6427876096865393*z[640] + z[561] - z[575] - MG*(L1G*z[81]+L2G*z[77]);
	z[734] = z[562] + z[573] + z[583] - 0.766044443118978*z[5]*z[639] - MG*(z[726]*z[59]+z[727]*z[58]+z[728]*z[52]+L2G*z[52]*z[69]-L1G*z[78]-L2G*z[74]-z[729]*z[61]-z[730]*z[55]-z[731]*z[53]-z[732]*z[60]-z[733]*z[54]-L1G*z[52]*z[71]-L1G*z[53]*z[69]-L2G*z[53]*z[71]);
	z[735] = z[562] + z[573] + z[583] + MG*(L1G*z[79]+L2G*z[75]+z[703]*z[55]+z[704]*z[54]+L1G*z[53]*z[69]+L2G*z[52]*z[67]-L1G*z[53]*z[67]-L2G*z[52]*z[69]) - 0.766044443118978*z[5]*z[639];
	z[736] = z[563] + z[574] + z[584] - z[5]*z[638] - MG*(L2G*z[52]*z[70]-L1G*z[80]-L2G*z[76]-L1G*z[23]*z[57]-L1G*z[43]*z[57]-L1G*z[53]*z[70]-L1G*z[57]*z[66]-L2G*z[23]*z[56]-L2G*z[43]*z[56]-L2G*z[56]*z[66]);
	z[737] = z[564] + z[576] + z[585] + z[586] - z[5]*(z[641]+z[646]) - MG*(L1G*z[53]*z[295]+L1G*z[53]*z[298]+L1G*z[59]*z[212]+L1G*z[61]*z[213]+L2G*z[58]*z[212]+L2G*z[60]*z[213]-L1G*z[302]-L2G*z[301]-L1G*z[52]*z[299]-L1G*z[55]*z[296]-L1G*z[57]*z[214]-L1G*z[57]*z[294]-L1G*z[57]*z[297]-L1G*z[63]*z[293]-L2G*z[52]*z[295]-L2G*z[52]*z[298]-L2G*z[53]*z[299]-L2G*z[54]*z[296]-L2G*z[56]*z[214]-L2G*z[56]*z[294]-L2G*z[56]*z[297]-L2G*z[62]*z[293]);
	z[740] = z[738] + MC*(z[739]+pow(z[12],2));
	z[741] = z[444] + z[450] + z[462] + z[468];
	z[742] = z[445] + z[452] + z[463] + z[470] + z[599] + z[600] + MC*(L2B*z[182]+z[12]*z[183]+L2B*z[9]*z[181]+z[8]*z[12]*z[181]) - z[451] - z[469] - MB*(L1B*z[173]-L2B*z[172]-L1B*z[8]*z[171]-L2B*z[9]*z[171]) - MM2*(z[85]*z[311]-z[86]*z[310]-z[85]*z[8]*z[309]-z[86]*z[9]*z[309]);z[749] = z[743] + z[29]*z[532] + z[29]*z[541] + z[29]*z[562] + z[29]*z[573] + z[29]*z[583] + z[29]*z[642] + z[31]*z[529] + z[31]*z[538] + z[56]*z[554] + z[56]*z[565] + z[56]*z[577] + z[57]*z[558] + z[57]*z[569] + z[57]*z[580] + 0.4131759111665347*z[31]*z[636] + 0.5868240888334652*z[31]*z[639] - ME*(2*z[669]*z[28]-z[670]-z[671]-z[744]-z[745]-2*z[672]*z[27]-2*z[673]*z[28]-2*z[674]*z[27]) - MM4*(2*z[675]*z[28]-z[676]-z[677]-z[744]-z[745]-2*z[678]*z[27]-2*z[679]*z[28]-2*z[680]*z[27]) - MF*(2*z[26]*z[44]+2*z[681]*z[28]+2*z[20]*z[27]*z[44]-z[683]-z[744]-z[745]-z[746]-2*z[685]*z[27]-2*z[747]*z[27]-2*z[748]*z[28]-pow(z[44],2)-pow(z[46],2)-pow(z[49],2)-2*z[19]*z[28]*z[44]-2*z[19]*z[33]*z[46]-2*z[19]*z[34]*z[49]-2*z[20]*z[30]*z[46]-2*z[20]*z[32]*z[49]-2*z[25]*z[29]*z[46]-2*z[25]*z[31]*z[49]) - MM5*(2*z[681]*z[28]+2*z[19]*z[28]*z[98]-z[683]-z[744]-z[745]-z[746]-2*z[26]*z[98]-2*z[685]*z[27]-2*z[747]*z[27]-2*z[748]*z[28]-pow(z[98],2)-pow(z[100],2)-pow(z[102],2)-2*z[19]*z[33]*z[100]-2*z[19]*z[34]*z[102]-2*z[20]*z[27]*z[98]-2*z[20]*z[30]*z[100]-2*z[20]*z[32]*z[102]-2*z[25]*z[29]*z[100]-2*z[25]*z[31]*z[102]) - MG*(2*z[681]*z[28]+2*z[73]*z[83]+2*z[19]*z[28]*z[69]+2*z[19]*z[34]*z[83]+2*z[20]*z[32]*z[83]+2*z[20]*z[58]*z[74]+2*z[20]*z[59]*z[78]+2*z[25]*z[31]*z[83]+2*z[26]*z[52]*z[74]+2*z[52]*z[69]*z[74]-z[683]-z[744]-z[745]-z[746]-2*z[26]*z[69]-2*z[685]*z[27]-2*z[747]*z[27]-2*z[748]*z[28]-pow(z[69],2)-pow(z[71],2)-pow(z[73],2)-pow(z[74],2)-pow(z[78],2)-pow(z[83],2)-2*z[19]*z[33]*z[71]-2*z[19]*z[34]*z[73]-2*z[19]*z[60]*z[74]-2*z[19]*z[61]*z[78]-2*z[20]*z[27]*z[69]-2*z[20]*z[30]*z[71]-2*z[20]*z[32]*z[73]-2*z[25]*z[29]*z[71]-2*z[25]*z[31]*z[73]-2*z[25]*z[54]*z[74]-2*z[25]*z[55]*z[78]-2*z[26]*z[53]*z[78]-2*z[52]*z[71]*z[78]-2*z[53]*z[69]*z[78]-2*z[53]*z[71]*z[74]);
	z[750] = z[482] + z[488] + z[503] + z[509] + z[29]*z[524] + z[29]*z[533] + z[29]*z[542] + z[29]*z[563] + z[29]*z[574] + z[29]*z[584] + z[29]*z[643] + z[31]*z[522] + z[31]*z[530] + z[31]*z[539] + z[56]*z[555] + z[56]*z[566] + z[56]*z[578] + z[57]*z[559] + z[57]*z[570] + z[57]*z[581] + 0.6427876096865393*z[31]*z[635] + 0.766044443118978*z[31]*z[638] + z[655]*(L1X*z[16]+L2X*z[17]) + z[656]*(L1E*z[22]+L2E*z[21]+z[19]*z[16]+z[20]*z[17]) - MM5*(z[101]*z[102]+z[19]*z[28]*z[99]+z[19]*z[34]*z[101]+z[20]*z[32]*z[101]+z[25]*z[31]*z[101]+z[23]*z[29]*z[102]+z[29]*z[43]*z[102]+z[29]*z[96]*z[102]-z[26]*z[99]-z[98]*z[99]-z[20]*z[27]*z[99]-z[23]*z[31]*z[100]-z[31]*z[43]*z[100]-z[31]*z[96]*z[100]) - MF*(z[26]*z[45]+z[48]*z[49]+z[19]*z[34]*z[48]+z[20]*z[27]*z[45]+z[20]*z[32]*z[48]+z[25]*z[31]*z[48]+z[23]*z[29]*z[49]+z[29]*z[41]*z[49]+z[29]*z[43]*z[49]-z[44]*z[45]-z[46]*z[47]-z[19]*z[28]*z[45]-z[19]*z[33]*z[47]-z[20]*z[30]*z[47]-z[25]*z[29]*z[47]-z[23]*z[31]*z[46]-z[31]*z[41]*z[46]-z[31]*z[43]*z[46]) - MG*(z[72]*z[73]+z[73]*z[84]+z[19]*z[28]*z[70]+z[19]*z[34]*z[72]+z[19]*z[34]*z[84]+z[20]*z[32]*z[72]+z[20]*z[32]*z[84]+z[20]*z[58]*z[76]+z[20]*z[59]*z[80]+z[25]*z[31]*z[72]+z[25]*z[31]*z[84]+z[26]*z[52]*z[76]+z[23]*z[29]*z[73]+z[29]*z[43]*z[73]+z[29]*z[66]*z[73]+z[52]*z[69]*z[76]+z[52]*z[70]*z[74]-z[26]*z[70]-z[69]*z[70]-z[72]*z[83]-z[74]*z[76]-z[78]*z[80]-z[83]*z[84]-z[19]*z[60]*z[76]-z[19]*z[61]*z[80]-z[20]*z[27]*z[70]-z[25]*z[54]*z[76]-z[25]*z[55]*z[80]-z[26]*z[53]*z[80]-z[23]*z[29]*z[83]-z[23]*z[31]*z[71]-z[23]*z[56]*z[74]-z[23]*z[57]*z[78]-z[29]*z[43]*z[83]-z[29]*z[66]*z[83]-z[31]*z[43]*z[71]-z[31]*z[66]*z[71]-z[43]*z[56]*z[74]-z[43]*z[57]*z[78]-z[52]*z[71]*z[80]-z[53]*z[69]*z[80]-z[53]*z[70]*z[78]-z[53]*z[71]*z[76]-z[56]*z[66]*z[74]-z[57]*z[66]*z[78]);
	z[751] = IF12*z[31] + IF13*z[29] + z[29]*z[561] + z[56]*z[553] + z[57]*z[557] + 0.492403876506104*IM511*z[31] + MG*(z[51]*z[83]+z[82]*z[83]+z[20]*z[58]*z[77]+z[20]*z[59]*z[81]+z[26]*z[52]*z[77]+z[52]*z[69]*z[77]-z[51]*z[73]-z[711]*z[34]-z[712]*z[32]-z[713]*z[31]-z[73]*z[82]-z[74]*z[77]-z[78]*z[81]-z[19]*z[34]*z[82]-z[19]*z[60]*z[77]-z[19]*z[61]*z[81]-z[20]*z[32]*z[82]-z[25]*z[31]*z[82]-z[25]*z[54]*z[77]-z[25]*z[55]*z[81]-z[26]*z[53]*z[81]-z[52]*z[71]*z[81]-z[53]*z[69]*z[81]-z[53]*z[71]*z[77]) - z[29]*z[575] - z[56]*z[567] - z[57]*z[571] - 0.492403876506104*IM522*z[31] - z[432]*(z[102]+z[19]*z[34]+z[20]*z[32]+z[25]*z[31]) - MF*(L2F*z[49]+z[714]*z[34]+z[715]*z[32]+z[716]*z[31]-L3F*z[46]-z[717]*z[33]-z[718]*z[30]-z[719]*z[29]);
	z[752] = IG13*z[56] + IG23*z[57] + IG33*z[29] - 0.766044443118978*z[640]*z[31] - MG*(z[726]*z[59]+z[727]*z[58]+z[728]*z[52]+L2G*z[52]*z[69]-L1G*z[78]-L2G*z[74]-z[729]*z[61]-z[730]*z[55]-z[731]*z[53]-z[732]*z[60]-z[733]*z[54]-L1G*z[52]*z[71]-L1G*z[53]*z[69]-L2G*z[53]*z[71]);
	z[753] = z[483] + z[490] + z[504] + z[511] + z[613] + z[614] + z[626] + z[627] + z[29]*z[525] + z[29]*z[534] + z[29]*z[543] + z[29]*z[544] + z[29]*z[564] + z[29]*z[576] + z[29]*z[585] + z[29]*z[586] + z[29]*z[644] + z[29]*z[645] + z[31]*z[523] + z[31]*z[531] + z[31]*z[540] + z[31]*z[545] + z[56]*z[556] + z[56]*z[568] + z[56]*z[579] + z[56]*z[588] + z[57]*z[560] + z[57]*z[572] + z[57]*z[582] + z[57]*z[587] + 0.6427876096865393*z[31]*z[637] + 0.6427876096865393*z[31]*z[647] + 0.766044443118978*z[31]*z[641] + 0.766044443118978*z[31]*z[646] + ME*(L2E*z[215]+z[20]*z[212]+L1E*z[22]*z[210]+L1E*z[28]*z[212]+L2E*z[21]*z[210]+L2E*z[27]*z[212]+L2E*z[28]*z[213]+z[19]*z[16]*z[210]+z[20]*z[17]*z[210]+z[20]*z[27]*z[215]-L1E*z[216]-z[19]*z[213]-L1E*z[27]*z[213]-z[19]*z[27]*z[216]-z[19]*z[28]*z[215]-z[20]*z[28]*z[216]) + MM5*(z[20]*z[212]+z[25]*z[346]+z[26]*z[345]+z[98]*z[345]+z[100]*z[349]+z[19]*z[27]*z[346]+z[19]*z[28]*z[348]+z[19]*z[33]*z[349]+z[20]*z[27]*z[345]+z[20]*z[28]*z[346]+z[20]*z[30]*z[349]+z[25]*z[28]*z[212]+z[25]*z[29]*z[349]+z[26]*z[27]*z[212]+z[26]*z[28]*z[213]+z[27]*z[98]*z[212]+z[28]*z[98]*z[213]+z[29]*z[100]*z[346]+z[30]*z[100]*z[212]+z[31]*z[100]*z[214]+z[31]*z[100]*z[344]+z[31]*z[100]*z[347]+z[31]*z[102]*z[346]+z[32]*z[102]*z[212]+z[36]*z[100]*z[343]-z[19]*z[213]-z[26]*z[348]-z[98]*z[348]-z[102]*z[350]-z[19]*z[16]*z[343]-z[19]*z[28]*z[345]-z[19]*z[34]*z[350]-z[20]*z[17]*z[343]-z[20]*z[27]*z[348]-z[20]*z[32]*z[350]-z[25]*z[22]*z[343]-z[25]*z[27]*z[213]-z[25]*z[31]*z[350]-z[26]*z[21]*z[343]-z[29]*z[102]*z[214]-z[29]*z[102]*z[344]-z[29]*z[102]*z[347]-z[33]*z[100]*z[213]-z[34]*z[102]*z[213]-z[35]*z[98]*z[343]-z[37]*z[102]*z[343]) + MF*(z[20]*z[212]+z[25]*z[246]+z[26]*z[245]+z[44]*z[248]+z[46]*z[249]+z[19]*z[27]*z[246]+z[19]*z[28]*z[248]+z[19]*z[33]*z[249]+z[20]*z[27]*z[245]+z[20]*z[28]*z[246]+z[20]*z[30]*z[249]+z[25]*z[28]*z[212]+z[25]*z[29]*z[249]+z[26]*z[27]*z[212]+z[26]*z[28]*z[213]+z[29]*z[46]*z[246]+z[30]*z[46]*z[212]+z[31]*z[46]*z[214]+z[31]*z[46]*z[244]+z[31]*z[46]*z[247]+z[31]*z[49]*z[246]+z[32]*z[49]*z[212]+z[35]*z[44]*z[243]+z[36]*z[46]*z[243]-z[19]*z[213]-z[26]*z[248]-z[44]*z[245]-z[49]*z[250]-z[19]*z[16]*z[243]-z[19]*z[28]*z[245]-z[19]*z[34]*z[250]-z[20]*z[17]*z[243]-z[20]*z[27]*z[248]-z[20]*z[32]*z[250]-z[25]*z[22]*z[243]-z[25]*z[27]*z[213]-z[25]*z[31]*z[250]-z[26]*z[21]*z[243]-z[27]*z[44]*z[212]-z[28]*z[44]*z[213]-z[29]*z[49]*z[214]-z[29]*z[49]*z[244]-z[29]*z[49]*z[247]-z[33]*z[46]*z[213]-z[34]*z[49]*z[213]-z[37]*z[49]*z[243]) - z[489] - z[510] - MD*(L1X*z[195]-L2X*z[194]-L1X*z[16]*z[192]-L2X*z[17]*z[192]) - MM3*(z[88]*z[320]-z[89]*z[319]-z[88]*z[16]*z[318]-z[89]*z[17]*z[318]) - MM4*(z[19]*z[213]+z[91]*z[329]+z[19]*z[27]*z[329]+z[19]*z[28]*z[328]+z[20]*z[28]*z[329]+z[91]*z[27]*z[213]-z[20]*z[212]-z[92]*z[328]-z[19]*z[16]*z[327]-z[20]*z[17]*z[327]-z[20]*z[27]*z[328]-z[91]*z[22]*z[327]-z[91]*z[28]*z[212]-z[92]*z[21]*z[327]-z[92]*z[27]*z[212]-z[92]*z[28]*z[213]) - MG*(z[19]*z[213]+z[26]*z[295]+z[26]*z[298]+z[69]*z[295]+z[69]*z[298]+z[73]*z[300]+z[73]*z[303]+z[19]*z[16]*z[293]+z[19]*z[34]*z[300]+z[19]*z[34]*z[303]+z[20]*z[17]*z[293]+z[20]*z[27]*z[295]+z[20]*z[27]*z[298]+z[20]*z[32]*z[300]+z[20]*z[32]*z[303]+z[20]*z[58]*z[301]+z[20]*z[59]*z[302]+z[25]*z[22]*z[293]+z[25]*z[27]*z[213]+z[25]*z[31]*z[300]+z[25]*z[31]*z[303]+z[26]*z[21]*z[293]+z[26]*z[52]*z[301]+z[29]*z[73]*z[214]+z[29]*z[73]*z[294]+z[29]*z[73]*z[297]+z[31]*z[83]*z[296]+z[32]*z[83]*z[212]+z[33]*z[71]*z[213]+z[34]*z[73]*z[213]+z[35]*z[69]*z[293]+z[37]*z[73]*z[293]+z[52]*z[69]*z[301]+z[53]*z[78]*z[295]+z[53]*z[78]*z[298]+z[58]*z[74]*z[212]+z[59]*z[78]*z[212]+z[60]*z[74]*z[213]+z[61]*z[78]*z[213]-z[20]*z[212]-z[25]*z[296]-z[71]*z[299]-z[74]*z[301]-z[78]*z[302]-z[83]*z[300]-z[83]*z[303]-z[19]*z[27]*z[296]-z[19]*z[28]*z[295]-z[19]*z[28]*z[298]-z[19]*z[33]*z[299]-z[19]*z[60]*z[301]-z[19]*z[61]*z[302]-z[20]*z[28]*z[296]-z[20]*z[30]*z[299]-z[25]*z[28]*z[212]-z[25]*z[29]*z[299]-z[25]*z[54]*z[301]-z[25]*z[55]*z[302]-z[26]*z[27]*z[212]-z[26]*z[28]*z[213]-z[26]*z[53]*z[302]-z[27]*z[69]*z[212]-z[28]*z[69]*z[213]-z[29]*z[71]*z[296]-z[29]*z[83]*z[214]-z[29]*z[83]*z[294]-z[29]*z[83]*z[297]-z[30]*z[71]*z[212]-z[31]*z[71]*z[214]-z[31]*z[71]*z[294]-z[31]*z[71]*z[297]-z[31]*z[73]*z[296]-z[32]*z[73]*z[212]-z[34]*z[83]*z[213]-z[36]*z[71]*z[293]-z[37]*z[83]*z[293]-z[52]*z[71]*z[302]-z[52]*z[74]*z[295]-z[52]*z[74]*z[298]-z[52]*z[78]*z[299]-z[53]*z[69]*z[302]-z[53]*z[71]*z[301]-z[53]*z[74]*z[299]-z[54]*z[74]*z[296]-z[55]*z[78]*z[296]-z[56]*z[74]*z[214]-z[56]*z[74]*z[294]-z[56]*z[74]*z[297]-z[57]*z[78]*z[214]-z[57]*z[78]*z[294]-z[57]*z[78]*z[297]-z[62]*z[74]*z[293]-z[63]*z[78]*z[293]);
	z[754] = (z[654]*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))+z[657]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8])))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9])) - z[661];
	z[755] = z[662] - (z[654]*(2*(z[8]*(z[368]*z[16]+z[369]*z[17])+z[9]*(z[368]*z[17]-z[369]*z[16]))*U2*U3+(z[368]*z[16]*(z[368]*z[16]+z[369]*z[17])+z[369]*z[17]*(z[368]*z[16]+z[369]*z[17])+z[368]*z[17]*(z[368]*z[17]-z[369]*z[16])-z[369]*z[16]*(z[368]*z[17]-z[369]*z[16]))*pow(U4,2)-(L2B*z[8]*(z[368]*z[16]+z[369]*z[17])+L2B*z[9]*(z[368]*z[17]-z[369]*z[16])+(z[370]-Q3)*z[9]*(z[368]*z[16]+z[369]*z[17])-(z[370]-Q3)*z[8]*(z[368]*z[17]-z[369]*z[16]))*pow(U2,2))+z[657]*(2*(z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[9]*(L2B*z[9]-(z[370]-Q3)*z[8]))*U2*U3+(z[368]*z[16]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[369]*z[17]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[368]*z[17]*(L2B*z[9]-(z[370]-Q3)*z[8])-z[369]*z[16]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U4,2)-(L2B*z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+L2B*z[9]*(L2B*z[9]-(z[370]-Q3)*z[8])+(z[370]-Q3)*z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-(z[370]-Q3)*z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U2,2)))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[756] = z[663] - z[665]*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[757] = (z[741]*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))+z[750]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8])))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9])) - z[664];
	z[758] = z[740]*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9])) - z[665];
	z[759] = z[749]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[760] = z[693]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[761] = z[751]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[762] = z[752]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[763] = z[666] + ((z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))*z[742]+(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*z[753])/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[764] = z[756] + (z[758]*(z[9]*(z[368]*z[16]+z[369]*z[17])-z[8]*(z[368]*z[17]-z[369]*z[16]))+z[759]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8])))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[765] = z[763] - (z[758]*(2*(z[8]*(z[368]*z[16]+z[369]*z[17])+z[9]*(z[368]*z[17]-z[369]*z[16]))*U2*U3+(z[368]*z[16]*(z[368]*z[16]+z[369]*z[17])+z[369]*z[17]*(z[368]*z[16]+z[369]*z[17])+z[368]*z[17]*(z[368]*z[17]-z[369]*z[16])-z[369]*z[16]*(z[368]*z[17]-z[369]*z[16]))*pow(U4,2)-(L2B*z[8]*(z[368]*z[16]+z[369]*z[17])+L2B*z[9]*(z[368]*z[17]-z[369]*z[16])+(z[370]-Q3)*z[9]*(z[368]*z[16]+z[369]*z[17])-(z[370]-Q3)*z[8]*(z[368]*z[17]-z[369]*z[16]))*pow(U2,2))+z[759]*(2*(z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[9]*(L2B*z[9]-(z[370]-Q3)*z[8]))*U2*U3+(z[368]*z[16]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[369]*z[17]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[368]*z[17]*(L2B*z[9]-(z[370]-Q3)*z[8])-z[369]*z[16]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U4,2)-(L2B*z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+L2B*z[9]*(L2B*z[9]-(z[370]-Q3)*z[8])+(z[370]-Q3)*z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-(z[370]-Q3)*z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U2,2)))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[766] = z[706] - z[693]*(2*(z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[9]*(L2B*z[9]-(z[370]-Q3)*z[8]))*U2*U3+(z[368]*z[16]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[369]*z[17]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[368]*z[17]*(L2B*z[9]-(z[370]-Q3)*z[8])-z[369]*z[16]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U4,2)-(L2B*z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+L2B*z[9]*(L2B*z[9]-(z[370]-Q3)*z[8])+(z[370]-Q3)*z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-(z[370]-Q3)*z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U2,2))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[767] = z[720]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[768] = z[723] - z[720]*(2*(z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[9]*(L2B*z[9]-(z[370]-Q3)*z[8]))*U2*U3+(z[368]*z[16]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[369]*z[17]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[368]*z[17]*(L2B*z[9]-(z[370]-Q3)*z[8])-z[369]*z[16]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U4,2)-(L2B*z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+L2B*z[9]*(L2B*z[9]-(z[370]-Q3)*z[8])+(z[370]-Q3)*z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-(z[370]-Q3)*z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U2,2))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[769] = z[734]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[770] = z[737] - z[734]*(2*(z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[9]*(L2B*z[9]-(z[370]-Q3)*z[8]))*U2*U3+(z[368]*z[16]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[369]*z[17]*(L2B*z[8]+(z[370]-Q3)*z[9])+z[368]*z[17]*(L2B*z[9]-(z[370]-Q3)*z[8])-z[369]*z[16]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U4,2)-(L2B*z[8]*(L2B*z[8]+(z[370]-Q3)*z[9])+L2B*z[9]*(L2B*z[9]-(z[370]-Q3)*z[8])+(z[370]-Q3)*z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-(z[370]-Q3)*z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*pow(U2,2))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[771] = z[429]*U1 + z[430]*U3 + z[431]*U5 + z[433]*U6 - z[434]*U7;
	z[775] = z[23]*z[377] + z[43]*z[377] + z[66]*z[377] + z[72]*z[405] + z[108]*z[417] + z[111]*z[405] - z[70]*z[393] - z[106]*z[414];
	z[776] = (z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*(Dummy10)/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[777] = z[68]*z[395] + z[107]*z[417] + z[110]*z[405] - z[26]*z[393] - z[69]*z[393] - z[105]*z[414];
	z[778] = z[405]*(Dummy7);
	z[779] = z[103]*z[417] - z[104]*z[414];
	z[780] = z[108]*z[418] - z[38]*z[70] - z[72]*z[406] - z[106]*z[415] - z[111]*z[406];
	z[781] = (z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*(Dummy8)/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[782] = z[68]*z[35] + z[107]*z[418] - z[26]*z[38] - z[38]*z[69] - z[105]*z[415] - z[110]*z[406];
	z[783] = z[406]*(Dummy7);
	z[784] = z[103]*z[418] - z[104]*z[415];
	z[785] = z[23]*z[375] + z[43]*z[375] + z[66]*z[375] + z[72]*z[407] + z[108]*z[419] + z[111]*z[407] - z[70]*z[394] - z[106]*z[416];
	z[786] = (z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))*(Dummy11)/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[787] = z[68]*z[396] + z[107]*z[419] + z[110]*z[407] - z[26]*z[394] - z[69]*z[394] - z[105]*z[416];
	z[788] = z[407]*(Dummy7);
	z[789] = z[103]*z[419] - z[104]*z[416];
	z[790] = z[375]*(z[9]*(L2B*z[8]+(z[370]-Q3)*z[9])-z[8]*(L2B*z[9]-(z[370]-Q3)*z[8]))/((z[368]*z[16]+z[369]*z[17])*(L2B*z[9]-(z[370]-Q3)*z[8])-(z[368]*z[17]-z[369]*z[16])*(L2B*z[8]+(z[370]-Q3)*z[9]));
	z[791] = z[775]*z[781] + z[776]*z[780];
	z[792] = z[38]*z[787];
	z[793] = z[406]*z[787];
	z[794] = -z[38]*z[789] - z[406]*z[788];
	z[795] = z[375]*z[794] + z[394]*z[793] + z[407]*z[792];
	z[796] = z[775]*z[782] - z[777]*z[780];
	z[797] = z[38]*z[786];
	z[798] = z[406]*z[786];
	z[799] = z[790]*z[794] - z[394]*z[798] - z[407]*z[797];
	z[800] = -z[775]*z[783] - z[778]*z[780];
	z[801] = -z[375]*z[798] - z[790]*z[793];
	z[802] = z[775]*z[784] - z[779]*z[780];
	z[803] = z[375]*z[797] + z[790]*z[792];
	z[804] = -z[776]*z[782] - z[777]*z[781];
	z[805] = z[38]*z[785] - z[788];
	z[806] = -z[789] - z[406]*z[785];
	z[807] = z[407]*z[805] - z[394]*z[806];
	z[808] = z[776]*z[783] - z[778]*z[781];
	z[809] = -z[375]*z[806] - z[407]*z[787];
	z[810] = -z[776]*z[784] - z[779]*z[781];
	z[811] = -z[375]*z[805] - z[394]*z[787];
	z[812] = -z[777]*z[783] - z[778]*z[782];
	z[813] = z[407]*z[786] - z[790]*z[806];
	z[814] = z[777]*z[784] - z[779]*z[782];
	z[815] = z[394]*z[786] - z[790]*z[805];
	z[816] = z[778]*z[784] + z[779]*z[783];
	z[817] = z[375]*z[786] + z[787]*z[790];
	z[818] = z[791]*z[795] + z[800]*z[801] + z[804]*z[807] + z[810]*z[811] + z[812]*z[813] + z[816]*z[817] - z[796]*z[799] - z[802]*z[803] - z[808]*z[809] - z[814]*z[815];
	z[819] = z[781]*z[787] + z[782]*z[786];
	z[820] = z[38]*z[407] + z[394]*z[406];
	z[821] = z[781]*z[788] - z[783]*z[786];
	z[822] = z[375]*z[406];
	z[823] = z[781]*z[789] + z[784]*z[786];
	z[824] = z[38]*z[375];
	z[825] = z[782]*z[788] + z[783]*z[787];
	z[826] = z[406]*z[790];
	z[827] = z[782]*z[789] - z[784]*z[787];
	z[828] = z[38]*z[790];
	z[829] = -z[783]*z[789] - z[784]*z[788];
	z[830] = z[819]*z[820] + z[825]*z[826] + z[827]*z[828] - z[821]*z[822] - z[823]*z[824];
	z[831] = z[780]*z[787] - z[782]*z[785];
	z[832] = z[780]*z[788] + z[783]*z[785];
	z[833] = z[780]*z[789] - z[784]*z[785];
	z[834] = z[375]*z[829] + z[407]*z[825] + z[820]*z[831] - z[394]*z[827] - z[822]*z[832] - z[824]*z[833];
	z[835] = -z[780]*z[786] - z[781]*z[785];
	z[836] = z[407]*z[821] + z[790]*z[829] + z[820]*z[835] - z[394]*z[823] - z[826]*z[832] - z[828]*z[833];
	z[837] = z[407]*z[819] + z[790]*z[827] + z[822]*z[835] - z[375]*z[823] - z[826]*z[831];
	z[838] = z[394]*z[819] + z[790]*z[825] + z[828]*z[831] - z[375]*z[821] - z[824]*z[835];
	z[839] = z[777]*z[786] - z[776]*z[787];
	z[840] = z[778]*z[786] - z[776]*z[788];
	z[841] = z[779]*z[786] - z[776]*z[789];
	z[842] = z[777]*z[788] - z[778]*z[787];
	z[843] = z[777]*z[789] - z[779]*z[787];
	z[844] = z[778]*z[789] - z[779]*z[788];
	z[845] = z[820]*z[839] + z[826]*z[842] + z[828]*z[843] - z[822]*z[840] - z[824]*z[841];
	z[846] = z[775]*z[787] - z[777]*z[785];
	z[847] = z[775]*z[788] - z[778]*z[785];
	z[848] = z[775]*z[789] - z[779]*z[785];
	z[849] = z[375]*z[844] + z[407]*z[842] + z[820]*z[846] - z[394]*z[843] - z[822]*z[847] - z[824]*z[848];
	z[850] = z[776]*z[785] - z[775]*z[786];
	z[851] = z[407]*z[840] + z[790]*z[844] + z[820]*z[850] - z[394]*z[841] - z[826]*z[847] - z[828]*z[848];
	z[852] = z[407]*z[839] + z[790]*z[843] + z[822]*z[850] - z[375]*z[841] - z[826]*z[846];
	z[853] = z[394]*z[839] + z[790]*z[842] + z[828]*z[846] - z[375]*z[840] - z[824]*z[850];
	z[854] = z[804]*z[820] + z[812]*z[826] + z[814]*z[828] - z[808]*z[822] - z[810]*z[824];
	z[855] = z[375]*z[816] + z[407]*z[812] + z[796]*z[820] - z[394]*z[814] - z[800]*z[822] - z[802]*z[824];
	z[856] = z[407]*z[808] + z[790]*z[816] + z[791]*z[820] - z[394]*z[810] - z[800]*z[826] - z[802]*z[828];
	z[857] = z[407]*z[804] + z[790]*z[814] + z[791]*z[822] - z[375]*z[810] - z[796]*z[826];
	z[858] = z[394]*z[804] + z[790]*z[812] + z[796]*z[828] - z[375]*z[808] - z[791]*z[824];
	z[859] = z[407]*z[788] - z[394]*z[789];
	z[860] = z[407]*z[787] - z[375]*z[789];
	z[861] = z[394]*z[787] - z[375]*z[788];
	z[862] = -z[407]*z[786] - z[789]*z[790];
	z[863] = -z[394]*z[786] - z[788]*z[790];
	z[864] = -z[375]*z[786] - z[787]*z[790];
	z[865] = z[804]*z[859] + z[810]*z[861] + z[812]*z[862] + z[816]*z[864] - z[808]*z[860] - z[814]*z[863];
	z[866] = z[407]*z[785];
	z[867] = z[394]*z[785];
	z[868] = z[375]*z[785];
	z[869] = z[796]*z[859] + z[802]*z[861] + z[812]*z[866] + z[816]*z[868] - z[800]*z[860] - z[814]*z[867];
	z[870] = z[785]*z[790];
	z[871] = z[791]*z[859] + z[802]*z[863] + z[808]*z[866] + z[816]*z[870] - z[800]*z[862] - z[810]*z[867];
	z[872] = z[791]*z[860] + z[802]*z[864] + z[804]*z[866] + z[814]*z[870] - z[796]*z[862] - z[810]*z[868];
	z[873] = z[791]*z[861] + z[800]*z[864] + z[804]*z[867] + z[812]*z[870] - z[796]*z[863] - z[808]*z[868];
	z[874] = z[792]*z[810] + z[793]*z[808] + z[794]*z[804] + z[797]*z[814] + z[798]*z[812];
	z[875] = z[792]*z[802] + z[793]*z[800] + z[794]*z[796] + z[806]*z[812] - z[787]*z[816] - z[805]*z[814];
	z[876] = z[786]*z[816] + z[791]*z[794] + z[806]*z[808] - z[797]*z[802] - z[798]*z[800] - z[805]*z[810];
	z[877] = z[786]*z[814] + z[787]*z[810] + z[804]*z[806] - z[791]*z[793] - z[796]*z[798];
	z[878] = z[786]*z[812] + z[787]*z[808] + z[791]*z[792] + z[796]*z[797] + z[804]*z[805];
	z[879] = z[28]*z[387] - z[27]*z[385];
	z[880] = -z[29]*z[395] - z[31]*z[377];
	z[881] = z[31]*z[879];
	z[882] = z[29]*z[879];
	z[883] = z[52]*z[405];
	z[884] = z[52]*z[882] - z[53]*z[395];
	z[885] = -z[52]*z[393] - z[53]*z[402];
	z[886] = z[52]*z[395] + z[53]*z[882];
	z[887] = z[53]*z[405];
	z[888] = -z[38]*z[52] - z[53]*z[403];
	z[889] = z[52]*z[406];
	z[890] = z[53]*z[406];
	z[891] = z[27]*z[386] - z[28]*z[388];
	z[892] = z[27]*z[387] + z[28]*z[385];
	z[893] = -z[29]*z[377] - z[31]*z[892];
	z[894] = -z[29]*z[396] - z[31]*z[375];
	z[895] = z[31]*z[891];
	z[896] = z[29]*z[891];
	z[897] = z[29]*z[892] - z[31]*z[377];
	z[898] = z[52]*z[407];
	z[899] = z[52]*z[896] - z[53]*z[396];
	z[900] = z[52]*z[897] - z[53]*z[879];
	z[901] = -z[52]*z[394] - z[53]*z[404];
	z[902] = z[52]*z[396] + z[53]*z[896];
	z[903] = z[52]*z[879] + z[53]*z[897];
	z[904] = z[53]*z[407];
	}

void FindPhysPos ()
	{
	CurrCoords [0] = (L1E+L1V)*z[393] + (L1F+L1U)*z[393] + (L1G+L1P)*z[414] + (L1W+L1X)*z[385] + (L2F+L2U)*z[402] + (L2E-L2V)*z[395] + (L2W-L2X)*z[387] - (L1A-L1L)*cos(Q1) - (L2G+L2P)*z[417];  

	CurrCoords [1] = L2A + L2L + L2S + (L1E+L1V)*z[38] + (L1F+L1U)*z[38] + (L1G+L1P)*z[415] + (L1W+L1X)*z[16] + (L2F+L2U)*z[403] + (L2E-L2V)*z[35] - (L2G+L2P)*z[418] - (L2W-L2X)*z[17];  

	CurrCoords [2] = (L1A-L1L)*sin(Q1) + (L1E+L1V)*z[394] + (L1F+L1U)*z[394] + (L1G+L1P)*z[416] + (L2F+L2U)*z[404] + (L2E-L2V)*z[396] - (L1W+L1X)*z[386] - (L2G+L2P)*z[419] - (L2W-L2X)*z[388];
	}

void FindTorques ()
	{
	TAU1 = z[907] *	z[908]*	(G*z[429]-z[755]-(z[659]*(z[872]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[852]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[877]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[837]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[857]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[754]*(z[869]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[849]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[875]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[834]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[855]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[653]*(z[865]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[874]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[845]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[830]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[854]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[658]*(z[871]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[876]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[851]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[836]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[856]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[660]*(z[873]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[878]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[853]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[838]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[858]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[653]*(z[865]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[845]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[874]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[830]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[854]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[658]*(z[871]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[851]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[876]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[836]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[856]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[660]*(z[873]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[853]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[878]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[838]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[858]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[659]*(z[872]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[877]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[852]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[837]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[857]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[754]*(z[869]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[875]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[849]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[834]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[855]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2))))/z[818])/(z[5]*z[905]*z[906]);

	TAU2 = z[908] *	z[909]*	(G*z[430]-z[765]-(z[761]*(z[872]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[852]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[877]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[837]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[857]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[764]*(z[869]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[849]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[875]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[834]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[855]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[757]*(z[865]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[874]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[845]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[830]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[854]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[760]*(z[871]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[876]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[851]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[836]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[856]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[762]*(z[873]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[878]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[853]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[838]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[858]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[757]*(z[865]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[845]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[874]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[830]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[854]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[760]*(z[871]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[851]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[876]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[836]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[856]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[762]*(z[873]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[853]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[878]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[838]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[858]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[761]*(z[872]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[877]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[852]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[837]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[857]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[764]*(z[869]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[875]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[849]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[834]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[855]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2))))/z[818])/(z[5]*z[905]*z[906]);

	TAU3 =			z[908]*	(G*z[431]-z[766]-(z[702]*(z[872]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[852]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[877]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[837]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[857]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[760]*(z[869]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[849]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[875]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[834]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[855]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[667]*(z[865]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[874]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[845]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[830]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[854]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[698]*(z[871]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[876]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[851]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[836]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[856]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[705]*(z[873]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[878]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[853]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[838]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[858]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[667]*(z[865]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[845]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[874]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[830]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[854]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[698]*(z[871]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[851]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[876]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[836]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[856]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[705]*(z[873]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[853]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[878]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[838]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[858]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[702]*(z[872]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[877]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[852]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[837]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[857]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[760]*(z[869]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[875]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[849]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[834]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[855]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2))))/z[818])/(z[5]*z[906]);

	TAU4 =			z[910]*	(G*z[433]-z[768]-(z[709]*(z[872]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[852]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[877]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[837]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[857]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[767]*(z[869]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[849]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[875]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[834]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[855]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[710]*(z[865]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[874]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[845]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[830]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[854]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[721]*(z[871]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[876]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[851]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[836]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[856]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[722]*(z[873]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[878]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[853]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[838]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[858]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[710]*(z[865]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[845]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[874]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[830]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[854]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[721]*(z[871]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[851]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[876]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[836]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[856]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[722]*(z[873]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[853]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[878]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[838]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[858]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[709]*(z[872]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[877]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[852]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[837]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[857]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[767]*(z[869]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[875]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[849]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[834]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[855]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2))))/z[818])/(z[5]*z[906]);

	TAU5 =				   -(G*z[434]+z[770]+(z[725]*(z[872]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[852]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[877]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[837]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[857]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[769]*(z[869]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[849]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[875]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[834]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[855]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))+z[724]*(z[873]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[878]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[853]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[838]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[858]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[735]*(z[871]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[876]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[851]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[836]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[856]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))+z[736]*(z[865]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[874]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[845]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[830]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[854]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[724]*(z[873]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[853]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[878]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[838]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[858]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[735]*(z[871]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[851]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[876]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[836]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[856]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[736]*(z[865]*(-KV*(U1+z[38]*U6-z[406]*U7))+z[845]*(KP*(TgtCoords [1]-CurrCoords [1])+KV*(z[406]*(Dummy7)*U6-(Dummy6)*U7-(z[108]*z[418]-z[38]*z[70]-z[72]*z[406]-z[106]*z[415]-z[111]*z[406])*U1-(z[68]*z[35]+z[107]*z[418]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406])*U5-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*U3/(Dummy12)))-z[874]*(-KV*(z[375]*U5+z[394]*U6+z[407]*U7+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*U3/(Dummy12)))-z[830]*(KP*(TgtCoords [0]-CurrCoords [0])-KV*(z[405]*(Dummy7)*U6+(z[103]*z[417]-z[104]*z[414])*U7-(Dummy9)*U5-(z[70]*z[393]+z[106]*z[414]-z[23]*z[377]-z[43]*z[377]-z[72]*z[405]-z[108]*z[417]-z[111]*z[405]-sin(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*U3/(Dummy12)))-z[854]*(KP*(TgtCoords [2]-CurrCoords [2])-KV*(z[407]*(Dummy7)*U6+(z[103]*z[419]-z[104]*z[416])*U7-(z[26]*z[394]+z[69]*z[394]+z[105]*z[416]-z[68]*z[396]-z[107]*z[419]-z[110]*z[407])*U5-(z[70]*z[394]+z[106]*z[416]-z[23]*z[375]-z[43]*z[375]-z[72]*z[407]-z[108]*z[419]-z[111]*z[407]-cos(Q1)*z[66])*U1-(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*U3/(Dummy12))))-z[725]*(z[872]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[877]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[852]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[837]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[857]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2)))-z[769]*(z[869]*(U6*z[239]-U7*(z[403]*U6+z[31]*z[225]))-z[875]*(U6*(z[396]*U4+z[396]*U5+z[879]*U1)+U7*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-sin(Q1)*U1*U5-U3*(sin(Q1)*(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*U1+(Dummy12)*z[375]*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*z[375]*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2)))/pow((Dummy12),2))-z[849]*(U6*(z[406]*z[358]+(Dummy7)*(z[403]*U6+z[31]*z[225]))+U7*(z[103]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[104]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U5*(z[26]*z[239]+z[69]*z[239]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[68]*z[225]-z[38]*z[263]-z[415]*z[354]-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U1*(z[70]*z[239]+z[406]*z[360]+z[51]*z[38]*z[232]+z[51]*z[406]*z[239]+z[104]*z[415]*z[232]+z[72]*(z[403]*U6+z[31]*z[225])+z[111]*(z[403]*U6+z[31]*z[225])+z[108]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[103]*z[418]*z[232]-z[106]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225]))+U3*((Dummy12)*(Dummy8)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy8)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*sin(Q4)*U4+z[20]*cos(Q4)*U4+z[26]*z[239]+z[69]*z[239]+z[403]*z[265]+z[406]*z[359]+z[418]*z[356]+z[110]*(z[403]*U6+z[31]*z[225])+z[71]*(z[406]*U6-z[29]*z[225])+z[107]*(z[889]*U6+z[53]*z[239]-z[888]*U7-z[55]*z[225])-z[25]*z[225]-z[38]*z[263]-z[406]*z[267]-z[415]*z[354]-z[73]*(z[403]*U6+z[31]*z[225])-z[105]*(z[890]*U6-z[418]*U7-z[52]*z[239]-z[54]*z[225])))/pow((Dummy12),2))-z[834]*(U7*(z[103]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)-z[104]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6))+U6*((Dummy7)*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[405]*z[358])+U5*(z[68]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[417]*z[356])+U1*(cos(Q1)*z[23]*U1+cos(Q1)*z[43]*U1+cos(Q1)*z[66]*U1+z[72]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[108]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[111]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[377]*z[204]+z[377]*z[227]+z[405]*z[360]+z[51]*z[405]*z[239]+z[103]*z[417]*z[232]-z[70]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[106]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[51]*z[393]*z[232]-z[104]*z[414]*z[232]-z[50]*sin(Q1)*z[225])+U3*((Dummy12)*(Dummy10)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy10)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[20]*(z[386]*U1+z[387]*U4)+z[25]*(z[396]*U1+z[879]*U4+z[879]*U5)+z[71]*(z[404]*U1+z[405]*U6+z[882]*U4+z[882]*U5)+z[107]*(z[419]*U1+z[883]*U6+z[884]*U4+z[884]*U5+z[885]*U7)+z[110]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)+z[393]*z[263]+z[405]*z[359]+z[414]*z[354]-z[19]*(z[385]*U4-z[388]*U1)-z[26]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[69]*(z[394]*U1+z[395]*U4+z[395]*U5)-z[73]*(z[407]*U1+z[880]*U6-z[881]*U4-z[881]*U5)-z[105]*(z[416]*U1+z[417]*U7+z[886]*U4+z[886]*U5+z[887]*U6)-z[402]*z[265]-z[405]*z[267]-z[417]*z[356]))/pow((Dummy12),2))-z[855]*(U7*(z[103]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)-z[104]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6))+U6*((Dummy7)*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[407]*z[358])+U5*(z[68]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[419]*z[356])+U1*(z[72]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[108]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[111]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[375]*z[204]+z[375]*z[227]+z[407]*z[360]+z[51]*z[407]*z[239]+z[103]*z[419]*z[232]-sin(Q1)*z[23]*U1-sin(Q1)*z[43]*U1-sin(Q1)*z[66]*U1-z[70]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[106]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[51]*z[394]*z[232]-z[104]*z[416]*z[232]-z[50]*cos(Q1)*z[225])+U3*((Dummy12)*(Dummy11)*(sin(Q2)*(Dummy4+cos(Q2)*Dummy1)*U2+cos(Q2)*(Dummy5-sin(Q2)*Dummy1)*U2+sin(Q2)*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-cos(Q2)*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy11)*(Dummy3*(Dummy4+cos(Q2)*Dummy1)*U4+Dummy2*(Dummy5-sin(Q2)*Dummy1)*U4+Dummy3*(sin(Q2)*U3-Dummy4*U2-cos(Q2)*Dummy1*U2)-Dummy2*(Dummy5*U2-cos(Q2)*U3-sin(Q2)*Dummy1*U2))+(cos(Q2)*(Dummy4+cos(Q2)*Dummy1)-sin(Q2)*(Dummy5-sin(Q2)*Dummy1))*(Dummy12)*(z[19]*(z[386]*U4+z[387]*U1)+z[20]*(z[385]*U1-z[388]*U4)+z[25]*(z[891]*U4+z[891]*U5+z[892]*U1)+z[71]*(z[407]*U6+z[896]*U4+z[896]*U5+z[897]*U1)+z[107]*(z[898]*U6+z[899]*U4+z[899]*U5+z[900]*U1+z[901]*U7)+z[110]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)+z[394]*z[263]+z[407]*z[359]+z[416]*z[354]-z[26]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[69]*(z[396]*U4+z[396]*U5+z[879]*U1)-z[73]*(z[893]*U1+z[894]*U6-z[895]*U4-z[895]*U5)-z[105]*(z[419]*U7+z[902]*U4+z[902]*U5+z[903]*U1+z[904]*U6)-z[404]*z[265]-z[407]*z[267]-z[419]*z[356]))/pow((Dummy12),2))))/z[818])/z[5];
	}


/****************************************************************************************************************************/
//											 --[[ Initialization and Runtime ]]--											//
/****************************************************************************************************************************/

void setup()
	{
	Serial.begin (115200);		// The suspension processor uses 115200 as its baud rate, so we have to too
#if TestingState == false
	Wire.begin (ArmAddress);

	Wire.onRequest (SendArmData);
	Wire.onReceive (ReadCommand);
#endif

	pinMode (BasePWMPin, OUTPUT);	// PWM Pins
	pinMode (ShoulderPWMPin, OUTPUT);
	pinMode (ElbowPWMPin, OUTPUT);
	pinMode (WristRotatePWMPin, OUTPUT);
	pinMode (WristPitchPWMPin, OUTPUT);
	pinMode (GripperPWMPin, OUTPUT);
	pinMode (FingerPin, OUTPUT);

	pinMode (WristRotateAngleSenseA, INPUT);
	pinMode (WristRotateSenseReset, INPUT);

	digitalWrite (BaseNegative, LOW);	// GPIO
	digitalWrite (BasePositive, LOW);
	digitalWrite (ShoulderNegative, LOW);
	digitalWrite (ShoulderPositive, LOW);
	digitalWrite (ElbowNegative, LOW);
	digitalWrite (ElbowPositive, LOW);
	digitalWrite (WristRotateNegative, LOW);
	digitalWrite (WristRotatePositive, LOW);
	digitalWrite (WristPitchNegative, LOW);
	digitalWrite (WristPitchPositive, LOW);
	digitalWrite (GripperNegative, LOW);
	digitalWrite (GripperPositive, LOW);
	digitalWrite (FingerPin, LOW);

	attachInterrupt (digitalPinToInterrupt (WristRotateAngleSenseA), TrackWristRotation, CHANGE);
	attachInterrupt (digitalPinToInterrupt (WristRotateSenseReset), ResetWristCounter, RISING);

	CurrPose = (double *) malloc (ArmVars * sizeof (double));
	PrevPose = (double *) malloc (ArmVars * sizeof (double));

	memset ((void *) CurrPose, 0, ArmVars * sizeof (double));
	memset ((void *) PrevPose, 0, ArmVars * sizeof (double));
	
	CalculateZConstants ();

/*  do  {
      ManageMotor (WristRotatePWMPin, WristRotatePositive, WristRotateNegative, -WristRotationTopSpeed);
      CurrPose [4] = (((float) TickCount) / WristTicksPerCycle) * 360;
      } while (CurrPose [4] != 0);
  ManageMotor (WristRotatePWMPin, WristRotatePositive, WristRotateNegative, 0);
  
	while (true)
		{
		CurrPose [0] = ReadSensor (BaseAngleSense);
		CurrPose [1] = ReadSensor (ShoulderAngleSense);
		CurrPose [2] = LawOfCosinesSide (TriangleSideA, TriangleSideB, CalculateTheTriangleAngle (CurrPose [1]));
		CurrPose [3] = ReadSensor (ElbowAngleSense);
		CurrPose [4] = (((float) TickCount) / WristTicksPerCycle) * 360;
		CurrPose [5] = ReadSensor (WristPitchAngleSense);

#if TestingState == true
	Serial.print ("CurrPose: "); Serial.print (CurrPose [0], 8); Serial.print (", "); Serial.print (CurrPose [1], 8); Serial.print (", "); Serial.print (CurrPose [2], 8); Serial.print (", "); Serial.print (CurrPose [3], 8); Serial.print (", "); Serial.print (CurrPose [4], 8); Serial.print (", "); Serial.println (CurrPose [5], 8);
	Serial.println ();
	Serial.println ();
#endif

/*		if ((CurrPose [0] < 180.0) && (CurrPose [0] > 0.2))
				ManageMotor (BasePWMPin, BasePositive, BaseNegative, 255);
			else if ((CurrPose [0] > 180.0) && (CurrPose [0] < 359.8))
					ManageMotor (BasePWMPin, BasePositive, BaseNegative, -255);
				else ManageMotor (BasePWMPin, BasePositive, BaseNegative, 0);
		if ((CurrPose [1] < 43.0) || (CurrPose [1] > 180.0))
				ManageMotor (ShoulderPWMPin, ShoulderPositive, ShoulderNegative, 255);
			else ManageMotor (ShoulderPWMPin, ShoulderPositive, ShoulderNegative, 0);
		if ((CurrPose [3] > 260.0) || (CurrPose [3] < 180.0))
				ManageMotor (ElbowPWMPin, ElbowPositive, ElbowNegative, -75);
			else if ((CurrPose [3] > 180.0) && (CurrPose [3] < 256.0))
					ManageMotor (ElbowPWMPin, ElbowPositive, ElbowNegative, 75);
				else ManageMotor (ElbowPWMPin, ElbowPositive, ElbowNegative, 0);
		if (CurrPose [4] != 0.0)
				ManageMotor (WristRotatePWMPin, WristRotatePositive, WristRotateNegative, -WristRotationTopSpeed);
			else ManageMotor (WristRotatePWMPin, WristRotatePositive, WristRotateNegative, 0);
		if ((CurrPose [5] > 251.0) && (CurrPose [5] <= 360.0))
				ManageMotor (WristPitchPWMPin, WristPitchPositive, WristPitchNegative, -100);
			else if ((CurrPose [5] >= 0.0) && (CurrPose [5] < 250.0))
					ManageMotor (WristPitchPWMPin, WristPitchPositive, WristPitchNegative, 100);
				else ManageMotor (WristPitchPWMPin, WristPitchPositive, WristPitchNegative, 0);
		if	(((CurrPose [0] > 359.8) && (CurrPose [0] < 0.2))
			&& (CurrPose [1] < 43.0)
			&& ((CurrPose [3] > 256.0) && (CurrPose [3] < 260.0))
			&& ((CurrPose [4] > 359.8) && (CurrPose [4] < 0.2))
  			&& ((CurrPose [5] > 250.0) && (CurrPose [5] < 251.0))
			)
			break;     
		}
*/
	PrevTime = millis ();

	delay (5000);
	}

void loop()
	{
#if TestingState == true
	if (Serial.available())
		{
    TempString = Serial.readStringUntil (isSpace(Serial.peek ()));
    TempString.trim ();
    for (int i = 0; i < TempString.length (); i++)
        {
        Serial.print ("TempString["); Serial.print (i); Serial.print ("] = "); Serial.println (TempString [i]);
        }
    Serial.print ("if (TempString.equalsIgnoreCase (MOVE): "); Serial.println (TempString.equalsIgnoreCase ("MOVE") == true);
    Serial.print ("if (TempString.equalsIgnoreCase (KP): "); Serial.println (TempString.equalsIgnoreCase ("KP") == true);
		if (TempString.equalsIgnoreCase ("MOVE") == true)
				{
				for (int i = 0; i < 3; i++)	// We read in the target 3D coordinates as floating point values.
					TgtCoords [i] = Serial.parseFloat ();
				while (Serial.available ())
					Serial.read ();
				Serial.print ("I read in: "); Serial.print (TgtCoords [0]); Serial.print (", "); Serial.print (TgtCoords [1]); Serial.print (", "); Serial.println (TgtCoords [2]);
				}
			else if (TempString.equalsIgnoreCase ("KP") == true)
					{
					Serial.println ("I need to change the KP now!");
					while (Serial.available ())
						Serial.read ();
					}
		}
#endif

	CurrPose [0] = ReadSensor (BaseAngleSense);
	CurrPose [1] = ReadSensor (ShoulderAngleSense);
	CurrPose [2] = LawOfCosinesSide (TriangleSideA, TriangleSideB, CalculateTheTriangleAngle (CurrPose [1]));
	CurrPose [3] = ReadSensor (ElbowAngleSense);
	CurrPose [4] = (((float) TickCount) / WristTicksPerCycle) * 360;
	CurrPose [5] = ReadSensor (WristPitchAngleSense);
//	CurrPose [6] = KalmanFilter (GripperAngleSense, ReadSensor (GripperAngleSense));

#if TestingState == true
//	Serial.print ("CurrPose: "); Serial.print (CurrPose [0], 8); Serial.print (", "); Serial.print (CurrPose [1], 8); Serial.print (", "); Serial.print (CurrPose [2], 8); Serial.print (", "); Serial.print (CurrPose [3], 8); Serial.print (", "); Serial.print (CurrPose [4], 8); Serial.print (", "); Serial.println (CurrPose [5], 8);
#endif

	if ((millis () - PrevTime) >= CalculationTimer)
		{
		CurrTime = millis ();
		if (CurrTime < PrevTime)
				TimeDifference = (CurrTime - PrevTime + SIZE_T_MAX);		// [ Seconds ]
			else TimeDifference = (CurrTime - PrevTime);
		TimeDifference /= 1000;   // [ Seconds ]
#if TestingState == true
//		Serial.print ("Time Slice: "); Serial.print (TimeDifference, 8); Serial.println (" sec.");
#endif

		Q1 = ConvertToRadians (CurrPose [0]);
		Q2 = LawOfCosinesAng (TriangleSideA, TriangleSideB, CurrPose [2]) - ConvertToRadians (Q2AngleCorrection);
		Q3 = CurrPose [2] - LinearActuatorMinExtension;
		Q4 = ConvertToRadians (CurrPose [1]);
		Q5 = ConvertToRadians (CurrPose [3]);
		Q6 = ConvertToRadians (CurrPose [4]);
		Q7 = ConvertToRadians (CurrPose [5]);
#if TestingState == true
//		Serial.print ("Q Vars: "); Serial.print (Q1, 8); Serial.print (", "); Serial.print (Q2, 8); Serial.print (", "); Serial.print (Q3, 8); Serial.print (", "); Serial.print (Q4, 8); Serial.print (", "); Serial.print (Q5, 8); Serial.print (", "); Serial.print (Q6, 8); Serial.print (", "); Serial.println (Q7, 8);
#endif

		CalculateZEquations ();
		Dummy1 = L1BASE-L1B-L1C-L1C2-Q3;
		Dummy2 = (L2D+L2X)*cos(Q4)-(L1D-L1X)*sin(Q4);
		Dummy3 = (L2D+L2X)*sin(Q4)+(L1D-L1X)*cos(Q4);
		Dummy4 = L2B*sin(Q2);
		Dummy5 = L2B*cos(Q2);
		Dummy6 = z[103]*z[418]-z[104]*z[415];
		Dummy7 = z[51]+z[109];
		Dummy8 = z[19]*z[17]+z[25]*z[35]+z[71]*z[403]+z[73]*z[406]+z[107]*z[418]-z[20]*z[16]-z[26]*z[38]-z[38]*z[69]-z[105]*z[415]-z[110]*z[406];
		Dummy9 = z[26]*z[393]+z[69]*z[393]+z[105]*z[414]-z[68]*z[395]-z[107]*z[417]-z[110]*z[405];
		Dummy10 = z[19]*z[387]+z[20]*z[385]+z[26]*z[393]+z[69]*z[393]+z[73]*z[405]+z[105]*z[414]-z[25]*z[395]-z[71]*z[402]-z[107]*z[417]-z[110]*z[405];
		Dummy11 = z[26]*z[394]+z[69]*z[394]+z[73]*z[407]+z[105]*z[416]-z[19]*z[388]-z[20]*z[386]-z[25]*z[396]-z[71]*z[404]-z[107]*z[419]-z[110]*z[407];
		Dummy12 = Dummy3*(Dummy5-sin(Q2)*Dummy1)-Dummy2*(Dummy4+cos(Q2)*Dummy1);

		U1 = CalculateVelocity (ConvertToRadians (CurrPose [0]), ConvertToRadians (PrevPose [0]), TimeDifference);
		U2 = (cos(Q2)*(Dummy3)-sin(Q2)*(Dummy2))*U3/((Dummy3)*(Dummy5-sin(Q2)*(Dummy1))-(Dummy2)*(Dummy4+cos(Q2)*(Dummy1)));
		U3 = CalculateVelocity (CurrPose [2], PrevPose [2], TimeDifference);
		U4 = (cos(Q2)*(Dummy4+cos(Q2)*(Dummy1))-sin(Q2)*(Dummy5-sin(Q2)*(Dummy1)))*U3/((Dummy3)*(Dummy5-sin(Q2)*(Dummy1))-(Dummy2)*(Dummy4+cos(Q2)*(Dummy1)));
		U5 = CalculateVelocity (ConvertToRadians (CurrPose [3]), ConvertToRadians (PrevPose [3]), TimeDifference);
		U6 = CalculateVelocity (ConvertToRadians (CurrPose [4]), ConvertToRadians (PrevPose [4]), TimeDifference);
		U7 = CalculateVelocity (ConvertToRadians (CurrPose [5]), ConvertToRadians (PrevPose [5]), TimeDifference);
#if TestingState == true
//		Serial.print ("Velocity: "); Serial.print (U1, 8); Serial.print (", "); Serial.print (U2, 8);Serial.print (", "); Serial.print (U3, 8); Serial.print (", "); Serial.print (U4, 8);Serial.print (", "); Serial.print (U5, 8); Serial.print (", "); Serial.print (U6, 8); Serial.print (", "); Serial.println (U7, 8);
#endif

		FindPhysPos ();
#if TestingState == true
//		Serial.print ("Physical Position: "); Serial.print (CurrCoords [0]); Serial.print (", "); Serial.print (CurrCoords [1]); Serial.print (", "); Serial.println (CurrCoords [2]);
//		Serial.print ("Target Position: "); Serial.print (TgtCoords [0]); Serial.print (", "); Serial.print (TgtCoords [1]); Serial.print (", "); Serial.println (TgtCoords [2]);
#endif
		FindTorques ();
#if TestingState == true
//		Serial.print ("Torques: "); Serial.print (TAU1, 8); Serial.print (", "); Serial.print (TAU2, 8); Serial.print (", "); Serial.print (TAU3, 8); Serial.print (", "); Serial.print (TAU4, 8); Serial.print (", "); Serial.println (TAU5, 8);
//		Serial.print ("Voltages: "); Serial.print (ConvertTorqueToPWM (TAU1, KT_Base, KI_Base, R_Base, U1)); Serial.print (", "); Serial.print (ConvertTorqueToPWM (TAU2, KT_Shoulder, KI_Shoulder, R_Shoulder, U3)); Serial.print (", "); Serial.print (ConvertTorqueToPWM (TAU3, KT_Elbow, KI_Elbow, R_Elbow, U5)); Serial.print (", "); Serial.print (ConvertTorqueToPWM (TAU4, KT_WristRotation, KI_WristRotation, R_WristRotation, U6)); Serial.print (", "); Serial.println (ConvertTorqueToPWM (TAU5, KT_WristPitch, KI_WristPitch, R_WristPitch, U7));
#endif

		ManageMotor (BasePWMPin, BasePositive, BaseNegative, ConvertTorqueToPWM (TAU1, KT_Base, KI_Base, R_Base, U1));
		ManageMotor (ShoulderPWMPin, ShoulderPositive, ShoulderNegative, ConvertTorqueToPWM (TAU2, KT_Shoulder, KI_Shoulder, R_Shoulder, U3));
		ManageMotor (ElbowPWMPin, ElbowPositive, ElbowNegative, ConvertTorqueToPWM (TAU3, KT_Elbow, KI_Elbow, R_Elbow, U5));
		ManageMotor (WristRotatePWMPin, WristRotatePositive, WristRotateNegative, ConvertTorqueToPWM (TAU4, KT_WristRotation, KI_WristRotation, R_WristRotation, U6));
		ManageMotor (WristPitchPWMPin, WristPitchPositive, WristPitchNegative, ConvertTorqueToPWM (TAU5, KT_WristPitch, KI_WristPitch, R_WristPitch, U7));
//		ManageMotor (GripperPWMPin, GripperPositive, GripperNegative, );
		}

#if TestingState == true
//	Serial.println ();
//	Serial.println ();
//	Serial.println ();
#endif

	TempPose = PrevPose;
	PrevPose = CurrPose;
	CurrPose = TempPose;

	if (PrevTime > millis ())
			PrevTime = millis ();
		else PrevTime = CurrTime;
	}
